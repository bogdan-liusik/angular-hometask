import { Component } from '@angular/core';
import {MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-delete-warning-dialog',
  templateUrl: './delete-warning-dialog.component.html',
  styleUrls: ['./delete-warning-dialog.component.scss']
})
export class DeleteWarningDialogComponent {

  constructor(public dialogRef: MatDialogRef<DeleteWarningDialogComponent>) { }

  onBackClick(): void {
    this.dialogRef.close();
  }
}
