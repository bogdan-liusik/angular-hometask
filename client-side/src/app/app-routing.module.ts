import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {TasksComponent} from "./modules/task/components/tasks/tasks.component";
import {TeamsComponent} from "./modules/team/components/teams/teams.component";
import {ProjectsComponent} from "./modules/project/components/projects/projects.component";
import {UsersComponent} from "./modules/user/components/users/users.component";
import {ProjectsGuard} from "./modules/project/guards/projects.guard";
import {TasksGuard} from "./modules/task/guards/tasks.guard";
import {UsersGuard} from "./modules/user/guards/users.guard";
import {TeamsGuard} from "./modules/team/guards/teams.guard";

const routes: Routes = [
  { path: 'projects', component: ProjectsComponent, canDeactivate: [ProjectsGuard] },
  { path: 'tasks', component: TasksComponent, canDeactivate: [TasksGuard] },
  { path: 'teams', component: TeamsComponent, canDeactivate: [TeamsGuard] },
  { path: 'users', component: UsersComponent, canDeactivate: [UsersGuard]},
  { path: '**', component: ProjectsComponent, canDeactivate: [ProjectsGuard] }
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
