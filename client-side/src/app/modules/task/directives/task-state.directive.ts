import {Directive, ElementRef, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {TaskState} from "../models/enums/task-state";

@Directive({
  selector: '[app-task-state]'
})
export class TaskStateDirective implements OnInit, OnChanges{
  @Input() state: TaskState = {} as TaskState;

  constructor(private el: ElementRef) { }

  ngOnInit(): void {

    switch(this.state){
      case TaskState.ToDo : {
        this.el.nativeElement.style.background = "#8FDFCF";
        break;
      }
      case TaskState.InProgress : {
        this.el.nativeElement.style.background = "#F3DF63";
        break;
      }
      case TaskState.Done : {
        this.el.nativeElement.style.background = "#4ACA3A";
        break;
      }
      case TaskState.Canceled : {
        this.el.nativeElement.style.background = "#EE4545";
        break;
      }
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.ngOnInit();
  }
}
