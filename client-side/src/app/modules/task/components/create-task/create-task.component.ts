import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Subject} from "rxjs";
import {SnackBarService} from "../../../../services/snack-bar.service";
import {takeUntil} from "rxjs/operators";
import {TasksService} from "../../services/tasks-service";
import {NewTask} from "../../models/new-task";
import {TaskState} from "../../models/enums/task-state";
import {Task} from "../../models/task";
import {taskConstants} from "../../models/taskConstants";

@Component({
  selector: 'app-create-task',
  templateUrl: './create-task.component.html',
  styleUrls: ['./create-task.component.scss'],
})
export class CreateTaskComponent implements OnInit {
  public isLoading: boolean = false;
  public formGroup: FormGroup = {} as FormGroup;
  public constants = taskConstants;

  @Output() onCancelCreating = new EventEmitter<void>();
  @Output() onTaskCreated = new EventEmitter<Task>()

  private unsubscribe$ = new Subject<void>();

  constructor(private taskService: TasksService,
              private snackBarService: SnackBarService) { }

  ngOnInit(): void {
    this.formGroup = new FormGroup({
      name: new FormControl(
        '',
        [
          Validators.required,
          Validators.minLength(this.constants.MIN_TASK_NAME_LENGTH) ,
          Validators.maxLength(this.constants.MAX_TASK_NAME_LENGTH)]
      ),

      description: new FormControl(
        '',
        [
          Validators.required,
          Validators.minLength(this.constants.MIN_TASK_DESCRIPTION_LENGTH),
          Validators.maxLength(this.constants.MAX_TASK_DESCRIPTION_LENGTH)]
      ),

      performerId: new FormControl(
        '',
        [
          Validators.required,
          Validators.min(1)
        ]),

      projectId: new FormControl(
        '',
        [
          Validators.required,
          Validators.min(1)]),

      state: new FormControl(
        TaskState.ToDo,
        [Validators.min(0), Validators.max(3)]
      ),

      createdAt: new FormControl(new Date()),
    });
  }

  onSubmit(): void {
    this.isLoading = true;
    const newTask: NewTask = <NewTask>this.formGroup.value;

    this.taskService
      .createTask(newTask)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        response => {
          const createdTask: Task = <Task>response.body;
          this.isLoading = false;
          this.onTaskCreated.emit(createdTask);
          this.snackBarService.showSuccessMessage('Successfully created!');
        },
        error => {
          this.isLoading = false;
          this.snackBarService.showErrorMessage(error.error.error);
        }
      );
  }
}
