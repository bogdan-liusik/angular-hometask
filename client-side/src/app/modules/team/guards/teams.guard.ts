import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanDeactivate, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable} from "rxjs";
import {TeamsComponent} from "../components/teams/teams.component";

@Injectable({
  providedIn: 'root'
})
export class TeamsGuard implements CanDeactivate<TeamsComponent>{
  canDeactivate(
    component: TeamsComponent,
    currentRoute: ActivatedRouteSnapshot,
    currentState: RouterStateSnapshot,
    nextState?: RouterStateSnapshot)
    : Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    const updatingTeam: boolean =  component.innerComponents
      .some(component => component.isUpdating);

    if(component.creatingNewTeam || updatingTeam){
      return window.confirm('You have unsaved changes! Leave?')
    }

    return true;
  }
}
