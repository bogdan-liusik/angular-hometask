import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Subject} from "rxjs";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {SnackBarService} from "../../../../services/snack-bar.service";
import {TeamsService} from "../../services/teams-service";
import {takeUntil} from "rxjs/operators";
import {UpdateTeam} from "../../models/update-team";
import {Team} from "../../models/team";
import {teamConstants} from "../../models/teamConstants";

@Component({
  selector: 'app-update-team',
  templateUrl: './update-team.component.html',
  styleUrls: ['./update-team.component.scss']
})
export class UpdateTeamComponent implements OnInit {
  @Input() public team: Team = {} as Team;
  @Output() onCancelUpdate = new EventEmitter<void>();
  @Output() onTeamUpdated = new EventEmitter<Team>();

  public isLoading: boolean = false;
  private unsubscribe$ = new Subject<void>();
  public formGroup: FormGroup = {} as FormGroup;
  public constants = teamConstants;

  constructor(private teamService: TeamsService,
              private snackBarService: SnackBarService) { }

  ngOnInit(): void {
    this.formGroup = new FormGroup({
      id: new FormControl(
        this.team.id
      ),

      name: new FormControl(
        this.team.name,
        [
          Validators.required,
          Validators.minLength(this.constants.MIN_NAME_LENGTH),
          Validators.maxLength(this.constants.MAX_NAME_LENGTH)]),
    });
  }

  public onSubmit(): void{
    this.isLoading = true;

    const updateTeam: UpdateTeam = <UpdateTeam>this.formGroup.value;

    this.teamService
      .updateTeam(updateTeam)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        response => {
          this.isLoading = false;
          this.team = <Team>response.body;
          this.onTeamUpdated.emit(this.team);
          this.snackBarService.showSuccessMessage('Successfully updated!');
        },
        error => {
          this.isLoading = false;
          this.snackBarService.showErrorMessage(error);
        });
  }
}
