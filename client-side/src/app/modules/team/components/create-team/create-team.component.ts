import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Subject} from "rxjs";
import {SnackBarService} from "../../../../services/snack-bar.service";
import {TeamsService} from "../../services/teams-service";
import {takeUntil} from "rxjs/operators";
import {NewTeam} from "../../models/new-team";
import {Team} from "../../models/team";
import {teamConstants} from "../../models/teamConstants";

@Component({
  selector: 'app-create-team',
  templateUrl: './create-team.component.html',
  styleUrls: ['./create-team.component.scss']
})
export class CreateTeamComponent implements OnInit {
  public isLoading: boolean = false;
  public formGroup: FormGroup = {} as FormGroup;
  public constants = teamConstants;

  @Output() onCancelCreating = new EventEmitter<void>();
  @Output() onTeamCreated = new EventEmitter<Team>();

  private unsubscribe$ = new Subject<void>();

  constructor(private teamsService: TeamsService,
              private snackBarService: SnackBarService) { }

  ngOnInit(): void {
    this.formGroup = new FormGroup({
      name: new FormControl(
        '',
        [
          Validators.required,
          Validators.minLength(this.constants.MIN_NAME_LENGTH),
          Validators.maxLength(this.constants.MAX_NAME_LENGTH)]),

      createdAt: new FormControl(new Date())
    });
  }

  onSubmit(): void{
    this.isLoading = true;
    const newTeam: NewTeam = <NewTeam>this.formGroup.value;

    this.teamsService
      .createTeam(newTeam)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        response => {
          const createdTeam: Team = <Team>response.body;
          this.isLoading = false;
          this.onTeamCreated.emit(createdTeam);
          this.snackBarService.showSuccessMessage('Successfully created!');
        },
        error => {
          this.isLoading = false;
          this.snackBarService.showErrorMessage(error);
        });
  }
}
