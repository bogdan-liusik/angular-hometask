import {Injectable} from "@angular/core";
import {HttpInternalService} from "../../../services/http-internal.service";
import {Observable} from "rxjs";
import {HttpResponse} from "@angular/common/http";
import {User} from "../models/user";
import {UpdateUser} from "../models/update-user";
import {NewUser} from "../models/new-user";

@Injectable({ providedIn: 'root' })
export class UsersService {
  public readonly routePrefix = '/api/users';

  constructor(private httpService: HttpInternalService) { }

  public getAllUsers(): Observable<User[]> {
    return this.httpService.getRequest<User[]>(`${this.routePrefix}`);
  }

  public createUser(newUser: NewUser): Observable<HttpResponse<User>> {
    return this.httpService.postFullRequest<User>(`${this.routePrefix}`, newUser);
  }

  public updateUser(updateUser: UpdateUser): Observable<HttpResponse<User>> {
    return this.httpService.putFullRequest(`${this.routePrefix}`, updateUser);
  }

  public deleteUser(id: number) {
    return this.httpService.deleteFullRequest(`${this.routePrefix}/${id}`);
  }
}
