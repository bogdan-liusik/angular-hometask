import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProjectsComponent } from './components/projects/projects.component';
import { ProjectComponent } from './components/project/project.component';
import { MatButtonToggleModule } from "@angular/material/button-toggle";
import { MaterialComponentsModule } from "../../common/material-components.module";
import { UpdateProjectComponent } from './components/update-project/update-project.component';
import { ReactiveFormsModule } from "@angular/forms";
import { CreateProjectComponent } from './components/create-project/create-project.component';
import {UkrainianDatePipe} from "../../common/pipes/ukrainian-date.pipe";

@NgModule({
  declarations: [
    ProjectsComponent,
    ProjectComponent,
    UpdateProjectComponent,
    CreateProjectComponent,
    UkrainianDatePipe
  ],
  exports: [
    UkrainianDatePipe
  ],
  imports: [
    CommonModule,
    MatButtonToggleModule,
    MaterialComponentsModule,
    ReactiveFormsModule,
  ]
})

export class ProjectModule { }
