﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.Services;
using ProjectStructure.Common.DTO.LinqQueryResults;
using ProjectStructure.Common.DTO.Task;

namespace ProjectStructure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LinqQueriesController : ControllerBase
    {
        private readonly LinqQueriesService _linqQueriesService;
        
        public LinqQueriesController(LinqQueriesService linqQueriesService)
        {
            _linqQueriesService = linqQueriesService;
        }

        [HttpGet("query1/{id:int}")]
        public async Task<ActionResult<ICollection<Query1StructureDTO>>> Query1(int id)
        {
            var result = await _linqQueriesService.Query1(id);
            if (result.Count == 0)
            {
                return NoContent();
            }
            return Ok(result);
        }
        
        [HttpGet("query2/{id:int}")]
        public async Task<ActionResult<ICollection<TaskDTO>>> Query2(int id)
        {
            return Ok(await _linqQueriesService.Query2(id));
        }

        [HttpGet("query3/{id:int}")]
        public async Task<ActionResult<ICollection<Query3StructureDTO>>> Query3(int id)
        {
            return Ok(await _linqQueriesService.Query3(id));
        }

        [HttpGet("query4")]
        public async Task<ActionResult<ICollection<Query4StructureDTO>>> Query4()
        {
            return Ok(await _linqQueriesService.Query4());
        }

        [HttpGet("query5")]
        public async Task<ActionResult<ICollection<Query5StructureDTO>>> Query5()
        {
            return Ok(await _linqQueriesService.Query5());
        }

        [HttpGet("query6/{id:int}")]
        public async Task<ActionResult<Query6StructureDTO>> Query6(int id)
        {
            return Ok(await _linqQueriesService.Query6(id));
        }

        [HttpGet("query7")]
        public async Task<ActionResult<ICollection<Query7StructureDTO>>> Query7()
        {
            return Ok(await _linqQueriesService.Query7());
        }
    }
}