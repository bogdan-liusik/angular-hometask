using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using ProjectStructure.DAL.Context;
using ProjectStructure.WebAPI.Extensions;
using ProjectStructure.WebAPI.Filters;

namespace ProjectStructure.WebAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
            
        public IConfiguration Configuration { get; }
        
        public void ConfigureServices(IServiceCollection services)
        {
            var migrationAssembly = typeof(ProjectsContext).Assembly.GetName().Name;
            services.AddDbContext<ProjectsContext>(options =>
                options.UseSqlServer(Configuration["ConnectionStrings:ProjectsDBConnection"], opt => opt.MigrationsAssembly(migrationAssembly)));
            
            services.RegisterCustomServices();
            services.RegisterAutoMapper();
            
            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo {Title = "ProjectStructure.WebAPI", Version = "v1"});
            });

            services.AddMvcCore(options => options.Filters.Add(typeof(CustomExceptionFilterAttribute)));
        }
        
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "ProjectStructure.WebAPI v1"));
            }
            
            app.UseCors(builder => builder
                .AllowAnyMethod()
                .AllowAnyHeader()
                .AllowCredentials()
                .WithOrigins("http://localhost:4200"));
            
            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }
    }
}