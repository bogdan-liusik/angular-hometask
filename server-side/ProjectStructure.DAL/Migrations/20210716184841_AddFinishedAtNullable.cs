﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ProjectStructure.DAL.Migrations
{
    public partial class AddFinishedAtNullable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "FinishedAt",
                table: "Tasks",
                type: "datetime2",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 44, new DateTime(2018, 4, 10, 14, 42, 54, 255, DateTimeKind.Unspecified).AddTicks(2143), new DateTime(2018, 10, 25, 23, 58, 42, 974, DateTimeKind.Unspecified).AddTicks(9955), "New ABC 13 9370, 13.3, 5th Gen CoreA5-8250U, 8GB RAM, 256GB SSD, power UHD Graphics, OS 10 Home, OS Office A & J 2016", "Handmade Fresh Bacon", 8 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 43, new DateTime(2021, 1, 28, 8, 8, 4, 593, DateTimeKind.Unspecified).AddTicks(8599), new DateTime(2021, 4, 3, 14, 29, 59, 860, DateTimeKind.Unspecified).AddTicks(6150), "Andy shoes are designed to keeping in mind durability as well as trends, the most stylish range of shoes & sandals", "Gorgeous Plastic Fish", 1 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 78, new DateTime(2020, 11, 5, 16, 55, 5, 905, DateTimeKind.Unspecified).AddTicks(5434), new DateTime(2021, 5, 5, 2, 43, 39, 320, DateTimeKind.Unspecified).AddTicks(9145), "The Football Is Good For Training And Recreational Purposes", "Ergonomic Soft Pants", 8 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 37, new DateTime(2021, 3, 29, 11, 37, 38, 221, DateTimeKind.Unspecified).AddTicks(2357), new DateTime(2021, 6, 11, 11, 33, 59, 845, DateTimeKind.Unspecified).AddTicks(609), "New range of formal shirts are designed keeping you in mind. With fits and styling that will make you stand apart", "Rustic Steel Table", 10 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 47, new DateTime(2020, 1, 1, 22, 38, 25, 893, DateTimeKind.Unspecified).AddTicks(537), new DateTime(2021, 2, 22, 15, 26, 21, 219, DateTimeKind.Unspecified).AddTicks(37), "New range of formal shirts are designed keeping you in mind. With fits and styling that will make you stand apart", "Generic Plastic Shirt", 9 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 17, new DateTime(2021, 3, 16, 2, 10, 59, 549, DateTimeKind.Unspecified).AddTicks(3663), new DateTime(2021, 9, 28, 14, 13, 37, 367, DateTimeKind.Unspecified).AddTicks(4840), "New range of formal shirts are designed keeping you in mind. With fits and styling that will make you stand apart", "Tasty Fresh Chicken", 1 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 54, new DateTime(2020, 11, 4, 13, 27, 6, 135, DateTimeKind.Unspecified).AddTicks(8087), new DateTime(2021, 11, 21, 9, 16, 2, 130, DateTimeKind.Unspecified).AddTicks(7418), "The Apollotech B340 is an affordable wireless mouse with reliable connectivity, 12 months battery life and modern design", "Sleek Wooden Pants", 5 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name" },
                values: new object[] { 24, new DateTime(2020, 6, 2, 21, 40, 34, 320, DateTimeKind.Unspecified).AddTicks(1705), new DateTime(2020, 12, 9, 0, 37, 37, 112, DateTimeKind.Unspecified).AddTicks(5749), "The Nagasaki Lander is the trademarked name of several series of Nagasaki sport bikes, that started with the 1984 ABC800J", "Small Plastic Computer" });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 55, new DateTime(2020, 3, 8, 4, 27, 50, 175, DateTimeKind.Unspecified).AddTicks(2114), new DateTime(2020, 8, 7, 8, 27, 4, 706, DateTimeKind.Unspecified).AddTicks(9194), "New ABC 13 9370, 13.3, 5th Gen CoreA5-8250U, 8GB RAM, 256GB SSD, power UHD Graphics, OS 10 Home, OS Office A & J 2016", "Fantastic Granite Gloves", 2 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 8, new DateTime(2018, 11, 8, 0, 6, 19, 466, DateTimeKind.Unspecified).AddTicks(4844), new DateTime(2019, 4, 15, 14, 44, 13, 138, DateTimeKind.Unspecified).AddTicks(1364), "Ergonomic executive chair upholstered in bonded black leather and PVC padded seat and back for all-day comfort and support", "Rustic Soft Hat", 10 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 48, new DateTime(2020, 3, 8, 19, 9, 53, 949, DateTimeKind.Unspecified).AddTicks(8184), new DateTime(2020, 7, 5, 19, 24, 24, 433, DateTimeKind.Unspecified).AddTicks(5738), "The slim & simple Maple Gaming Keyboard from Dev Byte comes with a sleek body and 7- Color RGB LED Back-lighting for smart functionality", "Rustic Metal Ball", 6 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 63, new DateTime(2019, 1, 13, 16, 38, 56, 655, DateTimeKind.Unspecified).AddTicks(4543), new DateTime(2021, 1, 1, 16, 17, 0, 586, DateTimeKind.Unspecified).AddTicks(4395), "New ABC 13 9370, 13.3, 5th Gen CoreA5-8250U, 8GB RAM, 256GB SSD, power UHD Graphics, OS 10 Home, OS Office A & J 2016", "Sleek Granite Fish", 1 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 57, new DateTime(2018, 2, 6, 22, 20, 5, 127, DateTimeKind.Unspecified).AddTicks(1619), new DateTime(2019, 7, 14, 18, 51, 17, 612, DateTimeKind.Unspecified).AddTicks(1527), "Boston's most advanced compression wear technology increases muscle oxygenation, stabilizes active muscles", "Intelligent Cotton Table", 8 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 74, new DateTime(2020, 12, 25, 19, 45, 49, 199, DateTimeKind.Unspecified).AddTicks(1903), new DateTime(2021, 1, 6, 6, 39, 21, 5, DateTimeKind.Unspecified).AddTicks(5783), "The beautiful range of Apple Naturalé that has an exciting mix of natural ingredients. With the Goodness of 100% Natural Ingredients", "Awesome Wooden Mouse", 8 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 6, new DateTime(2020, 8, 13, 1, 11, 39, 387, DateTimeKind.Unspecified).AddTicks(8744), new DateTime(2021, 9, 8, 11, 33, 1, 389, DateTimeKind.Unspecified).AddTicks(7973), "Carbonite web goalkeeper gloves are ergonomically designed to give easy fit", "Ergonomic Frozen Gloves", 4 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 33, new DateTime(2019, 8, 7, 20, 45, 10, 99, DateTimeKind.Unspecified).AddTicks(8108), new DateTime(2019, 10, 17, 8, 16, 55, 655, DateTimeKind.Unspecified).AddTicks(3991), "New ABC 13 9370, 13.3, 5th Gen CoreA5-8250U, 8GB RAM, 256GB SSD, power UHD Graphics, OS 10 Home, OS Office A & J 2016", "Refined Wooden Chicken", 4 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 45, new DateTime(2019, 7, 17, 17, 58, 58, 283, DateTimeKind.Unspecified).AddTicks(550), new DateTime(2020, 8, 18, 0, 25, 10, 483, DateTimeKind.Unspecified).AddTicks(4100), "Boston's most advanced compression wear technology increases muscle oxygenation, stabilizes active muscles", "Sleek Fresh Pizza", 6 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 31, new DateTime(2018, 12, 9, 19, 10, 27, 205, DateTimeKind.Unspecified).AddTicks(737), new DateTime(2021, 4, 10, 16, 44, 11, 645, DateTimeKind.Unspecified).AddTicks(2297), "The Nagasaki Lander is the trademarked name of several series of Nagasaki sport bikes, that started with the 1984 ABC800J", "Handcrafted Fresh Cheese", 8 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 61, new DateTime(2020, 2, 25, 2, 5, 21, 281, DateTimeKind.Unspecified).AddTicks(3064), new DateTime(2021, 8, 28, 21, 0, 5, 898, DateTimeKind.Unspecified).AddTicks(1425), "The beautiful range of Apple Naturalé that has an exciting mix of natural ingredients. With the Goodness of 100% Natural Ingredients", "Generic Metal Keyboard", 8 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 1, new DateTime(2021, 6, 3, 4, 4, 31, 38, DateTimeKind.Unspecified).AddTicks(6223), new DateTime(2021, 7, 2, 19, 4, 8, 360, DateTimeKind.Unspecified).AddTicks(9877), "The Football Is Good For Training And Recreational Purposes", "Generic Metal Salad", 9 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 72, new DateTime(2019, 9, 11, 19, 56, 13, 135, DateTimeKind.Unspecified).AddTicks(2785), new DateTime(2021, 1, 22, 6, 38, 24, 720, DateTimeKind.Unspecified).AddTicks(1399), "Ergonomic executive chair upholstered in bonded black leather and PVC padded seat and back for all-day comfort and support", "Handcrafted Wooden Table", 2 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 49, new DateTime(2019, 6, 21, 1, 31, 57, 532, DateTimeKind.Unspecified).AddTicks(9197), new DateTime(2019, 10, 9, 9, 27, 28, 767, DateTimeKind.Unspecified).AddTicks(8624), "New ABC 13 9370, 13.3, 5th Gen CoreA5-8250U, 8GB RAM, 256GB SSD, power UHD Graphics, OS 10 Home, OS Office A & J 2016", "Sleek Fresh Salad", 3 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 21, new DateTime(2019, 2, 28, 9, 43, 58, 137, DateTimeKind.Unspecified).AddTicks(8439), new DateTime(2021, 4, 20, 19, 37, 51, 646, DateTimeKind.Unspecified).AddTicks(8301), "Boston's most advanced compression wear technology increases muscle oxygenation, stabilizes active muscles", "Refined Frozen Towels", 7 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name" },
                values: new object[] { 49, new DateTime(2019, 7, 20, 15, 2, 48, 156, DateTimeKind.Unspecified).AddTicks(8078), new DateTime(2021, 8, 13, 18, 39, 48, 191, DateTimeKind.Unspecified).AddTicks(8780), "The beautiful range of Apple Naturalé that has an exciting mix of natural ingredients. With the Goodness of 100% Natural Ingredients", "Intelligent Steel Computer" });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 25, new DateTime(2021, 2, 12, 19, 19, 33, 177, DateTimeKind.Unspecified).AddTicks(5822), new DateTime(2021, 7, 17, 13, 53, 44, 179, DateTimeKind.Unspecified).AddTicks(6439), "The slim & simple Maple Gaming Keyboard from Dev Byte comes with a sleek body and 7- Color RGB LED Back-lighting for smart functionality", "Unbranded Rubber Bacon", 2 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 65, new DateTime(2018, 10, 22, 14, 19, 46, 374, DateTimeKind.Unspecified).AddTicks(3452), new DateTime(2019, 12, 16, 13, 22, 18, 483, DateTimeKind.Unspecified).AddTicks(8487), "The beautiful range of Apple Naturalé that has an exciting mix of natural ingredients. With the Goodness of 100% Natural Ingredients", "Intelligent Granite Gloves", 5 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 20, new DateTime(2021, 2, 27, 17, 29, 36, 331, DateTimeKind.Unspecified).AddTicks(4340), new DateTime(2021, 3, 13, 7, 13, 49, 821, DateTimeKind.Unspecified).AddTicks(984), "The automobile layout consists of a front-engine design, with transaxle-type transmissions mounted at the rear of the engine and four wheel drive", "Intelligent Soft Fish", 3 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 3, new DateTime(2021, 6, 30, 3, 27, 58, 379, DateTimeKind.Unspecified).AddTicks(5698), new DateTime(2021, 10, 18, 7, 19, 7, 133, DateTimeKind.Unspecified).AddTicks(9580), "The beautiful range of Apple Naturalé that has an exciting mix of natural ingredients. With the Goodness of 100% Natural Ingredients", "Licensed Metal Pizza", 6 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 36, new DateTime(2020, 11, 19, 11, 34, 32, 181, DateTimeKind.Unspecified).AddTicks(8623), new DateTime(2021, 8, 11, 8, 45, 20, 897, DateTimeKind.Unspecified).AddTicks(7966), "Andy shoes are designed to keeping in mind durability as well as trends, the most stylish range of shoes & sandals", "Practical Steel Ball", 2 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 24, new DateTime(2021, 5, 24, 19, 21, 17, 544, DateTimeKind.Unspecified).AddTicks(6598), new DateTime(2021, 11, 3, 16, 34, 58, 177, DateTimeKind.Unspecified).AddTicks(142), "Boston's most advanced compression wear technology increases muscle oxygenation, stabilizes active muscles", "Small Steel Hat", 5 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 12, new DateTime(2020, 3, 6, 1, 42, 0, 249, DateTimeKind.Unspecified).AddTicks(3399), new DateTime(2021, 5, 10, 21, 40, 58, 473, DateTimeKind.Unspecified).AddTicks(7050), "The beautiful range of Apple Naturalé that has an exciting mix of natural ingredients. With the Goodness of 100% Natural Ingredients", "Licensed Soft Chicken", 4 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 27, new DateTime(2019, 8, 31, 13, 32, 26, 472, DateTimeKind.Unspecified).AddTicks(575), new DateTime(2021, 2, 10, 17, 58, 59, 303, DateTimeKind.Unspecified).AddTicks(5051), "Carbonite web goalkeeper gloves are ergonomically designed to give easy fit", "Handmade Soft Hat", 1 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 15, new DateTime(2021, 3, 24, 20, 12, 17, 41, DateTimeKind.Unspecified).AddTicks(5575), new DateTime(2021, 4, 18, 15, 5, 4, 481, DateTimeKind.Unspecified).AddTicks(3638), "New range of formal shirts are designed keeping you in mind. With fits and styling that will make you stand apart", "Awesome Metal Table", 3 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 61, new DateTime(2020, 9, 2, 1, 15, 5, 174, DateTimeKind.Unspecified).AddTicks(4569), new DateTime(2021, 1, 29, 20, 0, 26, 930, DateTimeKind.Unspecified).AddTicks(5821), "The automobile layout consists of a front-engine design, with transaxle-type transmissions mounted at the rear of the engine and four wheel drive", "Tasty Steel Car", 2 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 73, new DateTime(2019, 10, 17, 15, 18, 22, 73, DateTimeKind.Unspecified).AddTicks(4631), new DateTime(2020, 1, 1, 3, 22, 57, 0, DateTimeKind.Unspecified).AddTicks(1131), "The slim & simple Maple Gaming Keyboard from Dev Byte comes with a sleek body and 7- Color RGB LED Back-lighting for smart functionality", "Handcrafted Steel Car", 2 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 51, new DateTime(2019, 3, 24, 11, 59, 28, 698, DateTimeKind.Unspecified).AddTicks(8928), new DateTime(2019, 4, 13, 0, 10, 7, 914, DateTimeKind.Unspecified).AddTicks(8056), "Boston's most advanced compression wear technology increases muscle oxygenation, stabilizes active muscles", "Practical Rubber Ball", 2 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 3, new DateTime(2020, 2, 25, 6, 26, 22, 592, DateTimeKind.Unspecified).AddTicks(4360), new DateTime(2021, 2, 18, 6, 48, 55, 713, DateTimeKind.Unspecified).AddTicks(591), "Carbonite web goalkeeper gloves are ergonomically designed to give easy fit", "Generic Fresh Mouse", 3 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name" },
                values: new object[] { 24, new DateTime(2021, 3, 16, 13, 45, 28, 497, DateTimeKind.Unspecified).AddTicks(6680), new DateTime(2021, 8, 17, 18, 33, 2, 716, DateTimeKind.Unspecified).AddTicks(5105), "The automobile layout consists of a front-engine design, with transaxle-type transmissions mounted at the rear of the engine and four wheel drive", "Licensed Rubber Salad" });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 75, new DateTime(2020, 7, 23, 22, 50, 29, 733, DateTimeKind.Unspecified).AddTicks(3253), new DateTime(2021, 2, 5, 21, 43, 53, 307, DateTimeKind.Unspecified).AddTicks(9389), "The slim & simple Maple Gaming Keyboard from Dev Byte comes with a sleek body and 7- Color RGB LED Back-lighting for smart functionality", "Small Concrete Sausages", 2 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 80, new DateTime(2019, 8, 10, 17, 18, 25, 302, DateTimeKind.Unspecified).AddTicks(9605), new DateTime(2021, 6, 12, 20, 29, 37, 571, DateTimeKind.Unspecified).AddTicks(1681), "Carbonite web goalkeeper gloves are ergonomically designed to give easy fit", "Rustic Plastic Gloves", 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 8, 8, 14, 32, 36, 33, DateTimeKind.Unspecified).AddTicks(1362), "Nostrum atque blanditiis sed quos voluptas voluptatem.", new DateTime(2020, 8, 15, 1, 1, 51, 980, DateTimeKind.Unspecified).AddTicks(6131), "Eaque natus aut aut.", 56, 17, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 4, 15, 2, 7, 3, 599, DateTimeKind.Unspecified).AddTicks(6773), "Ratione consequatur odio eligendi rerum voluptatem omnis culpa quia.", new DateTime(2021, 1, 19, 14, 0, 12, 260, DateTimeKind.Unspecified).AddTicks(5127), "Qui numquam dicta vitae.", 3, 5, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2018, 8, 9, 1, 4, 25, 599, DateTimeKind.Unspecified).AddTicks(9500), "Cum in quia omnis non nisi et ullam error.", new DateTime(2018, 10, 22, 10, 58, 25, 861, DateTimeKind.Unspecified).AddTicks(4636), "Et saepe totam aut.", 10, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 4, 11, 14, 22, 54, 929, DateTimeKind.Unspecified).AddTicks(6290), "Maiores repudiandae sed sequi aut iste dicta expedita odit.", new DateTime(2021, 4, 13, 13, 49, 0, 5, DateTimeKind.Unspecified).AddTicks(1134), "Dolorem impedit mollitia animi.", 55, 33, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 5, 6, 9, 5, 9, 767, DateTimeKind.Unspecified).AddTicks(848), "Magnam sit quia ut temporibus exercitationem voluptas.", new DateTime(2019, 11, 2, 18, 2, 14, 637, DateTimeKind.Unspecified).AddTicks(2222), "Placeat nobis soluta dolorem ut.", 70, 12 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 5, 5, 26, 45, 165, DateTimeKind.Unspecified).AddTicks(203), "Placeat ut aliquam dolore consectetur possimus dignissimos qui voluptas enim commodi maiores atque accusantium.", new DateTime(2020, 11, 5, 18, 14, 12, 202, DateTimeKind.Unspecified).AddTicks(4107), "Esse voluptas adipisci.", 72, 18, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 12, 3, 6, 24, 0, 751, DateTimeKind.Unspecified).AddTicks(4666), "Voluptatem ea est est voluptatem debitis voluptates enim.", new DateTime(2021, 5, 30, 18, 44, 11, 276, DateTimeKind.Unspecified).AddTicks(4669), "Et eius.", 16, 40, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 8, 9, 3, 40, 21, 397, DateTimeKind.Unspecified).AddTicks(7677), "Quisquam praesentium accusantium ea enim itaque magni accusamus vel illo soluta accusamus.", new DateTime(2020, 12, 27, 2, 37, 46, 111, DateTimeKind.Unspecified).AddTicks(8758), "Accusamus veritatis voluptatem.", 75, 5, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 6, 2, 14, 18, 22, 11, DateTimeKind.Unspecified).AddTicks(587), "Consequatur iusto voluptas explicabo incidunt est.", new DateTime(2020, 10, 6, 6, 56, 36, 428, DateTimeKind.Unspecified).AddTicks(4667), "Et odit dolor rerum quam.", 72, 18, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 11, 23, 6, 27, 2, 314, DateTimeKind.Unspecified).AddTicks(5658), "Veritatis vero officia magni iusto commodi ea corrupti ut.", new DateTime(2020, 9, 1, 19, 9, 36, 645, DateTimeKind.Unspecified).AddTicks(2720), "Ut sint quae placeat occaecati.", 63, 40 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 2, 6, 18, 23, 6, 597, DateTimeKind.Unspecified).AddTicks(9422), "Ipsum aperiam quidem ipsa est aut incidunt quo blanditiis assumenda rem sit voluptas.", new DateTime(2021, 4, 29, 23, 13, 12, 517, DateTimeKind.Unspecified).AddTicks(6153), "Accusantium aperiam.", 45, 31, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 6, 3, 17, 11, 29, 455, DateTimeKind.Unspecified).AddTicks(978), "Maxime nihil totam aut deleniti ut sed porro non rerum ipsum culpa.", new DateTime(2021, 6, 10, 23, 4, 36, 140, DateTimeKind.Unspecified).AddTicks(6946), "Labore repellat ea.", 47, 20, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 30, 20, 43, 42, 868, DateTimeKind.Unspecified).AddTicks(9347), "Consequatur sunt quo saepe deleniti laborum quibusdam.", new DateTime(2019, 4, 11, 14, 12, 27, 922, DateTimeKind.Unspecified).AddTicks(1951), "Est et harum perferendis sit.", 50, 36, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 6, 24, 0, 20, 7, 847, DateTimeKind.Unspecified).AddTicks(5188), "Suscipit illo quaerat dolores tempore ipsum aut accusantium et enim quo nisi enim.", new DateTime(2021, 6, 24, 19, 28, 10, 669, DateTimeKind.Unspecified).AddTicks(7700), "Ut facere.", 27, 20, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 10, 8, 4, 37, 13, 1, DateTimeKind.Unspecified).AddTicks(6326), "Animi dicta nostrum consequatur eos et in aut.", new DateTime(2021, 10, 13, 23, 0, 31, 599, DateTimeKind.Unspecified).AddTicks(4383), "Voluptates consectetur tempora.", 47, 28 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 9, 9, 0, 26, 26, 45, DateTimeKind.Unspecified).AddTicks(2945), "Ipsum omnis blanditiis est ad quis.", new DateTime(2021, 10, 23, 1, 48, 57, 754, DateTimeKind.Unspecified).AddTicks(5263), "Velit totam quidem totam nobis.", 68, 7, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 6, 28, 17, 29, 46, 295, DateTimeKind.Unspecified).AddTicks(4301), "Ea sapiente neque laudantium sed neque velit ex molestiae.", new DateTime(2021, 7, 2, 5, 48, 2, 955, DateTimeKind.Unspecified).AddTicks(3264), "Aut qui distinctio quae.", 53, 20, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 2, 18, 1, 21, 2, 539, DateTimeKind.Unspecified).AddTicks(1785), "Harum soluta sed at cumque accusamus ullam architecto ullam ullam praesentium possimus explicabo.", new DateTime(2021, 7, 8, 15, 7, 31, 8, DateTimeKind.Unspecified).AddTicks(7463), "Quas adipisci et.", 42, 7 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 12, 14, 23, 39, 24, 40, DateTimeKind.Unspecified).AddTicks(4438), "Aut consectetur quidem rerum itaque ea fugit sint possimus.", new DateTime(2021, 1, 21, 19, 48, 49, 35, DateTimeKind.Unspecified).AddTicks(8400), "Delectus et aliquam magnam.", 9, 34, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 9, 6, 22, 6, 7, 62, DateTimeKind.Unspecified).AddTicks(9049), "Inventore totam quisquam aut quod omnis eum non voluptatibus et aspernatur quia vel molestiae.", new DateTime(2020, 9, 28, 17, 31, 36, 6, DateTimeKind.Unspecified).AddTicks(502), "Deserunt et dicta.", 29, 8, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 24, 2, 2, 30, 9, DateTimeKind.Unspecified).AddTicks(6755), "Alias aliquam id iure qui ea veritatis aut veritatis non.", new DateTime(2021, 7, 28, 0, 8, 42, 227, DateTimeKind.Unspecified).AddTicks(581), "Saepe sit.", 25, 15, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 10, 6, 9, 34, 45, 128, DateTimeKind.Unspecified).AddTicks(2621), "Harum aperiam ut rerum inventore hic ea omnis hic aut debitis.", new DateTime(2020, 10, 6, 22, 11, 53, 386, DateTimeKind.Unspecified).AddTicks(6636), "Dolorem omnis voluptatum dolores quis.", 32, 37, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 8, 24, 1, 21, 21, 734, DateTimeKind.Unspecified).AddTicks(4651), "Ut corporis delectus velit laboriosam amet.", new DateTime(2020, 2, 23, 2, 56, 49, 235, DateTimeKind.Unspecified).AddTicks(7481), "Non sint expedita eaque cum.", 28, 24 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 26, 18, 19, 27, 458, DateTimeKind.Unspecified).AddTicks(6217), "Laborum excepturi culpa nisi voluptatem a.", new DateTime(2021, 8, 29, 14, 59, 37, 834, DateTimeKind.Unspecified).AddTicks(1378), "Cumque sed vel ut.", 50, 15, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 12, 7, 6, 51, 43, 317, DateTimeKind.Unspecified).AddTicks(463), "Suscipit explicabo facere sit commodi commodi.", new DateTime(2021, 3, 11, 5, 4, 37, 393, DateTimeKind.Unspecified).AddTicks(6614), "Eius harum dolorum tenetur aut.", 1, 24, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 5, 26, 21, 0, 27, 834, DateTimeKind.Unspecified).AddTicks(3968), "Ipsa non aspernatur nobis sint nisi animi sint ducimus velit delectus rem eum.", new DateTime(2021, 7, 1, 16, 8, 18, 883, DateTimeKind.Unspecified).AddTicks(3256), "Ad quia sit.", 7, 25, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 6, 25, 1, 49, 34, 69, DateTimeKind.Unspecified).AddTicks(7087), "Numquam placeat corrupti quis maiores soluta officia.", new DateTime(2021, 6, 28, 5, 46, 24, 327, DateTimeKind.Unspecified).AddTicks(7528), "Magnam ea deserunt.", 47, 20, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 8, 17, 8, 39, 21, 650, DateTimeKind.Unspecified).AddTicks(65), "Asperiores quos ut sunt tenetur in quia.", new DateTime(2018, 9, 9, 12, 29, 11, 353, DateTimeKind.Unspecified).AddTicks(5891), "At voluptas.", 49, 1, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2020, 10, 11, 23, 53, 2, 682, DateTimeKind.Unspecified).AddTicks(5573), "Qui vel aut recusandae et illo laborum fugiat eos repudiandae.", new DateTime(2020, 10, 23, 16, 15, 5, 38, DateTimeKind.Unspecified).AddTicks(3383), "Facilis non.", 51, 19 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 12, 29, 1, 7, 2, 70, DateTimeKind.Unspecified).AddTicks(823), "Adipisci illum fuga atque et suscipit.", new DateTime(2021, 1, 5, 1, 12, 32, 507, DateTimeKind.Unspecified).AddTicks(4023), "Consequatur delectus deleniti omnis quia.", 67, 14, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 5, 15, 23, 10, 7, 331, DateTimeKind.Unspecified).AddTicks(6524), "Aut ea impedit ipsa non quam qui aut voluptatem sint et modi dolorem ipsum.", new DateTime(2018, 6, 13, 3, 41, 14, 833, DateTimeKind.Unspecified).AddTicks(7056), "Dolores quo aut reprehenderit nobis.", 25, 13, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 1, 22, 13, 14, 47, 966, DateTimeKind.Unspecified).AddTicks(5951), "Accusamus blanditiis ut sint voluptas aut minima ut debitis quisquam enim.", new DateTime(2021, 10, 21, 19, 32, 24, 689, DateTimeKind.Unspecified).AddTicks(6481), "Vel ad atque aut id.", 19, 7 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2018, 4, 15, 2, 29, 20, 778, DateTimeKind.Unspecified).AddTicks(8877), "Libero quidem nihil cupiditate et dicta quod consequuntur id sint eum iusto.", new DateTime(2018, 6, 4, 4, 31, 3, 740, DateTimeKind.Unspecified).AddTicks(423), "Fugiat id vitae.", 9, 13 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2020, 4, 6, 15, 51, 16, 653, DateTimeKind.Unspecified).AddTicks(6737), "Sed aperiam sit aspernatur quae qui sapiente et ratione occaecati recusandae.", new DateTime(2020, 5, 25, 5, 14, 24, 605, DateTimeKind.Unspecified).AddTicks(126), "Enim rerum fuga nostrum.", 75, 9 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 1, 8, 43, 25, 52, DateTimeKind.Unspecified).AddTicks(3043), "Similique expedita assumenda sed numquam sequi laudantium eos veritatis vel eaque officia.", new DateTime(2021, 7, 2, 8, 29, 19, 597, DateTimeKind.Unspecified).AddTicks(5337), "Ut velit ad aut deleniti.", 44, 20, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 7, 6, 2, 22, 59, 803, DateTimeKind.Unspecified).AddTicks(8011), "Enim repudiandae nihil quod eum ea non perferendis quibusdam.", new DateTime(2019, 9, 16, 13, 19, 10, 321, DateTimeKind.Unspecified).AddTicks(7828), "Voluptatem nobis sint eum.", 23, 26, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 21, 5, 54, 30, 234, DateTimeKind.Unspecified).AddTicks(8136), "Accusamus dolorem dolor est et non ut pariatur accusamus.", new DateTime(2019, 11, 29, 3, 51, 59, 734, DateTimeKind.Unspecified).AddTicks(4877), "Placeat dolores soluta.", 15, 26, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 7, 5, 4, 36, 17, 228, DateTimeKind.Unspecified).AddTicks(6881), "Beatae id pariatur commodi et tenetur dolorem aut ut.", new DateTime(2020, 8, 7, 18, 43, 50, 913, DateTimeKind.Unspecified).AddTicks(7868), "Praesentium quia molestias.", 1, 17, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 5, 15, 9, 2, 22, 459, DateTimeKind.Unspecified).AddTicks(6482), "Non atque omnis nisi vel aliquam voluptas occaecati nihil dolores illum aliquam et.", new DateTime(2021, 5, 15, 23, 34, 34, 319, DateTimeKind.Unspecified).AddTicks(1476), "Excepturi cupiditate provident nemo.", 37, 4, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId" },
                values: new object[] { new DateTime(2021, 4, 26, 8, 32, 38, 960, DateTimeKind.Unspecified).AddTicks(7470), "Repellendus quod omnis autem voluptatem cum dolor non eveniet.", new DateTime(2021, 5, 16, 0, 39, 0, 505, DateTimeKind.Unspecified).AddTicks(4105), "Et id aspernatur ea.", 41 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 41,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 8, 20, 13, 6, 56, 644, DateTimeKind.Unspecified).AddTicks(1842), "Dolorem et nobis ea saepe corrupti et doloribus et inventore molestiae.", new DateTime(2020, 12, 17, 2, 3, 42, 734, DateTimeKind.Unspecified).AddTicks(2657), "Maiores fugit est est voluptate.", 39, 5, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 42,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 8, 5, 2, 39, 52, 408, DateTimeKind.Unspecified).AddTicks(463), "Necessitatibus ea provident consectetur alias praesentium sint voluptatum omnis.", new DateTime(2019, 10, 6, 10, 7, 21, 5, DateTimeKind.Unspecified).AddTicks(1515), "Quaerat placeat.", 68, 22 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 43,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 1, 28, 10, 41, 42, 130, DateTimeKind.Unspecified).AddTicks(8881), "Itaque et et praesentium voluptatum et hic ut excepturi et iure itaque.", new DateTime(2021, 2, 9, 0, 6, 34, 164, DateTimeKind.Unspecified).AddTicks(283), "Sunt sunt iure consequatur amet.", 40, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 44,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 4, 5, 7, 41, 11, 610, DateTimeKind.Unspecified).AddTicks(9550), "Fugit delectus sit dolorem veniam mollitia perspiciatis iure nam.", new DateTime(2019, 4, 12, 15, 8, 5, 144, DateTimeKind.Unspecified).AddTicks(2935), "Iusto aut officia.", 10, 36, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 45,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 2, 1, 8, 37, 49, 776, DateTimeKind.Unspecified).AddTicks(936), "Voluptatum nihil voluptatum ut magni quos.", new DateTime(2021, 2, 5, 14, 6, 4, 85, DateTimeKind.Unspecified).AddTicks(3582), "Minus cupiditate qui error magni.", 18, 39 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 46,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 3, 26, 13, 26, 10, 285, DateTimeKind.Unspecified).AddTicks(1277), "Dignissimos enim repellendus ex et quia ea quos autem velit possimus aliquid.", new DateTime(2019, 3, 27, 9, 52, 28, 577, DateTimeKind.Unspecified).AddTicks(7058), "Et magni.", 33, 10 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 47,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 2, 20, 8, 49, 8, 34, DateTimeKind.Unspecified).AddTicks(3735), "Voluptatem voluptatem aut sunt autem soluta delectus aliquid excepturi qui.", new DateTime(2020, 6, 2, 6, 27, 20, 696, DateTimeKind.Unspecified).AddTicks(1885), "In quidem voluptatem.", 49, 40, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 48,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 1, 12, 2, 38, 34, 123, DateTimeKind.Unspecified).AddTicks(5291), "Non aspernatur corrupti autem qui minus.", new DateTime(2021, 1, 26, 8, 40, 17, 335, DateTimeKind.Unspecified).AddTicks(9351), "Voluptas sint.", 54, 34 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 49,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 4, 2, 1, 55, 41, 104, DateTimeKind.Unspecified).AddTicks(5564), "Quae harum aliquam tempora voluptas quisquam non accusantium ut corporis commodi ipsa voluptatum nam.", new DateTime(2020, 3, 21, 12, 54, 26, 369, DateTimeKind.Unspecified).AddTicks(2796), "Dolores quam.", 45, 18, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 50,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 10, 16, 1, 20, 25, 983, DateTimeKind.Unspecified).AddTicks(6812), "A voluptas delectus in architecto perspiciatis pariatur aliquid quos omnis iure ipsum.", new DateTime(2020, 10, 7, 21, 2, 46, 304, DateTimeKind.Unspecified).AddTicks(1934), "Dignissimos omnis.", 29, 40 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 51,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 8, 17, 19, 17, 19, 325, DateTimeKind.Unspecified).AddTicks(175), "Ex odio blanditiis id provident dolorum recusandae quis praesentium enim at exercitationem.", new DateTime(2019, 9, 14, 4, 46, 48, 213, DateTimeKind.Unspecified).AddTicks(458), "Architecto officia quasi dolor.", 39, 16, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 52,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 5, 16, 8, 40, 27, 50, DateTimeKind.Unspecified).AddTicks(9775), "Dicta dolores blanditiis et aperiam necessitatibus quia consequatur.", new DateTime(2021, 6, 6, 0, 26, 40, 801, DateTimeKind.Unspecified).AddTicks(2000), "Tenetur officiis.", 12, 15, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 53,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 6, 27, 1, 53, 29, 399, DateTimeKind.Unspecified).AddTicks(1824), "Natus ea non ut quia distinctio impedit tempora non incidunt mollitia sed.", new DateTime(2020, 8, 30, 7, 11, 31, 396, DateTimeKind.Unspecified).AddTicks(9961), "Eos dolorum earum ipsam.", 76, 5, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 54,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 7, 16, 11, 56, 10, 704, DateTimeKind.Unspecified).AddTicks(9488), "Est consequatur explicabo vitae ratione fugit labore facere alias fugiat sed minus.", new DateTime(2020, 7, 19, 22, 34, 59, 664, DateTimeKind.Unspecified).AddTicks(3473), "Ea dolores.", 48, 9, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 55,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 1, 25, 3, 27, 32, 376, DateTimeKind.Unspecified).AddTicks(7712), "Dolores assumenda est deserunt enim est nisi adipisci dolorum qui illum repellat fugiat in.", new DateTime(2021, 1, 27, 16, 42, 34, 134, DateTimeKind.Unspecified).AddTicks(8928), "Qui quia distinctio.", 38, 34 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 56,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 4, 7, 17, 9, 3, 657, DateTimeKind.Unspecified).AddTicks(3925), "Saepe mollitia error aut aliquid libero est quia quasi minus accusamus quia iusto.", new DateTime(2020, 9, 10, 8, 12, 17, 986, DateTimeKind.Unspecified).AddTicks(4719), "Quia in veritatis.", 65, 23, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 57,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 9, 7, 22, 41, 13, 217, DateTimeKind.Unspecified).AddTicks(5410), "Deleniti laudantium voluptatem omnis ex ipsa vero consectetur.", new DateTime(2021, 1, 17, 13, 43, 15, 962, DateTimeKind.Unspecified).AddTicks(7109), "Tenetur repudiandae.", 56, 32, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 58,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 26, 14, 52, 54, 169, DateTimeKind.Unspecified).AddTicks(6045), "Facilis molestiae aut non excepturi vel quam cumque hic veritatis in amet odit.", new DateTime(2019, 3, 28, 8, 15, 27, 205, DateTimeKind.Unspecified).AddTicks(2335), "Nesciunt ut nulla hic.", 64, 36, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 59,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 4, 16, 3, 42, 3, 86, DateTimeKind.Unspecified).AddTicks(9563), "Dolores et deserunt eos dolorem ex dolor delectus voluptatem ratione enim exercitationem vel itaque.", new DateTime(2021, 4, 16, 23, 54, 14, 510, DateTimeKind.Unspecified).AddTicks(7232), "Ipsam minus voluptatem aut.", 73, 33, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 60,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 10, 1, 17, 26, 3, 949, DateTimeKind.Unspecified).AddTicks(8295), "A voluptatem error veritatis dignissimos enim quidem architecto voluptas quae nesciunt.", new DateTime(2019, 10, 10, 10, 2, 39, 863, DateTimeKind.Unspecified).AddTicks(3762), "Reprehenderit unde.", 43, 16 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 61,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 9, 22, 23, 27, 25, 208, DateTimeKind.Unspecified).AddTicks(9031), "Autem id neque voluptatibus quia autem reprehenderit velit facere omnis officia.", new DateTime(2018, 10, 12, 21, 45, 57, 39, DateTimeKind.Unspecified).AddTicks(9884), "Aut et molestiae sapiente.", 24, 1, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 62,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2020, 11, 25, 17, 38, 1, 658, DateTimeKind.Unspecified).AddTicks(7158), "Eius unde voluptates est vel aut enim dolore.", new DateTime(2020, 12, 20, 3, 43, 54, 903, DateTimeKind.Unspecified).AddTicks(1215), "Eligendi et quasi earum.", 52, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 63,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 16, 21, 51, 22, 67, DateTimeKind.Unspecified).AddTicks(3780), "Exercitationem id et debitis sequi porro non ullam ratione quod ullam saepe cum quia.", new DateTime(2020, 7, 5, 16, 48, 1, 341, DateTimeKind.Unspecified).AddTicks(5166), "Eveniet laudantium aspernatur quaerat soluta.", 80, 21, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 64,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2020, 12, 16, 22, 36, 3, 558, DateTimeKind.Unspecified).AddTicks(4874), "Exercitationem amet omnis qui enim mollitia consequatur iure accusantium.", new DateTime(2020, 12, 18, 16, 2, 59, 259, DateTimeKind.Unspecified).AddTicks(4205), "Aut quas mollitia delectus.", 37, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 65,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 5, 26, 5, 32, 51, 547, DateTimeKind.Unspecified).AddTicks(9898), "Maxime distinctio cupiditate enim veniam reprehenderit non ut.", new DateTime(2021, 5, 26, 9, 48, 8, 955, DateTimeKind.Unspecified).AddTicks(2646), "Officiis aut aspernatur eius.", 67, 4, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 66,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 21, 3, 22, 20, 209, DateTimeKind.Unspecified).AddTicks(2021), "Debitis qui non quas et ipsum ut accusamus eaque fugit sit quo explicabo ea.", new DateTime(2019, 10, 7, 18, 35, 50, 154, DateTimeKind.Unspecified).AddTicks(2921), "Eos provident sequi.", 54, 16, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 67,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 1, 14, 15, 16, 39, 544, DateTimeKind.Unspecified).AddTicks(7990), "Nostrum numquam neque id voluptates sed.", new DateTime(2021, 2, 6, 19, 9, 43, 681, DateTimeKind.Unspecified).AddTicks(586), "Fugit aut et.", 18, 24, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 68,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 3, 15, 51, 21, 21, DateTimeKind.Unspecified).AddTicks(239), "Ex voluptate esse ex esse recusandae.", new DateTime(2019, 10, 1, 7, 48, 10, 166, DateTimeKind.Unspecified).AddTicks(1602), "Qui labore.", 16, 16, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 69,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 6, 8, 20, 50, 37, 201, DateTimeKind.Unspecified).AddTicks(4921), "Eum non iusto consequatur ullam sapiente.", new DateTime(2021, 7, 2, 13, 36, 56, 978, DateTimeKind.Unspecified).AddTicks(218), "Tempora accusamus est temporibus repellat.", 79, 20 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 70,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 9, 10, 11, 24, 43, 669, DateTimeKind.Unspecified).AddTicks(9412), "Fuga nemo inventore cumque cupiditate et dolorem voluptates dolores.", new DateTime(2020, 12, 23, 10, 57, 40, 241, DateTimeKind.Unspecified).AddTicks(514), "Corporis dolore.", 30, 32, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 71,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 12, 29, 14, 23, 42, 888, DateTimeKind.Unspecified).AddTicks(8988), "Error accusantium sunt aliquid odit qui vero saepe id reprehenderit id sed sit hic.", new DateTime(2021, 5, 31, 5, 7, 9, 463, DateTimeKind.Unspecified).AddTicks(4865), "Aperiam voluptatem tempora.", 8, 19, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 72,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 6, 27, 1, 53, 25, 559, DateTimeKind.Unspecified).AddTicks(1082), "Tempora sit magnam qui sequi recusandae a distinctio a corporis soluta quis illo tenetur.", new DateTime(2021, 8, 5, 9, 25, 51, 618, DateTimeKind.Unspecified).AddTicks(163), "Nihil nam corporis.", 35, 38, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 73,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 12, 22, 9, 30, 44, 656, DateTimeKind.Unspecified).AddTicks(7452), "Assumenda nihil ea incidunt facere in eveniet.", new DateTime(2021, 6, 20, 19, 36, 5, 134, DateTimeKind.Unspecified).AddTicks(8235), "Omnis in rerum omnis.", 17, 29, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 74,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2021, 3, 30, 11, 5, 25, 405, DateTimeKind.Unspecified).AddTicks(5295), "Architecto et accusantium enim sunt unde id soluta voluptas.", new DateTime(2021, 4, 14, 12, 24, 42, 926, DateTimeKind.Unspecified).AddTicks(8507), "Id et.", 15, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 75,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 7, 3, 22, 50, 51, 66, DateTimeKind.Unspecified).AddTicks(5561), "Omnis et sed qui repellendus est quam eum.", new DateTime(2021, 7, 7, 8, 19, 26, 146, DateTimeKind.Unspecified).AddTicks(7073), "Eligendi corrupti id iure.", 51, 7 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 76,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 3, 22, 18, 47, 9, 457, DateTimeKind.Unspecified).AddTicks(1437), "Quos magni eius aspernatur est iure tenetur repudiandae distinctio itaque blanditiis.", new DateTime(2021, 5, 30, 12, 52, 44, 357, DateTimeKind.Unspecified).AddTicks(5031), "Est sit.", 28, 19, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 77,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 7, 13, 5, 8, 31, 971, DateTimeKind.Unspecified).AddTicks(588), "Odio magni omnis molestias quibusdam dolorem quidem est nihil corrupti.", new DateTime(2019, 7, 13, 23, 1, 54, 483, DateTimeKind.Unspecified).AddTicks(7009), "Aut facilis ipsa ullam.", 14, 13 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 78,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 21, 22, 33, 2, 981, DateTimeKind.Unspecified).AddTicks(1586), "Vel quia impedit sapiente ex dicta sit sed.", new DateTime(2021, 7, 25, 22, 10, 48, 406, DateTimeKind.Unspecified).AddTicks(743), "Eum ipsa.", 24, 28, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 79,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 10, 16, 16, 3, 24, 861, DateTimeKind.Unspecified).AddTicks(7931), "Numquam deserunt repellendus ad illo ut delectus sequi.", new DateTime(2018, 10, 18, 8, 9, 0, 926, DateTimeKind.Unspecified).AddTicks(7098), "Consequatur est sunt.", 35, 1, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 80,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 9, 22, 19, 45, 57, 973, DateTimeKind.Unspecified).AddTicks(3637), "Commodi minus dolorem quam nihil laudantium delectus sapiente omnis quas.", new DateTime(2021, 4, 9, 6, 56, 15, 306, DateTimeKind.Unspecified).AddTicks(1893), "Animi temporibus.", 63, 18, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 81,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 4, 21, 23, 18, 2, 842, DateTimeKind.Unspecified).AddTicks(1442), "Totam ut quasi voluptatum repellat sed beatae dolorum et dolores.", new DateTime(2021, 6, 4, 5, 42, 29, 479, DateTimeKind.Unspecified).AddTicks(1774), "Vel et.", 67, 25, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 82,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2020, 12, 16, 3, 35, 8, 119, DateTimeKind.Unspecified).AddTicks(6535), "Illum enim quas qui occaecati quia labore.", new DateTime(2021, 5, 14, 1, 36, 20, 551, DateTimeKind.Unspecified).AddTicks(6171), "A molestiae ut autem.", 35, 29 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 83,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 3, 8, 3, 44, 35, 570, DateTimeKind.Unspecified).AddTicks(981), "Aliquam ipsum eius nisi nulla quisquam laudantium non.", new DateTime(2021, 8, 28, 4, 17, 21, 828, DateTimeKind.Unspecified).AddTicks(1459), "Quia rerum.", 28, 15, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 84,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 2, 22, 4, 48, 3, 559, DateTimeKind.Unspecified).AddTicks(2877), "Similique maxime voluptatum beatae quo eveniet architecto qui.", new DateTime(2020, 5, 16, 21, 3, 11, 7, DateTimeKind.Unspecified).AddTicks(3137), "Sequi voluptates qui quia et.", 71, 40, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 85,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 6, 18, 17, 38, 42, 691, DateTimeKind.Unspecified).AddTicks(7026), "Minima explicabo pariatur sunt autem occaecati deserunt corporis quis laboriosam.", new DateTime(2021, 8, 7, 11, 26, 8, 316, DateTimeKind.Unspecified).AddTicks(6186), "Cumque facere nobis.", 54, 38, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 86,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 9, 2, 19, 6, 53, 884, DateTimeKind.Unspecified).AddTicks(2439), "Hic est quasi recusandae facilis impedit aliquid inventore asperiores magni.", new DateTime(2019, 9, 4, 17, 42, 45, 145, DateTimeKind.Unspecified).AddTicks(6896), "Est excepturi optio eos.", 45, 16 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 87,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2020, 9, 13, 11, 26, 49, 501, DateTimeKind.Unspecified).AddTicks(6300), "Sed non minus ducimus minima explicabo laudantium molestias neque et.", new DateTime(2020, 12, 4, 4, 14, 52, 486, DateTimeKind.Unspecified).AddTicks(2010), "Asperiores quis tenetur.", 27, 8 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 88,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 3, 5, 0, 46, 9, 462, DateTimeKind.Unspecified).AddTicks(6361), "Et dolore autem non unde quos et repellendus fugit laboriosam qui qui quod.", new DateTime(2021, 3, 12, 15, 1, 6, 195, DateTimeKind.Unspecified).AddTicks(8956), "Dolor rerum qui.", 50, 27, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 89,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 5, 12, 3, 32, 13, 131, DateTimeKind.Unspecified).AddTicks(4834), "Earum laboriosam soluta molestiae quo fugit animi ex rerum.", new DateTime(2021, 6, 2, 0, 33, 49, 197, DateTimeKind.Unspecified).AddTicks(1735), "Unde dolore nulla saepe.", 42, 6 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 90,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2020, 7, 8, 22, 58, 9, 840, DateTimeKind.Unspecified).AddTicks(3694), "Quae voluptatem necessitatibus aut voluptates cupiditate nesciunt maiores dolor error qui aliquid aut ut.", new DateTime(2020, 7, 11, 2, 37, 5, 139, DateTimeKind.Unspecified).AddTicks(3902), "Temporibus at cum qui qui.", 80, 9 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 91,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 1, 13, 22, 31, 50, 251, DateTimeKind.Unspecified).AddTicks(224), "Enim qui quisquam et eligendi similique qui quasi laudantium saepe voluptatem temporibus.", new DateTime(2021, 4, 23, 19, 16, 52, 758, DateTimeKind.Unspecified).AddTicks(6847), "Quis et mollitia velit.", 30, 19 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 92,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 7, 21, 44, 50, 405, DateTimeKind.Unspecified).AddTicks(8387), "Magnam est reiciendis ipsa id qui vitae.", new DateTime(2019, 9, 22, 21, 7, 52, 562, DateTimeKind.Unspecified).AddTicks(7403), "Culpa nulla earum.", 24, 22, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 93,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 6, 17, 4, 39, 5, 656, DateTimeKind.Unspecified).AddTicks(9392), "Rerum minima perspiciatis autem consectetur voluptatem ea enim voluptate corporis alias aperiam sit similique.", new DateTime(2021, 6, 21, 12, 31, 57, 910, DateTimeKind.Unspecified).AddTicks(4760), "Consequuntur odio sed atque.", 72, 30, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 94,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 9, 25, 3, 14, 50, 766, DateTimeKind.Unspecified).AddTicks(8067), "Ullam ipsam sequi neque praesentium alias magnam reiciendis officia.", new DateTime(2021, 1, 31, 13, 13, 1, 954, DateTimeKind.Unspecified).AddTicks(1531), "Delectus illo doloribus itaque.", 41, 37, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 95,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 2, 2, 12, 46, 33, 641, DateTimeKind.Unspecified).AddTicks(4361), "Nihil voluptates error quas voluptatibus voluptate incidunt fugiat doloremque necessitatibus.", new DateTime(2020, 10, 16, 20, 8, 40, 42, DateTimeKind.Unspecified).AddTicks(2193), "Nobis sit maiores dolor cupiditate.", 57, 12, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 96,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 6, 21, 8, 15, 50, 925, DateTimeKind.Unspecified).AddTicks(2599), "Placeat inventore et nam architecto ut ut provident.", new DateTime(2021, 6, 25, 5, 18, 47, 465, DateTimeKind.Unspecified).AddTicks(8023), "Odit hic.", 17, 38, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 97,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 12, 13, 11, 51, 54, 86, DateTimeKind.Unspecified).AddTicks(3893), "Expedita voluptatum deserunt voluptatem ratione iste perferendis quis doloribus pariatur est.", new DateTime(2018, 12, 18, 6, 12, 30, 845, DateTimeKind.Unspecified).AddTicks(376), "Fugiat mollitia ea.", 39, 10, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 98,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 3, 30, 16, 9, 25, 244, DateTimeKind.Unspecified).AddTicks(7254), "Debitis nostrum et qui qui beatae adipisci adipisci exercitationem corporis et ducimus.", new DateTime(2021, 8, 15, 18, 41, 9, 331, DateTimeKind.Unspecified).AddTicks(9800), "Blanditiis ad et corrupti delectus.", 70, 19, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 99,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 6, 19, 16, 19, 38, 214, DateTimeKind.Unspecified).AddTicks(4481), "Corrupti voluptate doloremque minus quidem vel quia nesciunt aut eligendi omnis porro.", new DateTime(2021, 7, 15, 16, 26, 27, 847, DateTimeKind.Unspecified).AddTicks(5385), "Nihil unde esse et.", 24, 30, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 100,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2020, 3, 31, 9, 48, 7, 500, DateTimeKind.Unspecified).AddTicks(3644), "Est ab ut in deserunt dolor esse corporis tenetur animi autem.", new DateTime(2020, 11, 29, 14, 12, 57, 273, DateTimeKind.Unspecified).AddTicks(8544), "Expedita ipsam earum et omnis.", 66, 40 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 101,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 6, 19, 5, 53, 53, 356, DateTimeKind.Unspecified).AddTicks(3787), "Harum quo laudantium esse facere possimus id placeat fuga harum praesentium a officia quo.", new DateTime(2020, 8, 25, 6, 19, 50, 752, DateTimeKind.Unspecified).AddTicks(3684), "Qui excepturi possimus quo doloribus.", 37, 37, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 102,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 4, 12, 3, 55, 56, 646, DateTimeKind.Unspecified).AddTicks(2144), "Nulla nisi et aliquid sit vero repellendus illo.", new DateTime(2020, 8, 4, 11, 30, 55, 146, DateTimeKind.Unspecified).AddTicks(842), "Magni ut ipsum qui earum.", 54, 37, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 103,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 3, 7, 8, 31, 933, DateTimeKind.Unspecified).AddTicks(7123), "Reprehenderit delectus aut ab in consectetur atque aliquam.", new DateTime(2019, 3, 26, 21, 15, 32, 676, DateTimeKind.Unspecified).AddTicks(7910), "Aliquid at facere ea.", 6, 10, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 104,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 4, 6, 23, 14, 12, 242, DateTimeKind.Unspecified).AddTicks(758), "Enim laborum id quia sed recusandae ut iusto tenetur laborum voluptates velit ad voluptatem.", new DateTime(2021, 4, 10, 4, 28, 55, 212, DateTimeKind.Unspecified).AddTicks(559), "Excepturi repudiandae voluptatem.", 70, 33, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 105,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 6, 29, 1, 30, 5, 553, DateTimeKind.Unspecified).AddTicks(8506), "Iste amet quia qui et saepe voluptatem harum dicta et deleniti.", new DateTime(2021, 8, 22, 14, 19, 1, 985, DateTimeKind.Unspecified).AddTicks(7442), "Tempora neque sed.", 4, 30, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 106,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 6, 9, 4, 48, 50, 581, DateTimeKind.Unspecified).AddTicks(3153), "Culpa id alias et labore quae aut est aut beatae consequatur.", new DateTime(2021, 6, 12, 17, 23, 58, 369, DateTimeKind.Unspecified).AddTicks(9209), "Eos ut accusamus nisi illum.", 30, 25, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 107,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 5, 16, 22, 38, 39, 196, DateTimeKind.Unspecified).AddTicks(1185), "Quis libero veritatis suscipit suscipit pariatur dolorem rem.", new DateTime(2021, 5, 23, 10, 54, 28, 555, DateTimeKind.Unspecified).AddTicks(3504), "Laudantium minima.", 60, 4, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 108,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 6, 29, 6, 13, 58, 919, DateTimeKind.Unspecified).AddTicks(1280), "Qui aut dolor quis libero quisquam animi architecto quis est voluptas quia voluptate et.", new DateTime(2021, 7, 1, 15, 31, 30, 615, DateTimeKind.Unspecified).AddTicks(1038), "Deserunt voluptate veritatis aut.", 54, 20 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 109,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 5, 19, 0, 49, 36, 217, DateTimeKind.Unspecified).AddTicks(4407), "Esse placeat officiis possimus neque nobis nemo.", new DateTime(2021, 5, 24, 1, 9, 28, 581, DateTimeKind.Unspecified).AddTicks(9314), "Iure eius et quidem.", 16, 4 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 110,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 7, 11, 12, 57, 2, 239, DateTimeKind.Unspecified).AddTicks(4207), "Ut maxime voluptatem facere aliquam amet magnam vel soluta minus qui iure.", new DateTime(2020, 10, 25, 12, 38, 17, 849, DateTimeKind.Unspecified).AddTicks(8583), "Reprehenderit et minus.", 15, 21, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 111,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 2, 20, 11, 49, 12, 26, DateTimeKind.Unspecified).AddTicks(6484), "Ipsa praesentium dicta vero autem ad voluptatem.", new DateTime(2021, 6, 26, 5, 32, 15, 804, DateTimeKind.Unspecified).AddTicks(980), "Ut qui voluptatem.", 50, 25, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 112,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 3, 16, 22, 36, 4, 439, DateTimeKind.Unspecified).AddTicks(5274), "Fuga autem deleniti tenetur voluptates nihil accusantium voluptas id est fugit.", new DateTime(2021, 6, 13, 22, 32, 2, 267, DateTimeKind.Unspecified).AddTicks(3669), "Facere ipsam et nesciunt.", 34, 6 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 113,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 18, 51, 119, DateTimeKind.Unspecified).AddTicks(7070), "Ex quasi ducimus ut provident voluptates nobis vero tempora facere aspernatur voluptate odit earum.", new DateTime(2019, 8, 20, 14, 32, 8, 655, DateTimeKind.Unspecified).AddTicks(4794), "Eos nam in vel.", 67, 26, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 114,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 4, 12, 6, 11, 31, 787, DateTimeKind.Unspecified).AddTicks(4539), "Voluptatem ea pariatur nesciunt voluptatem ab qui assumenda omnis placeat.", new DateTime(2019, 8, 16, 10, 10, 12, 409, DateTimeKind.Unspecified).AddTicks(1783), "Tempore autem.", 46, 26, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 115,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 12, 2, 19, 55, 3, 600, DateTimeKind.Unspecified).AddTicks(4034), "Quam ea aperiam ut eveniet excepturi est dolorum ipsam veniam similique voluptas.", new DateTime(2019, 2, 7, 0, 40, 27, 340, DateTimeKind.Unspecified).AddTicks(5686), "Sed rerum.", 76, 13, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 116,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 8, 21, 52, 43, 598, DateTimeKind.Unspecified).AddTicks(3669), "Laboriosam distinctio blanditiis et incidunt in voluptatibus non impedit voluptate molestiae tenetur eos fugit.", new DateTime(2021, 9, 13, 5, 2, 58, 862, DateTimeKind.Unspecified).AddTicks(3616), "Placeat nihil aut omnis.", 35, 7, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 117,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 8, 21, 16, 24, 15, 836, DateTimeKind.Unspecified).AddTicks(9420), "Nisi ipsa quo non dolorum quos quis eius earum facilis autem optio in dignissimos.", new DateTime(2020, 4, 18, 23, 8, 56, 752, DateTimeKind.Unspecified).AddTicks(2613), "Atque consectetur.", 6, 24, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 118,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 12, 22, 22, 39, 4, 220, DateTimeKind.Unspecified).AddTicks(2117), "Qui aut recusandae vel quidem et ratione soluta aut aut qui commodi.", new DateTime(2021, 2, 10, 8, 14, 18, 754, DateTimeKind.Unspecified).AddTicks(6731), "Ea autem accusantium iusto sit.", 79, 15, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 119,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 3, 18, 8, 27, 47, 901, DateTimeKind.Unspecified).AddTicks(1409), "Illo hic iure aut velit suscipit dolorum delectus perferendis in.", new DateTime(2021, 3, 22, 18, 13, 33, 913, DateTimeKind.Unspecified).AddTicks(6176), "Omnis esse deserunt.", 23, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 120,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 6, 9, 4, 11, 1, 963, DateTimeKind.Unspecified).AddTicks(484), "Quo ratione cum aut ex occaecati ipsum reprehenderit.", new DateTime(2021, 6, 20, 11, 6, 21, 77, DateTimeKind.Unspecified).AddTicks(3930), "Consequatur officiis omnis saepe.", 18, 29, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 121,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 4, 6, 11, 16, 5, 144, DateTimeKind.Unspecified).AddTicks(6330), "Sunt vel inventore ratione et asperiores suscipit excepturi tenetur et et nihil corporis.", new DateTime(2019, 4, 7, 15, 19, 20, 746, DateTimeKind.Unspecified).AddTicks(5805), "Maiores asperiores.", 28, 36, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 122,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 1, 6, 3, 30, 48, 799, DateTimeKind.Unspecified).AddTicks(8456), "Exercitationem est ut libero ut modi ullam similique quia repellendus quod.", new DateTime(2021, 1, 9, 17, 4, 15, 704, DateTimeKind.Unspecified).AddTicks(6812), "Impedit eum illo vel qui.", 28, 34, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 123,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 6, 15, 22, 43, 40, 108, DateTimeKind.Unspecified).AddTicks(3814), "Esse temporibus ut voluptatum ipsa nihil sit veniam alias aperiam repellat est similique similique.", new DateTime(2020, 6, 24, 3, 47, 3, 251, DateTimeKind.Unspecified).AddTicks(5258), "In blanditiis voluptatem.", 68, 11, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 124,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 11, 8, 17, 20, 5, 157, DateTimeKind.Unspecified).AddTicks(5751), "Asperiores quos et commodi consequatur expedita ea consectetur id aut recusandae consequatur consectetur.", new DateTime(2020, 3, 12, 15, 58, 30, 875, DateTimeKind.Unspecified).AddTicks(4445), "At consequuntur aspernatur quia.", 14, 21, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 125,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 3, 17, 17, 4, 33, 658, DateTimeKind.Unspecified).AddTicks(1442), "Fuga maiores laborum expedita fugit quis eligendi omnis.", new DateTime(2021, 5, 21, 9, 1, 59, 84, DateTimeKind.Unspecified).AddTicks(7264), "Quod tempora et minima aut.", 74, 25 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 126,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2020, 3, 21, 10, 1, 49, 410, DateTimeKind.Unspecified).AddTicks(2941), "Suscipit cumque est repudiandae voluptas quos sunt a ut vel rerum vel.", new DateTime(2020, 7, 2, 20, 8, 31, 313, DateTimeKind.Unspecified).AddTicks(2538), "Autem rerum quam delectus.", 54, 9 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 127,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 5, 16, 46, 1, 168, DateTimeKind.Unspecified).AddTicks(8553), "Culpa qui corporis qui commodi provident autem aut aut est deserunt.", new DateTime(2019, 10, 5, 23, 14, 19, 128, DateTimeKind.Unspecified).AddTicks(8464), "Provident quos mollitia et.", 23, 22, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 128,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 26, 4, 4, 13, 826, DateTimeKind.Unspecified).AddTicks(1036), "Qui ut libero repellat omnis fugiat saepe nihil necessitatibus.", new DateTime(2019, 3, 27, 19, 46, 45, 799, DateTimeKind.Unspecified).AddTicks(2832), "Perspiciatis voluptas.", 9, 36, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 129,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 3, 31, 11, 4, 10, 181, DateTimeKind.Unspecified).AddTicks(581), "Quia nihil ut ad fugiat qui dolorum tempora molestias assumenda mollitia laudantium consequatur rerum.", new DateTime(2020, 5, 29, 13, 30, 23, 106, DateTimeKind.Unspecified).AddTicks(4673), "Praesentium voluptatem.", 63, 11, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 130,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 10, 4, 8, 45, 22, 614, DateTimeKind.Unspecified).AddTicks(1260), "Non et dolor quas qui totam voluptates laborum voluptatem quaerat suscipit ut.", new DateTime(2021, 8, 19, 9, 50, 54, 529, DateTimeKind.Unspecified).AddTicks(6795), "Dolorem provident rerum.", 31, 15, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 131,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 5, 29, 4, 45, 28, 644, DateTimeKind.Unspecified).AddTicks(2823), "Cupiditate qui adipisci sint qui cum quisquam beatae cum repellendus.", new DateTime(2019, 6, 23, 21, 32, 3, 467, DateTimeKind.Unspecified).AddTicks(7866), "Quia quia aut molestiae.", 10, 13, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 132,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 4, 12, 8, 48, 9, 797, DateTimeKind.Unspecified).AddTicks(5659), "Consequatur cupiditate quasi deleniti voluptas molestiae exercitationem quia occaecati natus repellendus.", new DateTime(2020, 5, 15, 6, 0, 3, 595, DateTimeKind.Unspecified).AddTicks(4361), "Ipsa vero maiores velit aut.", 44, 17, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 133,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 1, 14, 9, 14, 40, 887, DateTimeKind.Unspecified).AddTicks(5849), "Sed quasi inventore modi iure quaerat possimus veniam voluptatem quia incidunt.", new DateTime(2020, 6, 7, 11, 55, 22, 288, DateTimeKind.Unspecified).AddTicks(543), "Recusandae perspiciatis sit.", 68, 17, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 134,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2020, 7, 15, 23, 25, 4, 715, DateTimeKind.Unspecified).AddTicks(8610), "Minima molestias ut odio sint quos in illo expedita molestiae tenetur dolore.", new DateTime(2021, 1, 17, 2, 59, 2, 953, DateTimeKind.Unspecified).AddTicks(3700), "Quo autem repellendus totam ut.", 54, 19 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 135,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 4, 12, 14, 53, 18, 249, DateTimeKind.Unspecified).AddTicks(2822), "Id modi officia nihil atque aspernatur placeat qui consectetur at voluptas omnis quo corrupti.", new DateTime(2021, 4, 17, 19, 24, 9, 611, DateTimeKind.Unspecified).AddTicks(4969), "Molestiae aliquam iusto iste.", 6, 33, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 136,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 1, 14, 3, 0, 33, 535, DateTimeKind.Unspecified).AddTicks(161), "Quo eum consequuntur magnam ut aut nemo est molestias neque numquam non temporibus.", new DateTime(2021, 2, 1, 18, 33, 23, 235, DateTimeKind.Unspecified).AddTicks(7013), "Deleniti sapiente quia numquam.", 61, 37, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 137,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 8, 27, 14, 25, 53, 207, DateTimeKind.Unspecified).AddTicks(6585), "Nulla nostrum eos reprehenderit commodi voluptatibus occaecati omnis perferendis quis officiis est quo dolorum.", new DateTime(2021, 8, 28, 4, 24, 45, 861, DateTimeKind.Unspecified).AddTicks(9820), "Consequuntur labore perspiciatis.", 5, 6, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 138,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 5, 31, 21, 6, 14, 530, DateTimeKind.Unspecified).AddTicks(8761), "Alias hic fugit libero hic sed vel dicta.", new DateTime(2020, 10, 2, 6, 27, 27, 934, DateTimeKind.Unspecified).AddTicks(1084), "Quia id.", 77, 24, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 139,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 11, 29, 13, 58, 13, 334, DateTimeKind.Unspecified).AddTicks(8818), "Iure velit iste sint qui et veniam eum odit omnis aut amet suscipit rerum.", new DateTime(2019, 1, 29, 2, 53, 33, 387, DateTimeKind.Unspecified).AddTicks(4434), "Dolorum rerum maxime.", 20, 10, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 140,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 6, 10, 19, 13, 13, 815, DateTimeKind.Unspecified).AddTicks(4915), "Minima animi exercitationem laboriosam consectetur magnam eius architecto.", new DateTime(2019, 6, 24, 14, 41, 11, 177, DateTimeKind.Unspecified).AddTicks(1356), "Similique sunt sed.", 61, 13 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 141,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2020, 5, 30, 1, 54, 38, 821, DateTimeKind.Unspecified).AddTicks(4851), "Similique explicabo voluptatem reiciendis ea fugiat dolor eaque reiciendis.", new DateTime(2020, 6, 15, 0, 1, 10, 68, DateTimeKind.Unspecified).AddTicks(9743), "Aut officiis.", 34, 11 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 142,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "ProjectId" },
                values: new object[] { new DateTime(2021, 6, 11, 22, 48, 0, 69, DateTimeKind.Unspecified).AddTicks(4503), "Consectetur vero sit eaque debitis sed cumque fugiat modi ab quidem autem et alias.", new DateTime(2021, 6, 30, 4, 28, 33, 903, DateTimeKind.Unspecified).AddTicks(6701), "Repellat enim quidem perspiciatis hic.", 20 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 143,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 8, 10, 22, 42, 4, 463, DateTimeKind.Unspecified).AddTicks(5480), "Officiis et quia voluptate atque ducimus voluptatem molestiae ducimus.", new DateTime(2021, 9, 6, 19, 42, 56, 400, DateTimeKind.Unspecified).AddTicks(3986), "Corporis numquam maiores ipsa.", 43, 15, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 144,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 2, 22, 0, 55, 51, 209, DateTimeKind.Unspecified).AddTicks(9407), "Quia excepturi esse suscipit inventore delectus omnis omnis iusto rerum.", new DateTime(2021, 1, 1, 18, 3, 2, 954, DateTimeKind.Unspecified).AddTicks(4479), "Beatae sed facere numquam.", 1, 40, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 145,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 11, 24, 22, 50, 42, 468, DateTimeKind.Unspecified).AddTicks(6383), "Quis ducimus voluptatum et et aut commodi ut quae iusto aspernatur.", new DateTime(2020, 12, 9, 14, 19, 35, 931, DateTimeKind.Unspecified).AddTicks(7604), "Culpa molestiae id.", 30, 19, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 146,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 6, 27, 5, 37, 5, 654, DateTimeKind.Unspecified).AddTicks(4954), "Corrupti quos consequatur qui consequatur quidem veniam occaecati in dolores omnis sit.", new DateTime(2021, 6, 27, 22, 20, 41, 584, DateTimeKind.Unspecified).AddTicks(2686), "Temporibus aliquid occaecati.", 63, 25, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 147,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 12, 23, 21, 3, 15, 116, DateTimeKind.Unspecified).AddTicks(7654), "Qui repellat ut occaecati explicabo autem est rerum recusandae.", new DateTime(2021, 1, 5, 16, 35, 11, 886, DateTimeKind.Unspecified).AddTicks(1402), "Quae animi.", 71, 32, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 148,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2020, 6, 28, 10, 8, 12, 943, DateTimeKind.Unspecified).AddTicks(485), "Est itaque voluptatem odio neque voluptas dolor sit mollitia optio corporis et ad.", new DateTime(2021, 4, 1, 19, 6, 43, 963, DateTimeKind.Unspecified).AddTicks(2416), "Libero incidunt officiis quis vitae.", 22, 31 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 149,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 5, 21, 5, 7, 17, 341, DateTimeKind.Unspecified).AddTicks(1874), "Optio veniam repudiandae consequatur omnis eius cum sint qui vitae inventore modi beatae.", new DateTime(2021, 6, 17, 21, 14, 7, 328, DateTimeKind.Unspecified).AddTicks(855), "Tempore debitis totam nihil nihil.", 27, 38 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 150,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 11, 10, 12, 11, 33, 594, DateTimeKind.Unspecified).AddTicks(1097), "Magni totam ea ex animi non voluptate hic dolor ut et.", new DateTime(2019, 2, 24, 4, 12, 9, 829, DateTimeKind.Unspecified).AddTicks(4902), "Assumenda exercitationem.", 78, 26, 2 });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2018, 11, 22, 23, 17, 13, 808, DateTimeKind.Unspecified).AddTicks(8039), "Viva Pfeffer" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2020, 9, 6, 9, 26, 7, 411, DateTimeKind.Unspecified).AddTicks(7560), "Reilly Marks" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2021, 3, 7, 1, 28, 8, 782, DateTimeKind.Unspecified).AddTicks(6259), "Isaac Connelly" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2019, 1, 20, 3, 41, 13, 700, DateTimeKind.Unspecified).AddTicks(9575), "Ignacio Lebsack" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2019, 4, 16, 23, 29, 4, 134, DateTimeKind.Unspecified).AddTicks(3161), "Buck Koss" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2019, 8, 28, 5, 23, 32, 219, DateTimeKind.Unspecified).AddTicks(5245), "Jesse Walter" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2019, 8, 12, 19, 44, 29, 285, DateTimeKind.Unspecified).AddTicks(7835), "Zoila Upton" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2018, 3, 25, 19, 48, 7, 892, DateTimeKind.Unspecified).AddTicks(8607), "Briana Hermiston" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2018, 9, 26, 13, 4, 49, 574, DateTimeKind.Unspecified).AddTicks(5296), "Cathryn Boehm" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2021, 1, 26, 15, 49, 44, 919, DateTimeKind.Unspecified).AddTicks(5875), "Seamus Mosciski" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1975, 6, 7, 15, 20, 48, 964, DateTimeKind.Unspecified).AddTicks(9043), new DateTime(1996, 10, 28, 11, 29, 1, 262, DateTimeKind.Unspecified).AddTicks(7155), "Kristy.Walker37@yahoo.com", "Rosanna", "Schowalter", 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1972, 9, 9, 0, 25, 50, 84, DateTimeKind.Unspecified).AddTicks(27), new DateTime(1990, 10, 4, 7, 9, 25, 417, DateTimeKind.Unspecified).AddTicks(855), "Therese_OReilly@hotmail.com", "Esperanza", "Muller", 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1999, 1, 31, 9, 11, 58, 648, DateTimeKind.Unspecified).AddTicks(2886), new DateTime(2014, 2, 13, 0, 4, 23, 326, DateTimeKind.Unspecified).AddTicks(7364), "Dillan19@gmail.com", "Tyrique", "Mills", 6 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1997, 1, 5, 19, 34, 54, 368, DateTimeKind.Unspecified).AddTicks(1136), new DateTime(2012, 7, 7, 19, 36, 3, 662, DateTimeKind.Unspecified).AddTicks(3121), "Lauriane_Ratke54@gmail.com", "Keyon", "Ledner", 6 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1992, 5, 29, 7, 27, 41, 306, DateTimeKind.Unspecified).AddTicks(4657), new DateTime(2015, 9, 9, 10, 50, 26, 452, DateTimeKind.Unspecified).AddTicks(13), "Brian.Langosh@hotmail.com", "Fletcher", "O'Connell", 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1974, 1, 10, 13, 50, 6, 199, DateTimeKind.Unspecified).AddTicks(461), new DateTime(1999, 3, 21, 11, 15, 42, 712, DateTimeKind.Unspecified).AddTicks(5451), "Stella31@yahoo.com", "Rashawn", "Hilpert", 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1979, 5, 15, 1, 23, 6, 258, DateTimeKind.Unspecified).AddTicks(4516), new DateTime(2012, 7, 7, 6, 42, 26, 594, DateTimeKind.Unspecified).AddTicks(5592), "Jessyca.Schowalter@hotmail.com", "Millie", "Nicolas", 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1984, 5, 14, 18, 11, 24, 429, DateTimeKind.Unspecified).AddTicks(7827), new DateTime(2020, 10, 6, 1, 45, 4, 381, DateTimeKind.Unspecified).AddTicks(8568), "Shaina.McKenzie13@hotmail.com", "Elroy", "Kling", 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1992, 7, 17, 5, 10, 25, 948, DateTimeKind.Unspecified).AddTicks(3166), new DateTime(2018, 11, 12, 11, 18, 8, 477, DateTimeKind.Unspecified).AddTicks(5327), "Nicole_Cremin@gmail.com", "Cameron", "Runolfsdottir", 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2010, 10, 20, 16, 54, 19, 723, DateTimeKind.Unspecified).AddTicks(4078), new DateTime(2021, 6, 1, 4, 30, 15, 950, DateTimeKind.Unspecified).AddTicks(5998), "Rocky.Reichert32@yahoo.com", "Jacinthe", "D'Amore", 10 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1981, 3, 21, 10, 38, 52, 569, DateTimeKind.Unspecified).AddTicks(3345), new DateTime(2007, 11, 12, 16, 59, 30, 554, DateTimeKind.Unspecified).AddTicks(5181), "Forrest.Mayert@gmail.com", "Obie", "Walker", 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName" },
                values: new object[] { new DateTime(1999, 3, 24, 0, 50, 1, 699, DateTimeKind.Unspecified).AddTicks(4662), new DateTime(2007, 6, 19, 13, 47, 30, 117, DateTimeKind.Unspecified).AddTicks(6489), "Keven_Swaniawski@hotmail.com", "Ocie", "Koepp" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2008, 8, 27, 7, 44, 4, 311, DateTimeKind.Unspecified).AddTicks(4442), new DateTime(2017, 2, 14, 15, 53, 10, 974, DateTimeKind.Unspecified).AddTicks(9323), "Rubye4@yahoo.com", "Mckenna", "Stracke", 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1995, 7, 21, 11, 5, 24, 674, DateTimeKind.Unspecified).AddTicks(2906), new DateTime(2003, 7, 30, 14, 25, 12, 293, DateTimeKind.Unspecified).AddTicks(3345), "Bettie.Homenick39@gmail.com", "Quentin", "Jaskolski", 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1982, 3, 12, 23, 23, 21, 126, DateTimeKind.Unspecified).AddTicks(4948), new DateTime(2008, 10, 28, 15, 41, 42, 395, DateTimeKind.Unspecified).AddTicks(3281), "Marques.Quitzon84@gmail.com", "Deonte", "Grimes", 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1987, 4, 11, 8, 56, 20, 229, DateTimeKind.Unspecified).AddTicks(5142), new DateTime(2009, 4, 12, 12, 57, 12, 261, DateTimeKind.Unspecified).AddTicks(5590), "Stone7@gmail.com", "Stefan", "Franecki", 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1989, 4, 4, 11, 2, 24, 684, DateTimeKind.Unspecified).AddTicks(8220), new DateTime(2005, 9, 20, 6, 6, 46, 424, DateTimeKind.Unspecified).AddTicks(2174), "Frances.Kihn3@yahoo.com", "Carley", "Kerluke", 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1987, 6, 28, 22, 39, 46, 589, DateTimeKind.Unspecified).AddTicks(879), new DateTime(2015, 9, 14, 23, 33, 43, 369, DateTimeKind.Unspecified).AddTicks(6381), "Kenya57@yahoo.com", "Adrienne", "Hane", 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName" },
                values: new object[] { new DateTime(2012, 8, 5, 22, 7, 33, 354, DateTimeKind.Unspecified).AddTicks(1884), new DateTime(2020, 11, 23, 5, 36, 57, 137, DateTimeKind.Unspecified).AddTicks(8718), "Joel.Rutherford47@gmail.com", "Reinhold", "Lowe" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1974, 11, 25, 19, 44, 55, 465, DateTimeKind.Unspecified).AddTicks(6714), new DateTime(1985, 12, 23, 16, 44, 44, 191, DateTimeKind.Unspecified).AddTicks(3904), "Sabrina26@gmail.com", "Jan", "Weissnat", 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1971, 4, 8, 14, 20, 3, 460, DateTimeKind.Unspecified).AddTicks(4200), new DateTime(2000, 8, 23, 23, 17, 14, 709, DateTimeKind.Unspecified).AddTicks(165), "Shawna.Waelchi85@yahoo.com", "Ron", "Franecki", 10 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2001, 7, 16, 8, 56, 9, 394, DateTimeKind.Unspecified).AddTicks(8048), new DateTime(2016, 3, 15, 8, 44, 0, 25, DateTimeKind.Unspecified).AddTicks(2437), "Sherman_Fritsch80@yahoo.com", "Nikko", "Kassulke", 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1983, 8, 10, 8, 45, 18, 868, DateTimeKind.Unspecified).AddTicks(88), new DateTime(2000, 1, 25, 17, 29, 45, 494, DateTimeKind.Unspecified).AddTicks(5702), "Geovanni_Herman84@yahoo.com", "Alexandria", "Treutel", 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1997, 7, 16, 20, 43, 30, 859, DateTimeKind.Unspecified).AddTicks(6415), new DateTime(2020, 5, 21, 16, 8, 18, 330, DateTimeKind.Unspecified).AddTicks(1056), "Kasandra.Mueller14@gmail.com", "Cole", "Wuckert", 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1996, 8, 16, 13, 32, 34, 662, DateTimeKind.Unspecified).AddTicks(8714), new DateTime(2009, 11, 6, 13, 17, 57, 62, DateTimeKind.Unspecified).AddTicks(4480), "Norval.Wiza@yahoo.com", "Clark", "Rath", 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1998, 1, 14, 12, 9, 19, 273, DateTimeKind.Unspecified).AddTicks(7716), new DateTime(2006, 5, 4, 21, 46, 28, 661, DateTimeKind.Unspecified).AddTicks(1734), "Maryse.Hauck@gmail.com", "Dexter", "Hickle", 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1980, 4, 15, 8, 34, 7, 878, DateTimeKind.Unspecified).AddTicks(4188), new DateTime(2015, 6, 26, 9, 59, 3, 725, DateTimeKind.Unspecified).AddTicks(3948), "Dana_Herman@gmail.com", "Cole", "Ratke", 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2002, 8, 23, 19, 35, 32, 838, DateTimeKind.Unspecified).AddTicks(8022), new DateTime(2015, 3, 23, 6, 55, 0, 687, DateTimeKind.Unspecified).AddTicks(7836), "Dakota_Purdy58@yahoo.com", "Kennedi", "Wehner", 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1982, 10, 3, 3, 16, 55, 570, DateTimeKind.Unspecified).AddTicks(3418), new DateTime(2000, 5, 17, 16, 27, 35, 202, DateTimeKind.Unspecified).AddTicks(3276), "Tressie85@gmail.com", "Melisa", "Wiza", 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1988, 5, 6, 6, 51, 9, 980, DateTimeKind.Unspecified).AddTicks(7445), new DateTime(2006, 2, 19, 21, 25, 20, 159, DateTimeKind.Unspecified).AddTicks(9071), "Leonie.Kris@hotmail.com", "Jonas", "Miller", 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1992, 4, 19, 9, 47, 7, 426, DateTimeKind.Unspecified).AddTicks(6080), new DateTime(2014, 8, 6, 6, 39, 37, 207, DateTimeKind.Unspecified).AddTicks(1537), "Rachael_Hansen98@yahoo.com", "Dax", "Satterfield", 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1992, 2, 29, 16, 49, 38, 428, DateTimeKind.Unspecified).AddTicks(2370), new DateTime(2018, 4, 9, 6, 32, 57, 490, DateTimeKind.Unspecified).AddTicks(5635), "Jeramie.Ritchie73@hotmail.com", "Sophie", "Miller", 10 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1978, 4, 17, 20, 53, 58, 859, DateTimeKind.Unspecified).AddTicks(7223), new DateTime(2006, 7, 2, 7, 4, 14, 716, DateTimeKind.Unspecified).AddTicks(460), "Eugenia.Schoen@gmail.com", "Joany", "Anderson", 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName" },
                values: new object[] { new DateTime(2005, 7, 17, 9, 26, 41, 301, DateTimeKind.Unspecified).AddTicks(5174), new DateTime(2014, 4, 17, 11, 52, 41, 972, DateTimeKind.Unspecified).AddTicks(6952), "Timmy_Herzog@gmail.com", "Evie", "Purdy" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2010, 9, 14, 7, 22, 47, 940, DateTimeKind.Unspecified).AddTicks(8260), new DateTime(2018, 9, 20, 16, 8, 25, 118, DateTimeKind.Unspecified).AddTicks(1586), "Lewis.Zulauf73@hotmail.com", "Pablo", "Nikolaus", 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1987, 2, 11, 8, 10, 54, 245, DateTimeKind.Unspecified).AddTicks(3217), new DateTime(2015, 2, 12, 23, 10, 35, 290, DateTimeKind.Unspecified).AddTicks(5328), "Dorothy_Mitchell1@gmail.com", "Sigurd", "Stroman", 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1979, 9, 6, 22, 29, 44, 102, DateTimeKind.Unspecified).AddTicks(9830), new DateTime(2010, 7, 12, 3, 38, 53, 859, DateTimeKind.Unspecified).AddTicks(2545), "Skylar.Parker@yahoo.com", "Bailey", "O'Reilly", 10 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2009, 1, 26, 18, 1, 52, 898, DateTimeKind.Unspecified).AddTicks(2116), new DateTime(2019, 4, 8, 11, 52, 8, 767, DateTimeKind.Unspecified).AddTicks(6029), "Samir.DAmore@gmail.com", "Guido", "Wolf", 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2004, 2, 2, 18, 11, 22, 284, DateTimeKind.Unspecified).AddTicks(3540), new DateTime(2017, 12, 27, 2, 17, 25, 312, DateTimeKind.Unspecified).AddTicks(1546), "Jabari_Smitham75@yahoo.com", "Donnie", "Mante", 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1980, 12, 25, 16, 45, 27, 809, DateTimeKind.Unspecified).AddTicks(3448), new DateTime(2007, 1, 3, 4, 22, 35, 331, DateTimeKind.Unspecified).AddTicks(5735), "Kameron10@yahoo.com", "Melvina", "Buckridge", 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 41,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1989, 3, 29, 22, 16, 42, 596, DateTimeKind.Unspecified).AddTicks(1770), new DateTime(2004, 12, 14, 20, 50, 37, 103, DateTimeKind.Unspecified).AddTicks(9160), "Jedediah40@hotmail.com", "Maurine", "Harris", 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 42,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1972, 7, 12, 10, 45, 2, 22, DateTimeKind.Unspecified).AddTicks(5612), new DateTime(2015, 3, 16, 6, 16, 45, 891, DateTimeKind.Unspecified).AddTicks(5704), "Allie9@gmail.com", "Brian", "Trantow", 10 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 43,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2014, 5, 16, 1, 14, 45, 770, DateTimeKind.Unspecified).AddTicks(1978), new DateTime(2021, 11, 6, 15, 42, 23, 900, DateTimeKind.Unspecified).AddTicks(4456), "Candice_Powlowski32@gmail.com", "Lisa", "Klein", 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 44,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2001, 3, 6, 15, 28, 9, 920, DateTimeKind.Unspecified).AddTicks(4572), new DateTime(2012, 3, 13, 9, 49, 51, 787, DateTimeKind.Unspecified).AddTicks(5797), "Kip_Feil94@gmail.com", "Lon", "Wehner", 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 45,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1970, 6, 23, 17, 45, 12, 945, DateTimeKind.Unspecified).AddTicks(1515), new DateTime(2020, 1, 13, 3, 18, 35, 214, DateTimeKind.Unspecified).AddTicks(2447), "Raymundo.Stamm@yahoo.com", "Isai", "Wolf", 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 46,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName" },
                values: new object[] { new DateTime(1971, 5, 20, 8, 35, 51, 739, DateTimeKind.Unspecified).AddTicks(5906), new DateTime(1998, 4, 20, 13, 41, 55, 906, DateTimeKind.Unspecified).AddTicks(9921), "Deja_Goyette@hotmail.com", "Gregory", "Nitzsche" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 47,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1983, 3, 24, 4, 41, 24, 465, DateTimeKind.Unspecified).AddTicks(4136), new DateTime(2003, 4, 26, 9, 56, 18, 907, DateTimeKind.Unspecified).AddTicks(5966), "Joana46@yahoo.com", "Hoyt", "West", 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 48,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2009, 1, 31, 19, 21, 49, 934, DateTimeKind.Unspecified).AddTicks(9846), new DateTime(2017, 2, 26, 0, 37, 4, 249, DateTimeKind.Unspecified).AddTicks(8921), "Ellie50@hotmail.com", "Berta", "Considine", 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 49,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1984, 10, 4, 17, 29, 27, 144, DateTimeKind.Unspecified).AddTicks(6301), new DateTime(2020, 3, 30, 9, 23, 40, 308, DateTimeKind.Unspecified).AddTicks(7524), "Christina39@yahoo.com", "Emmalee", "Yost", 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 50,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1976, 8, 9, 4, 44, 30, 671, DateTimeKind.Unspecified).AddTicks(2222), new DateTime(2016, 6, 1, 9, 42, 4, 277, DateTimeKind.Unspecified).AddTicks(5018), "Carolyn_Torphy@gmail.com", "Cesar", "Volkman", 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 51,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1989, 2, 15, 17, 27, 7, 345, DateTimeKind.Unspecified).AddTicks(6378), new DateTime(2002, 12, 30, 18, 52, 31, 404, DateTimeKind.Unspecified).AddTicks(6558), "Rico97@yahoo.com", "Vincent", "Quitzon", 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 52,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1980, 4, 28, 10, 36, 15, 340, DateTimeKind.Unspecified).AddTicks(5142), new DateTime(1997, 6, 7, 11, 19, 54, 683, DateTimeKind.Unspecified).AddTicks(8682), "Clare_Runolfsdottir@gmail.com", "Armand", "Von", 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 53,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1992, 7, 16, 6, 8, 43, 223, DateTimeKind.Unspecified).AddTicks(8944), new DateTime(2009, 4, 16, 16, 34, 59, 590, DateTimeKind.Unspecified).AddTicks(3514), "Cristobal89@yahoo.com", "Narciso", "Collier", 6 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 54,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1995, 8, 15, 10, 3, 16, 638, DateTimeKind.Unspecified).AddTicks(8255), new DateTime(2008, 1, 20, 16, 34, 57, 889, DateTimeKind.Unspecified).AddTicks(5053), "Elroy.Grimes@gmail.com", "Josefa", "Herman", 10 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 55,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1972, 9, 20, 5, 17, 45, 445, DateTimeKind.Unspecified).AddTicks(7162), new DateTime(1996, 12, 13, 1, 20, 49, 28, DateTimeKind.Unspecified).AddTicks(1891), "Nelson89@gmail.com", "Emmy", "Wisoky", 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 56,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2008, 10, 21, 1, 0, 25, 751, DateTimeKind.Unspecified).AddTicks(3596), new DateTime(2020, 4, 21, 16, 1, 36, 692, DateTimeKind.Unspecified).AddTicks(9903), "Collin.Wintheiser39@hotmail.com", "Vinnie", "Hansen", 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 57,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1976, 11, 27, 9, 4, 18, 272, DateTimeKind.Unspecified).AddTicks(2052), new DateTime(1987, 9, 18, 14, 28, 45, 743, DateTimeKind.Unspecified).AddTicks(8750), "Marta_Rempel89@hotmail.com", "Jana", "Ankunding", 10 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 58,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2001, 4, 30, 1, 30, 27, 138, DateTimeKind.Unspecified).AddTicks(7444), new DateTime(2012, 12, 25, 7, 27, 5, 341, DateTimeKind.Unspecified).AddTicks(5522), "Ophelia.Baumbach@yahoo.com", "Winfield", "Gutkowski", 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 59,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1980, 9, 18, 13, 36, 39, 566, DateTimeKind.Unspecified).AddTicks(1042), new DateTime(2020, 10, 8, 7, 56, 29, 222, DateTimeKind.Unspecified).AddTicks(6634), "Clare.Kris46@yahoo.com", "Bernard", "Wuckert", 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 60,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1985, 12, 9, 1, 6, 56, 885, DateTimeKind.Unspecified).AddTicks(7713), new DateTime(1999, 3, 26, 6, 12, 19, 602, DateTimeKind.Unspecified).AddTicks(3716), "Vena37@hotmail.com", "Joey", "Moore", 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 61,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName" },
                values: new object[] { new DateTime(2004, 7, 28, 16, 6, 28, 674, DateTimeKind.Unspecified).AddTicks(6922), new DateTime(2015, 2, 20, 19, 3, 5, 890, DateTimeKind.Unspecified).AddTicks(9457), "Evelyn.Marvin78@gmail.com", "Deborah", "Hessel" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 62,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1973, 10, 11, 8, 51, 29, 341, DateTimeKind.Unspecified).AddTicks(3608), new DateTime(1996, 3, 7, 4, 24, 9, 334, DateTimeKind.Unspecified).AddTicks(5429), "Alexandro.Douglas@yahoo.com", "Idell", "Legros", 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 63,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2000, 10, 25, 14, 43, 49, 432, DateTimeKind.Unspecified).AddTicks(5280), new DateTime(2015, 7, 24, 14, 53, 1, 238, DateTimeKind.Unspecified).AddTicks(3416), "Jarrett_Pfeffer@gmail.com", "Taurean", "Bradtke", 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 64,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2010, 4, 25, 12, 52, 23, 85, DateTimeKind.Unspecified).AddTicks(6052), new DateTime(2021, 5, 20, 2, 34, 43, 785, DateTimeKind.Unspecified).AddTicks(642), "Samir_Mraz@yahoo.com", "Ezequiel", "Breitenberg", 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 65,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2011, 1, 26, 2, 28, 21, 531, DateTimeKind.Unspecified).AddTicks(2676), new DateTime(2019, 6, 11, 6, 30, 33, 427, DateTimeKind.Unspecified).AddTicks(6387), "Kailey49@hotmail.com", "Marjory", "Kohler", 6 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 66,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2008, 8, 4, 16, 55, 52, 134, DateTimeKind.Unspecified).AddTicks(168), new DateTime(2020, 12, 22, 10, 12, 44, 81, DateTimeKind.Unspecified).AddTicks(68), "Myrtle74@hotmail.com", "Brandy", "Goodwin", 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 67,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1992, 1, 1, 18, 25, 10, 9, DateTimeKind.Unspecified).AddTicks(1974), new DateTime(2008, 11, 16, 1, 28, 40, 271, DateTimeKind.Unspecified).AddTicks(5801), "Kathryne86@gmail.com", "Freeman", "Bosco", 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 68,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2002, 3, 19, 18, 17, 10, 235, DateTimeKind.Unspecified).AddTicks(9134), new DateTime(2014, 10, 31, 13, 50, 41, 437, DateTimeKind.Unspecified).AddTicks(8834), "Cora.Roberts@gmail.com", "Mable", "Heathcote", 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 69,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2011, 4, 14, 16, 49, 6, 966, DateTimeKind.Unspecified).AddTicks(974), new DateTime(2020, 7, 1, 0, 39, 6, 624, DateTimeKind.Unspecified).AddTicks(5854), "Virginia27@yahoo.com", "Zachariah", "Tromp", 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 70,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1976, 3, 16, 6, 53, 48, 656, DateTimeKind.Unspecified).AddTicks(7058), new DateTime(2010, 11, 8, 12, 42, 52, 396, DateTimeKind.Unspecified).AddTicks(7261), "Kianna.Schaefer@gmail.com", "Pete", "Wuckert", 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 71,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2014, 5, 21, 20, 2, 12, 686, DateTimeKind.Unspecified).AddTicks(1172), new DateTime(2022, 4, 14, 2, 46, 55, 326, DateTimeKind.Unspecified).AddTicks(2803), "Jeanne.Volkman62@yahoo.com", "Damian", "Zulauf", 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 72,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2012, 1, 4, 20, 25, 38, 553, DateTimeKind.Unspecified).AddTicks(8914), new DateTime(2021, 2, 27, 1, 29, 12, 386, DateTimeKind.Unspecified).AddTicks(3760), "Gerson.Kovacek@gmail.com", "Aida", "Koch", 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 73,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1988, 9, 10, 19, 58, 14, 535, DateTimeKind.Unspecified).AddTicks(9374), new DateTime(2000, 6, 18, 8, 11, 53, 337, DateTimeKind.Unspecified).AddTicks(9769), "Santa56@gmail.com", "Tracy", "Will", 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 74,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1972, 5, 4, 4, 8, 44, 785, DateTimeKind.Unspecified).AddTicks(3994), new DateTime(2014, 4, 22, 19, 6, 8, 103, DateTimeKind.Unspecified).AddTicks(1352), "George.Zemlak@hotmail.com", "Donald", "Altenwerth", 10 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 75,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1978, 4, 13, 9, 6, 8, 52, DateTimeKind.Unspecified).AddTicks(8907), new DateTime(1997, 6, 21, 14, 18, 54, 122, DateTimeKind.Unspecified).AddTicks(6443), "Johnathon_Sanford@yahoo.com", "Madge", "Waters", 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 76,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName" },
                values: new object[] { new DateTime(1997, 7, 15, 13, 26, 56, 990, DateTimeKind.Unspecified).AddTicks(2368), new DateTime(2009, 6, 9, 13, 43, 33, 341, DateTimeKind.Unspecified).AddTicks(2221), "Marisa45@hotmail.com", "Tressie", "Dietrich" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 77,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2010, 2, 12, 23, 29, 8, 549, DateTimeKind.Unspecified).AddTicks(7366), new DateTime(2019, 6, 20, 4, 26, 43, 300, DateTimeKind.Unspecified).AddTicks(39), "Luna78@hotmail.com", "Kobe", "Doyle", 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 78,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1994, 1, 28, 20, 18, 6, 997, DateTimeKind.Unspecified).AddTicks(5213), new DateTime(2003, 11, 3, 19, 42, 51, 126, DateTimeKind.Unspecified).AddTicks(5453), "Raymundo29@gmail.com", "Jerrod", "Farrell", 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 79,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2014, 1, 28, 7, 6, 31, 439, DateTimeKind.Unspecified).AddTicks(66), new DateTime(2021, 8, 4, 4, 0, 53, 613, DateTimeKind.Unspecified).AddTicks(7771), "Berry4@gmail.com", "Kyler", "Hirthe", 10 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 80,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1970, 8, 18, 19, 50, 45, 35, DateTimeKind.Unspecified).AddTicks(5732), new DateTime(2018, 8, 2, 8, 48, 55, 140, DateTimeKind.Unspecified).AddTicks(9032), "Freddie.Hermiston@hotmail.com", "Toby", "Kertzmann", 8 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "FinishedAt",
                table: "Tasks",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldNullable: true);

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 13, new DateTime(2019, 6, 7, 9, 21, 34, 842, DateTimeKind.Unspecified).AddTicks(6850), new DateTime(2020, 4, 28, 17, 41, 8, 488, DateTimeKind.Unspecified).AddTicks(2706), "Boston's most advanced compression wear technology increases muscle oxygenation, stabilizes active muscles", "Tasty Wooden Chips", 9 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 57, new DateTime(2019, 9, 18, 11, 41, 54, 714, DateTimeKind.Unspecified).AddTicks(9476), new DateTime(2020, 11, 12, 8, 12, 30, 315, DateTimeKind.Unspecified).AddTicks(4628), "Ergonomic executive chair upholstered in bonded black leather and PVC padded seat and back for all-day comfort and support", "Handmade Plastic Car", 3 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 26, new DateTime(2018, 3, 10, 17, 41, 22, 970, DateTimeKind.Unspecified).AddTicks(1365), new DateTime(2020, 3, 11, 22, 48, 50, 218, DateTimeKind.Unspecified).AddTicks(9427), "The Nagasaki Lander is the trademarked name of several series of Nagasaki sport bikes, that started with the 1984 ABC800J", "Handmade Metal Shoes", 10 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 62, new DateTime(2018, 4, 11, 5, 0, 7, 0, DateTimeKind.Unspecified).AddTicks(1413), new DateTime(2019, 5, 4, 23, 14, 51, 663, DateTimeKind.Unspecified).AddTicks(8979), "The automobile layout consists of a front-engine design, with transaxle-type transmissions mounted at the rear of the engine and four wheel drive", "Refined Cotton Pants", 9 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 12, new DateTime(2020, 2, 16, 5, 16, 17, 430, DateTimeKind.Unspecified).AddTicks(7240), new DateTime(2020, 9, 12, 11, 10, 23, 317, DateTimeKind.Unspecified).AddTicks(9761), "Ergonomic executive chair upholstered in bonded black leather and PVC padded seat and back for all-day comfort and support", "Practical Wooden Soap", 6 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 26, new DateTime(2019, 8, 24, 23, 9, 50, 388, DateTimeKind.Unspecified).AddTicks(6214), new DateTime(2020, 5, 16, 14, 12, 13, 510, DateTimeKind.Unspecified).AddTicks(7337), "The beautiful range of Apple Naturalé that has an exciting mix of natural ingredients. With the Goodness of 100% Natural Ingredients", "Sleek Granite Towels", 4 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 51, new DateTime(2018, 8, 14, 1, 39, 15, 29, DateTimeKind.Unspecified).AddTicks(2224), new DateTime(2018, 9, 3, 12, 59, 43, 389, DateTimeKind.Unspecified).AddTicks(1055), "Carbonite web goalkeeper gloves are ergonomically designed to give easy fit", "Incredible Concrete Bike", 7 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name" },
                values: new object[] { 37, new DateTime(2020, 1, 30, 1, 54, 17, 700, DateTimeKind.Unspecified).AddTicks(3408), new DateTime(2021, 10, 2, 5, 27, 7, 223, DateTimeKind.Unspecified).AddTicks(6620), "New range of formal shirts are designed keeping you in mind. With fits and styling that will make you stand apart", "Small Cotton Bacon" });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 51, new DateTime(2019, 4, 20, 10, 1, 36, 9, DateTimeKind.Unspecified).AddTicks(9438), new DateTime(2019, 9, 17, 15, 21, 45, 519, DateTimeKind.Unspecified).AddTicks(2093), "The Football Is Good For Training And Recreational Purposes", "Gorgeous Soft Soap", 5 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 34, new DateTime(2018, 10, 11, 1, 57, 17, 985, DateTimeKind.Unspecified).AddTicks(7147), new DateTime(2020, 7, 1, 19, 13, 32, 654, DateTimeKind.Unspecified).AddTicks(4992), "New range of formal shirts are designed keeping you in mind. With fits and styling that will make you stand apart", "Small Wooden Sausages", 2 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 49, new DateTime(2020, 4, 24, 1, 19, 13, 866, DateTimeKind.Unspecified).AddTicks(7350), new DateTime(2021, 8, 9, 0, 7, 23, 665, DateTimeKind.Unspecified).AddTicks(5226), "The Football Is Good For Training And Recreational Purposes", "Unbranded Steel Cheese", 5 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 60, new DateTime(2021, 1, 2, 8, 55, 10, 116, DateTimeKind.Unspecified).AddTicks(1601), new DateTime(2021, 2, 22, 21, 9, 7, 573, DateTimeKind.Unspecified).AddTicks(106), "Boston's most advanced compression wear technology increases muscle oxygenation, stabilizes active muscles", "Tasty Rubber Chair", 9 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 60, new DateTime(2020, 6, 1, 20, 20, 25, 394, DateTimeKind.Unspecified).AddTicks(2804), new DateTime(2021, 10, 19, 0, 36, 7, 421, DateTimeKind.Unspecified).AddTicks(7056), "The Football Is Good For Training And Recreational Purposes", "Tasty Metal Soap", 9 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 55, new DateTime(2020, 2, 6, 10, 16, 9, 691, DateTimeKind.Unspecified).AddTicks(9386), new DateTime(2021, 8, 27, 0, 22, 34, 920, DateTimeKind.Unspecified).AddTicks(6092), "The Nagasaki Lander is the trademarked name of several series of Nagasaki sport bikes, that started with the 1984 ABC800J", "Refined Steel Soap", 4 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 46, new DateTime(2019, 1, 23, 4, 59, 21, 561, DateTimeKind.Unspecified).AddTicks(5329), new DateTime(2021, 6, 23, 17, 1, 49, 460, DateTimeKind.Unspecified).AddTicks(3011), "The Nagasaki Lander is the trademarked name of several series of Nagasaki sport bikes, that started with the 1984 ABC800J", "Intelligent Concrete Pizza", 1 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 32, new DateTime(2019, 6, 19, 6, 12, 52, 387, DateTimeKind.Unspecified).AddTicks(7907), new DateTime(2019, 12, 17, 4, 43, 0, 423, DateTimeKind.Unspecified).AddTicks(6897), "Ergonomic executive chair upholstered in bonded black leather and PVC padded seat and back for all-day comfort and support", "Ergonomic Soft Soap", 7 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 47, new DateTime(2020, 10, 26, 22, 43, 1, 907, DateTimeKind.Unspecified).AddTicks(2751), new DateTime(2020, 11, 1, 14, 7, 56, 54, DateTimeKind.Unspecified).AddTicks(3971), "New range of formal shirts are designed keeping you in mind. With fits and styling that will make you stand apart", "Incredible Soft Bacon", 7 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 65, new DateTime(2018, 6, 2, 12, 50, 26, 389, DateTimeKind.Unspecified).AddTicks(1737), new DateTime(2020, 5, 12, 8, 5, 18, 438, DateTimeKind.Unspecified).AddTicks(3567), "The Football Is Good For Training And Recreational Purposes", "Incredible Fresh Fish", 3 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 79, new DateTime(2020, 1, 22, 13, 4, 45, 532, DateTimeKind.Unspecified).AddTicks(7183), new DateTime(2021, 9, 29, 0, 39, 52, 773, DateTimeKind.Unspecified).AddTicks(1777), "The Nagasaki Lander is the trademarked name of several series of Nagasaki sport bikes, that started with the 1984 ABC800J", "Tasty Wooden Shoes", 5 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 23, new DateTime(2019, 11, 4, 22, 31, 9, 623, DateTimeKind.Unspecified).AddTicks(7044), new DateTime(2020, 3, 7, 17, 52, 51, 761, DateTimeKind.Unspecified).AddTicks(8325), "Andy shoes are designed to keeping in mind durability as well as trends, the most stylish range of shoes & sandals", "Incredible Soft Table", 7 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 46, new DateTime(2018, 12, 4, 11, 25, 44, 299, DateTimeKind.Unspecified).AddTicks(2718), new DateTime(2020, 8, 9, 10, 43, 47, 598, DateTimeKind.Unspecified).AddTicks(869), "The beautiful range of Apple Naturalé that has an exciting mix of natural ingredients. With the Goodness of 100% Natural Ingredients", "Handcrafted Cotton Keyboard", 9 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 26, new DateTime(2018, 2, 14, 12, 11, 31, 988, DateTimeKind.Unspecified).AddTicks(2166), new DateTime(2021, 6, 28, 22, 0, 54, 128, DateTimeKind.Unspecified).AddTicks(291), "Boston's most advanced compression wear technology increases muscle oxygenation, stabilizes active muscles", "Practical Fresh Car", 8 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 17, new DateTime(2018, 10, 5, 14, 59, 26, 810, DateTimeKind.Unspecified).AddTicks(970), new DateTime(2021, 7, 13, 15, 43, 25, 677, DateTimeKind.Unspecified).AddTicks(9060), "New ABC 13 9370, 13.3, 5th Gen CoreA5-8250U, 8GB RAM, 256GB SSD, power UHD Graphics, OS 10 Home, OS Office A & J 2016", "Handcrafted Steel Car", 10 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name" },
                values: new object[] { 17, new DateTime(2020, 7, 25, 22, 3, 7, 99, DateTimeKind.Unspecified).AddTicks(9254), new DateTime(2021, 7, 7, 4, 22, 33, 960, DateTimeKind.Unspecified).AddTicks(4832), "New range of formal shirts are designed keeping you in mind. With fits and styling that will make you stand apart", "Intelligent Frozen Table" });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 7, new DateTime(2021, 6, 12, 23, 33, 41, 408, DateTimeKind.Unspecified).AddTicks(7274), new DateTime(2021, 7, 9, 14, 49, 32, 925, DateTimeKind.Unspecified).AddTicks(270), "The Apollotech B340 is an affordable wireless mouse with reliable connectivity, 12 months battery life and modern design", "Incredible Rubber Pizza", 3 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 30, new DateTime(2020, 1, 11, 1, 9, 57, 534, DateTimeKind.Unspecified).AddTicks(648), new DateTime(2021, 1, 31, 4, 42, 44, 183, DateTimeKind.Unspecified).AddTicks(6195), "The automobile layout consists of a front-engine design, with transaxle-type transmissions mounted at the rear of the engine and four wheel drive", "Unbranded Frozen Chair", 9 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 48, new DateTime(2021, 5, 8, 9, 3, 55, 338, DateTimeKind.Unspecified).AddTicks(4565), new DateTime(2021, 10, 5, 15, 26, 56, 801, DateTimeKind.Unspecified).AddTicks(2830), "New ABC 13 9370, 13.3, 5th Gen CoreA5-8250U, 8GB RAM, 256GB SSD, power UHD Graphics, OS 10 Home, OS Office A & J 2016", "Handmade Rubber Ball", 4 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 32, new DateTime(2019, 3, 20, 17, 49, 47, 332, DateTimeKind.Unspecified).AddTicks(6103), new DateTime(2020, 11, 5, 16, 25, 19, 125, DateTimeKind.Unspecified).AddTicks(3075), "Carbonite web goalkeeper gloves are ergonomically designed to give easy fit", "Intelligent Concrete Chicken", 5 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 75, new DateTime(2020, 10, 22, 13, 17, 31, 736, DateTimeKind.Unspecified).AddTicks(7084), new DateTime(2021, 4, 15, 7, 12, 28, 740, DateTimeKind.Unspecified).AddTicks(4490), "New range of formal shirts are designed keeping you in mind. With fits and styling that will make you stand apart", "Ergonomic Cotton Shirt", 7 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 75, new DateTime(2019, 10, 3, 20, 20, 23, 128, DateTimeKind.Unspecified).AddTicks(7349), new DateTime(2020, 7, 5, 1, 51, 41, 561, DateTimeKind.Unspecified).AddTicks(748), "The slim & simple Maple Gaming Keyboard from Dev Byte comes with a sleek body and 7- Color RGB LED Back-lighting for smart functionality", "Ergonomic Fresh Gloves", 1 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 46, new DateTime(2018, 5, 19, 10, 18, 0, 616, DateTimeKind.Unspecified).AddTicks(7798), new DateTime(2021, 11, 21, 23, 55, 3, 29, DateTimeKind.Unspecified).AddTicks(3869), "Carbonite web goalkeeper gloves are ergonomically designed to give easy fit", "Incredible Fresh Computer", 6 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 46, new DateTime(2020, 8, 20, 0, 32, 3, 917, DateTimeKind.Unspecified).AddTicks(7080), new DateTime(2021, 4, 3, 17, 55, 30, 913, DateTimeKind.Unspecified).AddTicks(82), "New range of formal shirts are designed keeping you in mind. With fits and styling that will make you stand apart", "Generic Wooden Mouse", 4 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 36, new DateTime(2020, 3, 15, 12, 51, 22, 448, DateTimeKind.Unspecified).AddTicks(7220), new DateTime(2020, 5, 6, 9, 10, 26, 427, DateTimeKind.Unspecified).AddTicks(1252), "Ergonomic executive chair upholstered in bonded black leather and PVC padded seat and back for all-day comfort and support", "Awesome Cotton Chair", 9 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 3, new DateTime(2020, 9, 29, 8, 11, 45, 724, DateTimeKind.Unspecified).AddTicks(4928), new DateTime(2020, 11, 19, 6, 23, 55, 71, DateTimeKind.Unspecified).AddTicks(7662), "Boston's most advanced compression wear technology increases muscle oxygenation, stabilizes active muscles", "Licensed Fresh Keyboard", 7 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 26, new DateTime(2018, 2, 13, 7, 36, 28, 333, DateTimeKind.Unspecified).AddTicks(7308), new DateTime(2020, 5, 8, 23, 54, 6, 558, DateTimeKind.Unspecified).AddTicks(9608), "The Football Is Good For Training And Recreational Purposes", "Handmade Plastic Ball", 8 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 35, new DateTime(2021, 1, 3, 22, 34, 11, 513, DateTimeKind.Unspecified).AddTicks(2464), new DateTime(2021, 9, 3, 19, 7, 24, 452, DateTimeKind.Unspecified).AddTicks(3865), "New range of formal shirts are designed keeping you in mind. With fits and styling that will make you stand apart", "Gorgeous Cotton Mouse", 10 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 4, new DateTime(2021, 2, 22, 7, 29, 51, 0, DateTimeKind.Unspecified).AddTicks(1105), new DateTime(2021, 7, 24, 21, 2, 38, 731, DateTimeKind.Unspecified).AddTicks(3823), "The Football Is Good For Training And Recreational Purposes", "Rustic Soft Fish", 6 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name" },
                values: new object[] { 31, new DateTime(2021, 5, 15, 13, 7, 13, 370, DateTimeKind.Unspecified).AddTicks(6944), new DateTime(2021, 11, 19, 15, 26, 25, 11, DateTimeKind.Unspecified).AddTicks(3391), "Boston's most advanced compression wear technology increases muscle oxygenation, stabilizes active muscles", "Gorgeous Steel Salad" });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 5, new DateTime(2018, 7, 20, 20, 2, 46, 425, DateTimeKind.Unspecified).AddTicks(8031), new DateTime(2021, 6, 10, 7, 23, 48, 396, DateTimeKind.Unspecified).AddTicks(8669), "New ABC 13 9370, 13.3, 5th Gen CoreA5-8250U, 8GB RAM, 256GB SSD, power UHD Graphics, OS 10 Home, OS Office A & J 2016", "Fantastic Steel Car", 3 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 52, new DateTime(2020, 4, 2, 16, 2, 10, 142, DateTimeKind.Unspecified).AddTicks(4200), new DateTime(2021, 7, 27, 8, 22, 59, 319, DateTimeKind.Unspecified).AddTicks(6034), "Ergonomic executive chair upholstered in bonded black leather and PVC padded seat and back for all-day comfort and support", "Fantastic Cotton Fish", 9 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 28, 2, 5, 4, 289, DateTimeKind.Unspecified).AddTicks(9966), "Magnam nihil facere veniam exercitationem dolorem.", new DateTime(2021, 9, 4, 6, 29, 30, 612, DateTimeKind.Unspecified).AddTicks(8418), "Velit provident numquam recusandae ut.", 20, 13, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 3, 27, 3, 41, 50, 454, DateTimeKind.Unspecified).AddTicks(9740), "Et et officiis eos mollitia omnis aut saepe in eum sunt.", new DateTime(2021, 4, 10, 13, 3, 47, 946, DateTimeKind.Unspecified).AddTicks(7013), "Ex ut et.", 80, 29, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2020, 3, 5, 4, 37, 41, 62, DateTimeKind.Unspecified).AddTicks(8898), "Nemo rerum eos doloremque inventore et ut natus in velit nostrum rerum autem.", new DateTime(2020, 3, 12, 8, 6, 3, 944, DateTimeKind.Unspecified).AddTicks(9676), "Consequatur dolorem.", 11, 30 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 9, 7, 16, 47, 39, 583, DateTimeKind.Unspecified).AddTicks(3476), "Repudiandae id est incidunt earum impedit dolore ut ut beatae quibusdam ex aut.", new DateTime(2020, 11, 20, 6, 32, 23, 994, DateTimeKind.Unspecified).AddTicks(5090), "Corrupti perspiciatis vero.", 69, 32, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2020, 10, 19, 12, 1, 55, 401, DateTimeKind.Unspecified).AddTicks(608), "Laboriosam incidunt quis consectetur itaque a non.", new DateTime(2020, 11, 8, 6, 45, 10, 971, DateTimeKind.Unspecified).AddTicks(9979), "Dolores maiores sunt.", 76, 14 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 1, 18, 8, 51, 53, 608, DateTimeKind.Unspecified).AddTicks(8781), "Voluptatem veritatis rerum consequatur culpa iure harum voluptas numquam quae consequuntur et modi.", new DateTime(2021, 4, 11, 11, 34, 13, 908, DateTimeKind.Unspecified).AddTicks(4526), "Ducimus perferendis sapiente.", 22, 15, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 12, 23, 1, 52, 0, 510, DateTimeKind.Unspecified).AddTicks(4097), "Reprehenderit illum quod occaecati et voluptates nulla omnis.", new DateTime(2021, 1, 10, 8, 4, 55, 291, DateTimeKind.Unspecified).AddTicks(8610), "Et sint.", 28, 26, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 8, 3, 19, 31, 18, 284, DateTimeKind.Unspecified).AddTicks(5738), "Est voluptates ea impedit illo sunt dolorem quia omnis.", new DateTime(2020, 12, 24, 6, 4, 5, 586, DateTimeKind.Unspecified).AddTicks(7604), "Ratione qui omnis voluptas expedita.", 68, 8, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 1, 8, 19, 23, 36, 827, DateTimeKind.Unspecified).AddTicks(7164), "Sint distinctio velit repellendus est possimus perferendis aut cum dolores molestias nesciunt.", new DateTime(2021, 4, 14, 23, 27, 30, 974, DateTimeKind.Unspecified).AddTicks(7278), "Consequuntur et inventore atque.", 33, 40, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 5, 27, 3, 54, 31, 890, DateTimeKind.Unspecified).AddTicks(1988), "Est temporibus qui voluptas mollitia magni quas magnam sit voluptas cumque ea.", new DateTime(2021, 8, 22, 18, 27, 9, 714, DateTimeKind.Unspecified).AddTicks(912), "Totam vel.", 50, 38 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 12, 6, 15, 14, 44, 201, DateTimeKind.Unspecified).AddTicks(5181), "Vero nemo dolore aliquam consectetur porro aperiam deserunt libero reiciendis adipisci officiis.", new DateTime(2020, 7, 2, 23, 37, 54, 206, DateTimeKind.Unspecified).AddTicks(8482), "Exercitationem beatae adipisci dignissimos fuga.", 41, 39, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 12, 19, 17, 42, 59, 348, DateTimeKind.Unspecified).AddTicks(9896), "Culpa cupiditate maiores autem ab ipsum.", new DateTime(2020, 1, 21, 1, 28, 11, 99, DateTimeKind.Unspecified).AddTicks(2718), "Distinctio consequatur aut quibusdam.", 62, 30, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 5, 8, 9, 59, 25, 472, DateTimeKind.Unspecified).AddTicks(2477), "Ullam exercitationem aut est a ipsam.", new DateTime(2021, 7, 17, 12, 8, 57, 931, DateTimeKind.Unspecified).AddTicks(6849), "Quis rerum magnam.", 54, 8, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 10, 28, 21, 54, 35, 161, DateTimeKind.Unspecified).AddTicks(9562), "Et nam minus sed molestiae voluptatibus.", new DateTime(2020, 11, 1, 6, 27, 2, 976, DateTimeKind.Unspecified).AddTicks(8612), "Rem repellendus illum accusamus.", 57, 17, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 1, 23, 7, 50, 44, 260, DateTimeKind.Unspecified).AddTicks(8100), "Quaerat distinctio ea excepturi optio odit odit ut suscipit qui et sed.", new DateTime(2019, 10, 30, 6, 35, 9, 384, DateTimeKind.Unspecified).AddTicks(4971), "Maiores nulla.", 28, 21 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 10, 14, 9, 3, 15, 116, DateTimeKind.Unspecified).AddTicks(512), "Enim est facere sed ea deleniti nisi quia consequatur.", new DateTime(2021, 10, 29, 18, 39, 12, 428, DateTimeKind.Unspecified).AddTicks(9873), "Velit quae eius occaecati vero.", 25, 38, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 2, 11, 17, 11, 18, 186, DateTimeKind.Unspecified).AddTicks(3453), "Qui facere exercitationem dolor sint est nisi esse consequatur.", new DateTime(2021, 2, 17, 12, 53, 24, 524, DateTimeKind.Unspecified).AddTicks(8928), "Hic dolores.", 60, 12, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2020, 3, 3, 18, 47, 10, 661, DateTimeKind.Unspecified).AddTicks(4713), "At odit qui adipisci in at enim quasi eaque qui praesentium voluptates qui.", new DateTime(2020, 6, 13, 14, 51, 2, 2, DateTimeKind.Unspecified).AddTicks(1266), "Voluptatibus sed consequatur aliquid sunt.", 62, 5 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 4, 6, 22, 15, 59, 79, DateTimeKind.Unspecified).AddTicks(118), "Voluptate sunt aut odio aut dolorem mollitia.", new DateTime(2021, 1, 30, 12, 36, 55, 508, DateTimeKind.Unspecified).AddTicks(3885), "Consectetur maiores fugit veritatis id.", 68, 23, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 6, 7, 5, 8, 27, 676, DateTimeKind.Unspecified).AddTicks(6847), "Omnis aut necessitatibus cumque dolorum optio et tempore.", new DateTime(2020, 5, 15, 2, 15, 17, 957, DateTimeKind.Unspecified).AddTicks(4977), "Nisi et architecto nisi.", 60, 10, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 1, 25, 15, 19, 57, 53, DateTimeKind.Unspecified).AddTicks(1936), "Veniam quaerat harum recusandae error et ut.", new DateTime(2021, 2, 1, 6, 52, 15, 928, DateTimeKind.Unspecified).AddTicks(7310), "Pariatur maiores.", 76, 12, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 6, 28, 8, 30, 34, 687, DateTimeKind.Unspecified).AddTicks(7208), "Molestias quos voluptatem nihil hic ipsam sed molestiae recusandae tempora sit expedita aut.", new DateTime(2021, 6, 30, 7, 8, 59, 655, DateTimeKind.Unspecified).AddTicks(3678), "Veritatis sed rerum.", 52, 25, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2020, 5, 17, 8, 54, 41, 895, DateTimeKind.Unspecified).AddTicks(5444), "Ipsa ut libero minus aut enim quaerat labore hic.", new DateTime(2020, 11, 22, 9, 50, 3, 287, DateTimeKind.Unspecified).AddTicks(9081), "Quo reprehenderit doloribus dolore perspiciatis.", 27, 22 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 3, 13, 7, 20, 10, 660, DateTimeKind.Unspecified).AddTicks(2000), "Est eos labore nisi quaerat nihil nemo molestiae qui occaecati et.", new DateTime(2021, 3, 15, 0, 12, 48, 470, DateTimeKind.Unspecified).AddTicks(389), "Explicabo quas harum quidem neque.", 70, 32, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 1, 3, 0, 30, 51, 732, DateTimeKind.Unspecified).AddTicks(98), "Et quidem non impedit nemo et dolorum rerum ea laborum vel temporibus ut.", new DateTime(2020, 10, 23, 13, 44, 56, 902, DateTimeKind.Unspecified).AddTicks(2090), "Consequuntur sint et quam.", 71, 15, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 8, 17, 3, 34, 31, 647, DateTimeKind.Unspecified).AddTicks(4524), "Nihil et tempora consequatur quas quisquam et.", new DateTime(2019, 9, 5, 2, 39, 46, 514, DateTimeKind.Unspecified).AddTicks(4334), "Enim iste perferendis fugit saepe.", 33, 9, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 6, 25, 8, 17, 17, 945, DateTimeKind.Unspecified).AddTicks(3539), "Harum qui quas rerum nihil odio corporis.", new DateTime(2021, 6, 26, 15, 22, 15, 783, DateTimeKind.Unspecified).AddTicks(48), "Sed odio nemo molestias.", 24, 24, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 11, 18, 5, 24, 26, 135, DateTimeKind.Unspecified).AddTicks(8602), "Rerum alias qui beatae at necessitatibus eos culpa ut culpa.", new DateTime(2020, 11, 19, 0, 37, 18, 613, DateTimeKind.Unspecified).AddTicks(5591), "Cupiditate eaque.", 42, 34, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 6, 14, 23, 12, 34, 419, DateTimeKind.Unspecified).AddTicks(7929), "Dolores tempore dolor neque repellendus quas molestiae est enim aspernatur.", new DateTime(2021, 6, 24, 4, 54, 34, 446, DateTimeKind.Unspecified).AddTicks(2498), "Ab at.", 2, 25 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 6, 11, 20, 12, 42, 975, DateTimeKind.Unspecified).AddTicks(794), "Nemo quod quaerat et exercitationem consequuntur laborum quia culpa.", new DateTime(2020, 9, 24, 21, 13, 9, 571, DateTimeKind.Unspecified).AddTicks(4661), "Odio voluptates.", 16, 11, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 2, 15, 2, 38, 56, 654, DateTimeKind.Unspecified).AddTicks(6229), "Voluptas est sunt non velit et.", new DateTime(2020, 3, 2, 7, 33, 17, 824, DateTimeKind.Unspecified).AddTicks(7533), "Ut et.", 49, 20, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 11, 20, 9, 0, 20, 885, DateTimeKind.Unspecified).AddTicks(8308), "Labore placeat corrupti aperiam quia dolor nisi molestiae illum asperiores autem omnis.", new DateTime(2020, 1, 5, 9, 41, 57, 229, DateTimeKind.Unspecified).AddTicks(7680), "Sed architecto explicabo dolorum perspiciatis.", 48, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2020, 5, 8, 10, 45, 32, 352, DateTimeKind.Unspecified).AddTicks(896), "Iusto similique exercitationem et quisquam mollitia est vel consequatur.", new DateTime(2020, 7, 28, 1, 28, 58, 516, DateTimeKind.Unspecified).AddTicks(4213), "Vero vel perspiciatis.", 10, 26 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2020, 8, 8, 11, 10, 0, 820, DateTimeKind.Unspecified).AddTicks(3561), "Quae quos aliquid minima porro assumenda quo voluptate voluptatum autem omnis dolorem.", new DateTime(2021, 8, 28, 23, 17, 45, 935, DateTimeKind.Unspecified).AddTicks(6361), "Aut iusto qui qui.", 60, 8 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 5, 9, 8, 25, 12, 211, DateTimeKind.Unspecified).AddTicks(5393), "Maiores maxime ut est sed eaque excepturi autem et.", new DateTime(2021, 6, 27, 23, 14, 51, 666, DateTimeKind.Unspecified).AddTicks(2586), "Praesentium placeat cumque.", 76, 19, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 14, 16, 12, 45, 993, DateTimeKind.Unspecified).AddTicks(5282), "Possimus voluptatem et non accusamus atque aut blanditiis et optio veritatis consequuntur recusandae fugiat.", new DateTime(2019, 10, 20, 8, 43, 25, 166, DateTimeKind.Unspecified).AddTicks(3829), "Perferendis ratione.", 31, 3, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 12, 13, 23, 49, 8, 672, DateTimeKind.Unspecified).AddTicks(605), "Consequatur nihil dolores consequatur cumque qui nesciunt molestias.", new DateTime(2021, 6, 6, 4, 38, 44, 697, DateTimeKind.Unspecified).AddTicks(8073), "Ut et eveniet soluta non.", 71, 40, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 7, 14, 6, 54, 30, 712, DateTimeKind.Unspecified).AddTicks(3847), "Minus id non delectus cumque id dignissimos laborum quidem nesciunt porro a nihil.", new DateTime(2019, 7, 21, 17, 42, 18, 90, DateTimeKind.Unspecified).AddTicks(2817), "Laborum beatae ut.", 55, 9, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 6, 13, 14, 40, 21, 855, DateTimeKind.Unspecified).AddTicks(6545), "Nobis beatae minus qui tenetur vel nesciunt error vitae hic adipisci est ea.", new DateTime(2020, 6, 15, 10, 52, 59, 352, DateTimeKind.Unspecified).AddTicks(2598), "Et repellat suscipit delectus molestiae.", 8, 30, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId" },
                values: new object[] { new DateTime(2021, 2, 18, 4, 15, 43, 46, DateTimeKind.Unspecified).AddTicks(6718), "Nemo quos nemo nemo molestiae fugit aut vel in assumenda aspernatur quia perferendis distinctio.", new DateTime(2021, 4, 2, 22, 9, 10, 644, DateTimeKind.Unspecified).AddTicks(8754), "Autem aperiam.", 45 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 41,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 1, 27, 11, 37, 40, 638, DateTimeKind.Unspecified).AddTicks(3360), "Impedit sapiente sed molestias repudiandae dolore delectus harum consequatur est assumenda labore quam.", new DateTime(2020, 11, 2, 15, 50, 59, 995, DateTimeKind.Unspecified).AddTicks(8089), "Laboriosam ad dicta.", 22, 26, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 42,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 6, 23, 8, 25, 10, 648, DateTimeKind.Unspecified).AddTicks(4090), "Numquam velit voluptate modi ut quidem.", new DateTime(2021, 7, 1, 16, 13, 21, 407, DateTimeKind.Unspecified).AddTicks(3918), "Molestiae tempore veniam quo.", 48, 25 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 43,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 11, 7, 3, 21, 49, 277, DateTimeKind.Unspecified).AddTicks(1343), "Ipsam quis saepe rem totam aperiam voluptas sint rerum quasi.", new DateTime(2020, 2, 18, 1, 9, 49, 530, DateTimeKind.Unspecified).AddTicks(7857), "Repudiandae ullam ut voluptatem aut.", 20, 30 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 44,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 7, 17, 40, 22, 887, DateTimeKind.Unspecified).AddTicks(5480), "Quod qui ipsum aut id quo et ab atque magni praesentium consequatur in.", new DateTime(2021, 7, 8, 12, 53, 28, 159, DateTimeKind.Unspecified).AddTicks(1995), "Nostrum quo.", 17, 25, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 45,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2018, 8, 24, 23, 31, 23, 175, DateTimeKind.Unspecified).AddTicks(2835), "Iste nemo maxime iure ipsum quisquam aut consequatur amet enim veritatis quasi ratione amet.", new DateTime(2018, 9, 3, 8, 52, 40, 294, DateTimeKind.Unspecified).AddTicks(8557), "Minus quis eveniet quisquam.", 68, 7 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 46,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 3, 11, 20, 1, 3, 901, DateTimeKind.Unspecified).AddTicks(7770), "Et praesentium qui ut unde voluptatibus.", new DateTime(2021, 4, 15, 20, 25, 26, 866, DateTimeKind.Unspecified).AddTicks(561), "Dolor cumque voluptas.", 44, 19 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 47,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 30, 5, 11, 3, 765, DateTimeKind.Unspecified).AddTicks(4583), "Voluptas blanditiis sit dicta natus dolores qui labore ab aut ipsum reprehenderit.", new DateTime(2020, 2, 21, 10, 41, 3, 940, DateTimeKind.Unspecified).AddTicks(7098), "Sunt occaecati aut.", 79, 3, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 48,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 1, 17, 0, 33, 36, 374, DateTimeKind.Unspecified).AddTicks(5037), "Quia nulla eius qui ut rerum quisquam perspiciatis eaque aut.", new DateTime(2019, 3, 13, 23, 0, 15, 944, DateTimeKind.Unspecified).AddTicks(7530), "Totam impedit eveniet ut.", 20, 4 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 49,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 7, 26, 3, 37, 50, 326, DateTimeKind.Unspecified).AddTicks(8607), "Iusto consequatur architecto temporibus suscipit ex eligendi blanditiis eum illo ipsum et nihil.", new DateTime(2020, 11, 27, 11, 34, 53, 114, DateTimeKind.Unspecified).AddTicks(5177), "Repellendus rerum qui.", 60, 40, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 50,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 6, 19, 20, 9, 12, 65, DateTimeKind.Unspecified).AddTicks(8860), "Veniam explicabo suscipit id tempora soluta quasi ipsam ab voluptatem qui est molestiae veritatis.", new DateTime(2021, 6, 22, 23, 25, 20, 661, DateTimeKind.Unspecified).AddTicks(7177), "Mollitia voluptas similique magni nostrum.", 79, 25 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 51,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 3, 28, 19, 43, 13, 733, DateTimeKind.Unspecified).AddTicks(1462), "Quae temporibus maiores cupiditate et soluta repellendus.", new DateTime(2020, 4, 11, 13, 15, 51, 103, DateTimeKind.Unspecified).AddTicks(7294), "Non harum qui ut alias.", 58, 33, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 52,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 3, 15, 9, 28, 3, 133, DateTimeKind.Unspecified).AddTicks(9110), "Autem aut qui totam voluptates cum ut facilis veritatis velit.", new DateTime(2020, 4, 24, 19, 35, 54, 814, DateTimeKind.Unspecified).AddTicks(3918), "Quis aut.", 30, 6, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 53,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 10, 12, 3, 30, 43, 490, DateTimeKind.Unspecified).AddTicks(905), "Sequi porro maiores et aut ex deserunt unde alias laborum.", new DateTime(2020, 11, 9, 23, 1, 37, 398, DateTimeKind.Unspecified).AddTicks(696), "Minus sunt harum minima sit.", 77, 34, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 54,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 11, 10, 19, 47, 0, 805, DateTimeKind.Unspecified).AddTicks(7775), "Blanditiis provident nihil ipsa ea saepe natus maiores et id.", new DateTime(2020, 11, 17, 17, 18, 59, 175, DateTimeKind.Unspecified).AddTicks(4858), "Tempora consequatur iste laboriosam.", 70, 34, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 55,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 6, 18, 0, 42, 32, 382, DateTimeKind.Unspecified).AddTicks(148), "Excepturi voluptate provident sed aut reprehenderit.", new DateTime(2021, 7, 6, 15, 10, 9, 679, DateTimeKind.Unspecified).AddTicks(3147), "Exercitationem libero aut.", 22, 25 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 56,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 4, 8, 15, 20, 2, 715, DateTimeKind.Unspecified).AddTicks(7469), "Sunt id praesentium aperiam error itaque officiis repellat odio ea itaque.", new DateTime(2019, 4, 27, 0, 55, 15, 209, DateTimeKind.Unspecified).AddTicks(7256), "Doloribus veritatis optio sit.", 54, 4, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 57,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 1, 3, 23, 45, 35, 44, DateTimeKind.Unspecified).AddTicks(2512), "Recusandae aut quis est temporibus sed rem aut incidunt temporibus sed nobis sint.", new DateTime(2021, 1, 6, 2, 10, 13, 481, DateTimeKind.Unspecified).AddTicks(7754), "Hic ab alias.", 76, 29, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 58,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 6, 6, 14, 34, 4, 887, DateTimeKind.Unspecified).AddTicks(981), "Tempore maxime qui molestiae similique quae nihil aut est sit tempore rerum mollitia quia.", new DateTime(2021, 7, 30, 16, 34, 8, 904, DateTimeKind.Unspecified).AddTicks(147), "Expedita sed tempore.", 72, 13, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 59,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 2, 7, 10, 50, 39, 44, DateTimeKind.Unspecified).AddTicks(6686), "Dolores numquam harum magnam ut consequatur qui nisi aliquid.", new DateTime(2021, 8, 5, 5, 8, 9, 137, DateTimeKind.Unspecified).AddTicks(1103), "Et qui.", 52, 11, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 60,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 11, 17, 10, 41, 55, 463, DateTimeKind.Unspecified).AddTicks(1321), "Rem quas dolore vitae totam magnam quo ipsum minus aliquam rerum laudantium.", new DateTime(2019, 11, 20, 8, 54, 14, 326, DateTimeKind.Unspecified).AddTicks(5536), "Et inventore non.", 58, 15 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 61,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 13, 16, 51, 36, 424, DateTimeKind.Unspecified).AddTicks(9035), "Autem in natus voluptas magni praesentium dolores quo.", new DateTime(2020, 9, 29, 12, 27, 24, 525, DateTimeKind.Unspecified).AddTicks(3596), "Quibusdam iusto odit veritatis quo.", 72, 28, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 62,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2018, 10, 25, 0, 58, 55, 989, DateTimeKind.Unspecified).AddTicks(2089), "Fugiat pariatur dicta quo quibusdam est voluptas rerum rerum error.", new DateTime(2020, 4, 25, 15, 14, 48, 298, DateTimeKind.Unspecified).AddTicks(9098), "Dolorem alias.", 33, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 63,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 4, 25, 19, 42, 41, 527, DateTimeKind.Unspecified).AddTicks(9314), "Eaque ad possimus et repellendus et quas iusto fugit.", new DateTime(2021, 6, 28, 17, 46, 44, 495, DateTimeKind.Unspecified).AddTicks(2249), "Molestiae nihil saepe eligendi.", 22, 24, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 64,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 5, 4, 0, 42, 28, 759, DateTimeKind.Unspecified).AddTicks(6187), "Molestiae autem est corrupti saepe cupiditate in officia consequatur itaque et.", new DateTime(2019, 12, 28, 15, 1, 45, 744, DateTimeKind.Unspecified).AddTicks(444), "Facere est.", 14, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 65,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 3, 13, 3, 29, 43, 413, DateTimeKind.Unspecified).AddTicks(7374), "Quia non voluptatibus quia totam quia ullam adipisci.", new DateTime(2021, 7, 20, 19, 30, 37, 170, DateTimeKind.Unspecified).AddTicks(1902), "In voluptas voluptates perferendis eius.", 72, 37, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 66,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 2, 10, 15, 11, 27, 75, DateTimeKind.Unspecified).AddTicks(7103), "Et error adipisci voluptatem voluptatem dolores atque.", new DateTime(2021, 3, 9, 12, 37, 37, 952, DateTimeKind.Unspecified).AddTicks(6640), "Omnis vel ut modi.", 57, 29, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 67,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 6, 19, 1, 42, 19, 691, DateTimeKind.Unspecified).AddTicks(2531), "Voluptatem aut vel et maxime voluptas est corporis corporis esse rem et adipisci explicabo.", new DateTime(2020, 6, 5, 4, 57, 46, 609, DateTimeKind.Unspecified).AddTicks(2265), "Nihil sed deleniti sapiente.", 2, 28, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 68,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 6, 25, 23, 26, 34, 363, DateTimeKind.Unspecified).AddTicks(2392), "Dolorem iste corrupti et iste voluptatem commodi numquam officia totam.", new DateTime(2021, 7, 24, 20, 41, 48, 200, DateTimeKind.Unspecified).AddTicks(2458), "Fugiat quo maiores.", 66, 37, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 69,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2018, 5, 29, 16, 30, 32, 259, DateTimeKind.Unspecified).AddTicks(3667), "Dignissimos voluptas perferendis placeat iste quo totam qui.", new DateTime(2019, 3, 27, 10, 54, 45, 55, DateTimeKind.Unspecified).AddTicks(6521), "Et nulla consectetur quasi et.", 20, 4 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 70,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 6, 16, 3, 33, 28, 895, DateTimeKind.Unspecified).AddTicks(7900), "Eaque ab aliquam expedita qui dolor similique alias voluptatem debitis voluptatem debitis.", new DateTime(2021, 6, 26, 12, 35, 51, 429, DateTimeKind.Unspecified).AddTicks(7714), "Voluptas aperiam.", 48, 40, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 71,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 9, 2, 6, 1, 57, 633, DateTimeKind.Unspecified).AddTicks(9124), "Impedit cupiditate placeat consequatur iure nemo doloribus non possimus et voluptatem laudantium aspernatur.", new DateTime(2018, 9, 2, 23, 52, 20, 832, DateTimeKind.Unspecified).AddTicks(4464), "Neque animi.", 58, 7, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 72,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 9, 29, 10, 3, 47, 8, DateTimeKind.Unspecified).AddTicks(9718), "Sapiente ipsam nihil explicabo dicta quia quidem et vitae.", new DateTime(2020, 10, 11, 18, 8, 38, 694, DateTimeKind.Unspecified).AddTicks(703), "Dignissimos placeat.", 43, 28, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 73,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 1, 7, 17, 3, 6, 168, DateTimeKind.Unspecified).AddTicks(9124), "Qui magnam consequuntur esse rerum consequatur.", new DateTime(2021, 4, 11, 17, 37, 51, 704, DateTimeKind.Unspecified).AddTicks(8432), "Et laboriosam accusamus culpa.", 2, 36, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 74,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2020, 4, 8, 19, 53, 59, 4, DateTimeKind.Unspecified).AddTicks(313), "Et ullam nostrum modi dolor exercitationem odit unde ut doloremque neque.", new DateTime(2020, 5, 4, 7, 45, 54, 60, DateTimeKind.Unspecified).AddTicks(9099), "Inventore nemo.", 31, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 75,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 7, 25, 10, 16, 3, 389, DateTimeKind.Unspecified).AddTicks(3875), "Consectetur nostrum non porro nemo consequatur quidem est distinctio veniam aut accusantium.", new DateTime(2019, 8, 26, 0, 22, 7, 971, DateTimeKind.Unspecified).AddTicks(9093), "Et cum consequatur.", 52, 39 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 76,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 10, 10, 23, 32, 20, 355, DateTimeKind.Unspecified).AddTicks(3184), "Unde velit temporibus sunt perferendis id quis vel ullam at et aut.", new DateTime(2019, 4, 19, 17, 3, 42, 308, DateTimeKind.Unspecified).AddTicks(2476), "Omnis nostrum nihil dolor eum.", 38, 4, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 77,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2020, 2, 23, 3, 20, 35, 321, DateTimeKind.Unspecified).AddTicks(1518), "In sit et et ut porro consequatur velit et.", new DateTime(2020, 3, 2, 0, 29, 10, 629, DateTimeKind.Unspecified).AddTicks(1589), "Nobis placeat minima quibusdam cupiditate.", 44, 20 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 78,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 3, 20, 8, 49, 26, 666, DateTimeKind.Unspecified).AddTicks(7500), "Eum laboriosam sapiente qui voluptas at.", new DateTime(2020, 4, 19, 8, 35, 43, 627, DateTimeKind.Unspecified).AddTicks(4906), "Molestiae asperiores et est.", 36, 33, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 79,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 5, 22, 19, 53, 1, 806, DateTimeKind.Unspecified).AddTicks(4054), "Deleniti culpa pariatur fugiat odit animi suscipit sunt.", new DateTime(2021, 5, 8, 14, 26, 54, 986, DateTimeKind.Unspecified).AddTicks(4664), "Necessitatibus eum sint nobis aut.", 61, 14, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 80,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 8, 25, 15, 12, 23, 138, DateTimeKind.Unspecified).AddTicks(6998), "Id quae eveniet nesciunt ipsam ut ipsa esse consequatur adipisci.", new DateTime(2020, 10, 25, 4, 56, 57, 954, DateTimeKind.Unspecified).AddTicks(7172), "Quasi velit sunt ut quis.", 2, 2, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 81,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 18, 19, 38, 39, 813, DateTimeKind.Unspecified).AddTicks(3856), "Commodi aut facere quas saepe expedita suscipit aut.", new DateTime(2021, 9, 20, 19, 55, 6, 163, DateTimeKind.Unspecified).AddTicks(4158), "Quaerat repellat.", 23, 27, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 82,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 8, 31, 7, 41, 51, 285, DateTimeKind.Unspecified).AddTicks(9790), "Est occaecati voluptas iste et similique sequi eveniet velit rerum maiores voluptas.", new DateTime(2021, 8, 31, 9, 34, 56, 278, DateTimeKind.Unspecified).AddTicks(4885), "Dolorum corrupti et.", 1, 38 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 83,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 3, 25, 2, 42, 39, 237, DateTimeKind.Unspecified).AddTicks(6603), "Eum vel id non eum placeat cum adipisci veniam qui praesentium sunt deleniti qui.", new DateTime(2020, 4, 9, 0, 2, 12, 370, DateTimeKind.Unspecified).AddTicks(4627), "Voluptatibus perspiciatis consequatur quas.", 80, 33, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 84,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 5, 14, 9, 4, 46, 897, DateTimeKind.Unspecified).AddTicks(828), "Voluptas ut minima nemo itaque quas necessitatibus cum amet sed blanditiis dolor.", new DateTime(2021, 6, 4, 3, 40, 37, 355, DateTimeKind.Unspecified).AddTicks(3729), "Iure eveniet eveniet.", 42, 8, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 85,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 11, 30, 3, 49, 20, 804, DateTimeKind.Unspecified).AddTicks(9869), "Velit quibusdam autem consectetur aut quae deserunt atque deleniti voluptatibus velit.", new DateTime(2020, 1, 12, 8, 41, 21, 775, DateTimeKind.Unspecified).AddTicks(734), "Cumque sit totam culpa aspernatur.", 63, 6, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 86,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 5, 29, 0, 52, 6, 45, DateTimeKind.Unspecified).AddTicks(9142), "Cumque voluptatem culpa pariatur recusandae quidem adipisci porro quis placeat eos.", new DateTime(2021, 8, 29, 9, 35, 28, 721, DateTimeKind.Unspecified).AddTicks(6491), "Dolor eum earum itaque.", 56, 27 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 87,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 6, 21, 10, 1, 57, 507, DateTimeKind.Unspecified).AddTicks(4244), "Quo fuga cum nesciunt dolores molestiae corrupti.", new DateTime(2020, 5, 23, 5, 32, 17, 259, DateTimeKind.Unspecified).AddTicks(5486), "Qui rerum aut optio.", 46, 28 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 88,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 9, 15, 16, 19, 17, 340, DateTimeKind.Unspecified).AddTicks(6254), "Aut tenetur quis voluptas eligendi qui consequatur.", new DateTime(2020, 9, 28, 10, 59, 25, 347, DateTimeKind.Unspecified).AddTicks(9665), "Rerum est.", 80, 32, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 89,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 10, 26, 5, 49, 1, 876, DateTimeKind.Unspecified).AddTicks(4973), "Molestiae et ipsam a ipsum omnis qui enim tempora animi omnis voluptatibus alias provident.", new DateTime(2020, 5, 8, 16, 12, 14, 136, DateTimeKind.Unspecified).AddTicks(6047), "Labore asperiores.", 3, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 90,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2020, 2, 17, 0, 57, 8, 71, DateTimeKind.Unspecified).AddTicks(8303), "Eveniet autem eos dolores sunt a nesciunt nihil est animi voluptas et doloribus.", new DateTime(2020, 12, 12, 23, 37, 6, 932, DateTimeKind.Unspecified).AddTicks(1076), "Nobis recusandae molestias eum et.", 8, 14 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 91,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2020, 11, 6, 11, 2, 56, 864, DateTimeKind.Unspecified).AddTicks(8199), "Animi quia consequatur veniam asperiores doloribus non laboriosam quis aut voluptas voluptate minus doloremque.", new DateTime(2021, 6, 9, 12, 31, 3, 193, DateTimeKind.Unspecified).AddTicks(2507), "Voluptatem autem qui beatae asperiores.", 36, 24 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 92,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 8, 26, 18, 26, 24, 193, DateTimeKind.Unspecified).AddTicks(9680), "Qui esse atque deserunt sunt ipsum ullam accusamus deserunt ut placeat.", new DateTime(2018, 8, 31, 12, 19, 52, 635, DateTimeKind.Unspecified).AddTicks(8909), "Iusto occaecati optio dolor.", 32, 7, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 93,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 10, 29, 7, 29, 26, 287, DateTimeKind.Unspecified).AddTicks(4822), "Aut et nemo velit quos voluptas quidem magnam totam vitae excepturi.", new DateTime(2020, 11, 10, 8, 31, 25, 469, DateTimeKind.Unspecified).AddTicks(9396), "Cumque blanditiis ab.", 36, 34, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 94,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 4, 6, 18, 54, 5, 530, DateTimeKind.Unspecified).AddTicks(3498), "Inventore nulla libero debitis ut eum nisi.", new DateTime(2020, 4, 27, 5, 25, 50, 994, DateTimeKind.Unspecified).AddTicks(4735), "Velit impedit laudantium ut.", 5, 5, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 95,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 5, 14, 18, 57, 25, 278, DateTimeKind.Unspecified).AddTicks(1238), "Distinctio et consequuntur necessitatibus labore voluptas aut ab.", new DateTime(2020, 8, 13, 20, 16, 3, 54, DateTimeKind.Unspecified).AddTicks(5694), "Aliquid quia reprehenderit mollitia.", 60, 2, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 96,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 6, 11, 19, 27, 49, 227, DateTimeKind.Unspecified).AddTicks(9940), "Laborum est consequatur et nisi quaerat ut optio error perferendis.", new DateTime(2019, 8, 9, 14, 26, 32, 184, DateTimeKind.Unspecified).AddTicks(2835), "Quasi libero consequatur laudantium.", 31, 9, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 97,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 3, 22, 10, 12, 23, 118, DateTimeKind.Unspecified).AddTicks(1475), "Sit autem reprehenderit omnis voluptatem et.", new DateTime(2021, 4, 25, 5, 55, 57, 541, DateTimeKind.Unspecified).AddTicks(1520), "Et perferendis dolorem id laboriosam.", 57, 22, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 98,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 9, 13, 10, 36, 1, 292, DateTimeKind.Unspecified).AddTicks(9115), "Ut et et eaque vel soluta eius ut commodi omnis voluptatem.", new DateTime(2021, 9, 17, 17, 3, 17, 903, DateTimeKind.Unspecified).AddTicks(6378), "Voluptatibus quisquam at quo dolor.", 44, 27, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 99,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 11, 16, 9, 12, 37, 494, DateTimeKind.Unspecified).AddTicks(4227), "Assumenda sequi culpa sunt aut iste quos et distinctio tenetur non ut sed quidem.", new DateTime(2021, 4, 12, 13, 50, 43, 641, DateTimeKind.Unspecified).AddTicks(6647), "Perferendis sint autem.", 57, 29, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 100,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 6, 7, 16, 58, 57, 201, DateTimeKind.Unspecified).AddTicks(7897), "Ut quas at libero dignissimos exercitationem quo velit doloribus eligendi sed voluptatum distinctio.", new DateTime(2021, 10, 5, 20, 11, 49, 575, DateTimeKind.Unspecified).AddTicks(610), "Quam dolores voluptas.", 23, 38 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 101,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 11, 14, 10, 0, 43, 171, DateTimeKind.Unspecified).AddTicks(2368), "Deleniti impedit veniam aperiam aut praesentium occaecati harum.", new DateTime(2019, 11, 26, 18, 48, 50, 647, DateTimeKind.Unspecified).AddTicks(4747), "Dolorem earum quam corporis.", 57, 16, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 102,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 10, 19, 12, 26, 5, 666, DateTimeKind.Unspecified).AddTicks(3479), "Iusto ut quia adipisci ab officia id eos et earum necessitatibus pariatur velit.", new DateTime(2020, 10, 20, 23, 43, 12, 29, DateTimeKind.Unspecified).AddTicks(1552), "Quia commodi sed perferendis et.", 65, 26, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 103,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 17, 6, 22, 52, 501, DateTimeKind.Unspecified).AddTicks(7433), "Et quia omnis quam et odit.", new DateTime(2021, 7, 24, 8, 23, 30, 733, DateTimeKind.Unspecified).AddTicks(8381), "Provident quia similique.", 37, 14, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 104,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 5, 30, 5, 18, 58, 786, DateTimeKind.Unspecified).AddTicks(3594), "Eum velit dignissimos et aut dolor quasi harum dignissimos ut ipsam et quo.", new DateTime(2021, 7, 3, 10, 45, 57, 38, DateTimeKind.Unspecified).AddTicks(805), "Et sed dicta voluptatibus numquam.", 6, 27, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 105,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 6, 12, 12, 45, 43, 102, DateTimeKind.Unspecified).AddTicks(4377), "Ut a cum id voluptas earum.", new DateTime(2021, 7, 2, 7, 42, 24, 630, DateTimeKind.Unspecified).AddTicks(8435), "Dolorem cumque doloremque.", 55, 8, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 106,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 12, 8, 14, 37, 9, 582, DateTimeKind.Unspecified).AddTicks(6997), "Enim nam doloribus reprehenderit enim praesentium autem et aspernatur.", new DateTime(2020, 12, 20, 19, 18, 37, 706, DateTimeKind.Unspecified).AddTicks(3504), "Consequatur facilis ipsa voluptatem autem.", 13, 11, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 107,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 5, 5, 3, 30, 4, 86, DateTimeKind.Unspecified).AddTicks(8875), "Odio incidunt eligendi et quam dolores odio aperiam.", new DateTime(2021, 6, 1, 9, 18, 36, 110, DateTimeKind.Unspecified).AddTicks(6550), "Odit non et.", 74, 36, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 108,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2020, 11, 30, 1, 9, 23, 438, DateTimeKind.Unspecified).AddTicks(253), "Aut voluptas est numquam rerum et nulla non repudiandae.", new DateTime(2021, 3, 28, 19, 23, 7, 332, DateTimeKind.Unspecified).AddTicks(5168), "Illum molestiae.", 21, 14 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 109,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2020, 10, 27, 3, 42, 52, 506, DateTimeKind.Unspecified).AddTicks(1715), "Culpa quam similique saepe accusamus molestiae repudiandae.", new DateTime(2020, 10, 27, 15, 48, 11, 164, DateTimeKind.Unspecified).AddTicks(5165), "Dolores at itaque.", 30, 17 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 110,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 8, 23, 13, 38, 11, 93, DateTimeKind.Unspecified).AddTicks(2480), "Consequatur quaerat vitae quos nemo nulla.", new DateTime(2021, 1, 2, 23, 34, 51, 165, DateTimeKind.Unspecified).AddTicks(8469), "Est maiores.", 41, 13, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 111,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 8, 27, 4, 26, 44, 118, DateTimeKind.Unspecified).AddTicks(558), "Amet optio nulla rerum sed hic.", new DateTime(2020, 12, 14, 7, 46, 46, 838, DateTimeKind.Unspecified).AddTicks(7799), "Labore et dicta.", 20, 32, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 112,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2020, 6, 11, 14, 54, 8, 833, DateTimeKind.Unspecified).AddTicks(3153), "Et debitis est dignissimos consequatur minus odit veritatis.", new DateTime(2020, 6, 22, 19, 36, 37, 438, DateTimeKind.Unspecified).AddTicks(4067), "Qui ut saepe quibusdam qui.", 15, 30 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 113,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 25, 6, 0, 43, 654, DateTimeKind.Unspecified).AddTicks(4694), "Ipsa totam esse atque nostrum praesentium vel animi et est adipisci quam impedit.", new DateTime(2021, 7, 25, 16, 37, 6, 40, DateTimeKind.Unspecified).AddTicks(3790), "A accusantium dolor provident.", 24, 40, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 114,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 3, 15, 16, 39, 6, 970, DateTimeKind.Unspecified).AddTicks(9189), "Eius accusamus iste enim porro molestiae quibusdam.", new DateTime(2021, 3, 23, 13, 36, 57, 994, DateTimeKind.Unspecified).AddTicks(5314), "Necessitatibus fugit voluptas voluptatum.", 64, 32, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 115,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 8, 2, 17, 12, 2, 572, DateTimeKind.Unspecified).AddTicks(7698), "Tempore autem sed eos quia quos sed.", new DateTime(2020, 12, 24, 12, 40, 50, 935, DateTimeKind.Unspecified).AddTicks(120), "Qui deleniti voluptatibus repellat.", 52, 22, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 116,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 5, 20, 16, 20, 20, 464, DateTimeKind.Unspecified).AddTicks(6315), "Et rerum quas saepe ea inventore temporibus sunt adipisci ut illum vel omnis necessitatibus.", new DateTime(2021, 3, 13, 9, 21, 21, 588, DateTimeKind.Unspecified).AddTicks(493), "Velit asperiores velit voluptatem quam.", 7, 40, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 117,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 13, 22, 11, 12, 444, DateTimeKind.Unspecified).AddTicks(9568), "Ex quo sequi numquam autem blanditiis.", new DateTime(2020, 1, 14, 16, 50, 8, 441, DateTimeKind.Unspecified).AddTicks(5066), "Veritatis aut sit dolor.", 20, 6, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 118,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 6, 28, 21, 52, 52, 321, DateTimeKind.Unspecified).AddTicks(1502), "Quam et aut amet non et.", new DateTime(2020, 7, 3, 6, 14, 10, 910, DateTimeKind.Unspecified).AddTicks(2418), "Atque magni.", 72, 28, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 119,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2020, 3, 21, 6, 11, 31, 282, DateTimeKind.Unspecified).AddTicks(9807), "Quia eum quae nisi harum ex in saepe nobis placeat nobis iure.", new DateTime(2020, 5, 18, 19, 41, 20, 326, DateTimeKind.Unspecified).AddTicks(3623), "Dolor possimus nulla iure.", 1, 30 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 120,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 8, 26, 0, 6, 47, 854, DateTimeKind.Unspecified).AddTicks(3431), "Magnam qui soluta a quis dolor ut earum tenetur corporis incidunt aut unde pariatur.", new DateTime(2021, 9, 26, 20, 59, 33, 615, DateTimeKind.Unspecified).AddTicks(9182), "Eos aut.", 46, 27, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 121,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 12, 2, 7, 12, 55, 642, DateTimeKind.Unspecified).AddTicks(3378), "Ab eaque ad a quia sint impedit veniam nihil.", new DateTime(2019, 5, 30, 20, 5, 42, 227, DateTimeKind.Unspecified).AddTicks(5282), "Exercitationem dolore ipsum doloribus.", 22, 3, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 122,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 1, 17, 22, 40, 38, 508, DateTimeKind.Unspecified).AddTicks(9989), "Ducimus blanditiis minima amet ratione delectus totam enim praesentium sunt dolores cumque.", new DateTime(2020, 3, 18, 2, 16, 17, 109, DateTimeKind.Unspecified).AddTicks(5833), "Et hic.", 26, 1, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 123,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 11, 7, 4, 34, 58, 593, DateTimeKind.Unspecified).AddTicks(5297), "Repellendus occaecati eum voluptas est delectus quisquam ut occaecati iure odit nisi excepturi.", new DateTime(2021, 3, 24, 20, 54, 22, 793, DateTimeKind.Unspecified).AddTicks(9343), "Fuga maiores.", 72, 29, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 124,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 10, 27, 11, 42, 19, 178, DateTimeKind.Unspecified).AddTicks(2285), "Temporibus consequatur et quo tempore autem earum.", new DateTime(2020, 11, 11, 14, 1, 22, 893, DateTimeKind.Unspecified).AddTicks(4713), "Quos ut illo.", 53, 2, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 125,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 11, 16, 11, 17, 59, 881, DateTimeKind.Unspecified).AddTicks(9737), "Itaque sit iure ex quos magnam sequi et blanditiis magnam enim dolores debitis qui.", new DateTime(2019, 12, 8, 4, 31, 53, 511, DateTimeKind.Unspecified).AddTicks(7744), "Ducimus ut quis repellat maxime.", 21, 16 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 126,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 11, 5, 4, 41, 12, 855, DateTimeKind.Unspecified).AddTicks(5367), "Incidunt vel natus inventore animi rerum occaecati sint ipsam.", new DateTime(2020, 6, 24, 4, 52, 45, 854, DateTimeKind.Unspecified).AddTicks(2971), "Iste culpa.", 33, 30 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 127,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 20, 5, 16, 58, 957, DateTimeKind.Unspecified).AddTicks(1446), "Repellendus similique rem architecto recusandae omnis velit excepturi nam quod.", new DateTime(2020, 8, 31, 21, 18, 5, 737, DateTimeKind.Unspecified).AddTicks(4493), "In laudantium occaecati exercitationem.", 13, 28, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 128,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 4, 11, 22, 30, 51, 588, DateTimeKind.Unspecified).AddTicks(8027), "Asperiores placeat ut laborum molestiae quidem amet magni autem non quaerat sunt.", new DateTime(2021, 5, 27, 2, 6, 11, 672, DateTimeKind.Unspecified).AddTicks(4621), "Recusandae consectetur omnis eos recusandae.", 11, 23, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 129,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 30, 10, 45, 24, 58, DateTimeKind.Unspecified).AddTicks(8719), "In ab laborum omnis consequuntur dolore tempore facilis enim delectus iusto omnis ab.", new DateTime(2019, 12, 22, 7, 18, 54, 370, DateTimeKind.Unspecified).AddTicks(141), "Incidunt aut.", 10, 23, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 130,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 6, 21, 20, 19, 44, 22, DateTimeKind.Unspecified).AddTicks(3435), "Quaerat quos et dignissimos et quas et molestiae.", new DateTime(2021, 7, 13, 5, 9, 27, 557, DateTimeKind.Unspecified).AddTicks(1458), "Quia sed hic ipsum odit.", 69, 36, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 131,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 3, 25, 20, 8, 29, 277, DateTimeKind.Unspecified).AddTicks(8542), "Natus dolorem ut occaecati sunt amet enim consequatur non blanditiis.", new DateTime(2020, 10, 9, 19, 45, 26, 551, DateTimeKind.Unspecified).AddTicks(5455), "Iure sint corrupti.", 41, 28, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 132,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 21, 11, 21, 11, 314, DateTimeKind.Unspecified).AddTicks(9143), "Sed laudantium aut fuga dolorem sequi accusantium.", new DateTime(2021, 8, 7, 13, 38, 6, 408, DateTimeKind.Unspecified).AddTicks(8180), "Accusamus possimus id.", 65, 36, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 133,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 2, 23, 9, 37, 6, 164, DateTimeKind.Unspecified).AddTicks(2111), "Est id in aut magni quaerat sunt ipsum.", new DateTime(2020, 6, 8, 5, 30, 33, 179, DateTimeKind.Unspecified).AddTicks(6562), "Molestiae est.", 63, 10, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 134,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2020, 3, 17, 20, 7, 31, 472, DateTimeKind.Unspecified).AddTicks(4749), "Voluptatem explicabo voluptatem sint aliquid itaque neque molestiae.", new DateTime(2021, 6, 22, 9, 6, 42, 12, DateTimeKind.Unspecified).AddTicks(4149), "Et harum repudiandae.", 22, 14 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 135,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 7, 22, 11, 12, 25, 342, DateTimeKind.Unspecified).AddTicks(492), "Magni ea quisquam et rerum saepe harum eum id ea quia aspernatur.", new DateTime(2020, 9, 7, 2, 34, 14, 153, DateTimeKind.Unspecified).AddTicks(4525), "Enim modi.", 61, 5, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 136,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 9, 5, 13, 51, 6, 391, DateTimeKind.Unspecified).AddTicks(3641), "Iure praesentium porro nihil sint laudantium aut consequatur exercitationem animi perspiciatis error deserunt.", new DateTime(2020, 12, 5, 4, 52, 40, 831, DateTimeKind.Unspecified).AddTicks(3136), "Excepturi quaerat.", 30, 13, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 137,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 4, 18, 0, 16, 850, DateTimeKind.Unspecified).AddTicks(2635), "Culpa a voluptas labore quisquam ipsam reiciendis excepturi dicta omnis sapiente a.", new DateTime(2020, 1, 12, 9, 28, 8, 479, DateTimeKind.Unspecified).AddTicks(6757), "Iure provident.", 3, 30, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 138,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 4, 4, 12, 50, 59, 21, DateTimeKind.Unspecified).AddTicks(1847), "Distinctio sed excepturi dicta atque nihil sed id nemo accusamus ad rerum.", new DateTime(2021, 7, 16, 19, 53, 8, 1, DateTimeKind.Unspecified).AddTicks(6679), "Provident sit nemo.", 60, 36, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 139,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 1, 9, 10, 14, 17, 698, DateTimeKind.Unspecified).AddTicks(6255), "Iste quis amet vero nisi omnis vero sint odio dolorem exercitationem.", new DateTime(2021, 3, 20, 14, 6, 58, 306, DateTimeKind.Unspecified).AddTicks(263), "Iure repellendus sint repellendus animi.", 22, 32, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 140,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 1, 26, 16, 15, 5, 334, DateTimeKind.Unspecified).AddTicks(5762), "Et doloribus aliquid soluta placeat et.", new DateTime(2021, 6, 18, 15, 30, 10, 951, DateTimeKind.Unspecified).AddTicks(9064), "Ut qui sunt soluta aut.", 70, 22 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 141,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2020, 4, 1, 10, 3, 46, 358, DateTimeKind.Unspecified).AddTicks(8822), "Perferendis omnis sed illo totam culpa hic fugit tenetur fugit.", new DateTime(2020, 4, 19, 6, 5, 27, 573, DateTimeKind.Unspecified).AddTicks(2233), "Fugit occaecati cum qui.", 77, 33 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 142,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "ProjectId" },
                values: new object[] { new DateTime(2019, 3, 26, 17, 3, 27, 939, DateTimeKind.Unspecified).AddTicks(573), "Aspernatur odit necessitatibus repudiandae qui quam eos at aspernatur porro facere ullam hic et.", new DateTime(2019, 5, 3, 18, 25, 41, 853, DateTimeKind.Unspecified).AddTicks(5181), "Ut rerum accusamus.", 4 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 143,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 2, 10, 11, 6, 2, 796, DateTimeKind.Unspecified).AddTicks(9861), "Porro quasi perspiciatis nihil repudiandae beatae voluptas sunt consequatur voluptate.", new DateTime(2020, 3, 8, 0, 59, 2, 585, DateTimeKind.Unspecified).AddTicks(1084), "In error quibusdam.", 38, 18, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 144,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 2, 24, 10, 25, 9, 881, DateTimeKind.Unspecified).AddTicks(7114), "Ut commodi excepturi modi voluptate et.", new DateTime(2020, 4, 9, 10, 52, 41, 389, DateTimeKind.Unspecified).AddTicks(5069), "Porro aspernatur.", 37, 2, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 145,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 4, 15, 14, 34, 20, 989, DateTimeKind.Unspecified).AddTicks(78), "Officiis odio itaque voluptatem tenetur dolor corrupti perspiciatis corrupti ipsum.", new DateTime(2020, 9, 15, 15, 54, 59, 663, DateTimeKind.Unspecified).AddTicks(4478), "Provident enim autem adipisci.", 56, 2, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 146,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 6, 30, 23, 37, 18, 727, DateTimeKind.Unspecified).AddTicks(7421), "Pariatur dolores doloribus debitis cupiditate iure ea.", new DateTime(2021, 8, 3, 19, 53, 52, 196, DateTimeKind.Unspecified).AddTicks(5384), "Unde natus ratione.", 40, 14, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 147,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 5, 28, 21, 13, 34, 443, DateTimeKind.Unspecified).AddTicks(4163), "Nesciunt asperiores minima amet voluptatem cum et voluptatem.", new DateTime(2021, 6, 24, 9, 9, 23, 745, DateTimeKind.Unspecified).AddTicks(2774), "Tenetur quia.", 62, 40, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 148,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 3, 1, 15, 37, 41, 962, DateTimeKind.Unspecified).AddTicks(9724), "Nam quod ducimus veritatis et error omnis qui qui officia in omnis sint perspiciatis.", new DateTime(2020, 6, 26, 7, 14, 40, 692, DateTimeKind.Unspecified).AddTicks(7651), "Voluptatibus placeat tempora.", 23, 23 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 149,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 1, 13, 9, 55, 38, 738, DateTimeKind.Unspecified).AddTicks(235), "Molestias a sunt non sint voluptas ut odit quis et fugiat.", new DateTime(2019, 2, 28, 1, 38, 46, 59, DateTimeKind.Unspecified).AddTicks(4254), "Amet ipsum numquam qui necessitatibus.", 71, 4 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 150,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 4, 22, 13, 54, 10, 919, DateTimeKind.Unspecified).AddTicks(6136), "Commodi qui nihil totam voluptatem et dolores cupiditate quisquam ipsum repudiandae inventore qui similique.", new DateTime(2021, 6, 7, 18, 32, 19, 276, DateTimeKind.Unspecified).AddTicks(6931), "Sit nobis iusto nihil sit.", 33, 8, 1 });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2021, 5, 2, 10, 30, 55, 23, DateTimeKind.Unspecified).AddTicks(2906), "Hallie Dooley" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2019, 3, 13, 12, 2, 10, 513, DateTimeKind.Unspecified).AddTicks(1184), "Ola Zboncak" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2020, 11, 19, 8, 54, 0, 203, DateTimeKind.Unspecified).AddTicks(6052), "Durward Mueller" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2018, 6, 11, 11, 7, 3, 261, DateTimeKind.Unspecified).AddTicks(1548), "Xander Paucek" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2019, 12, 7, 19, 53, 3, 488, DateTimeKind.Unspecified).AddTicks(6073), "Joshua O'Reilly" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2020, 11, 19, 0, 44, 25, 992, DateTimeKind.Unspecified).AddTicks(7993), "Efren Schuppe" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2020, 1, 25, 18, 55, 26, 410, DateTimeKind.Unspecified).AddTicks(3395), "Zoe Wehner" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2018, 8, 25, 19, 27, 43, 599, DateTimeKind.Unspecified).AddTicks(4660), "Natalie Turcotte" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2020, 8, 28, 10, 9, 34, 475, DateTimeKind.Unspecified).AddTicks(7315), "Joanny Walsh" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2018, 11, 1, 11, 7, 25, 369, DateTimeKind.Unspecified).AddTicks(2301), "Jessy Mann" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1978, 6, 17, 15, 24, 24, 68, DateTimeKind.Unspecified).AddTicks(629), new DateTime(1992, 12, 6, 1, 24, 38, 456, DateTimeKind.Unspecified).AddTicks(6325), "Jovani52@hotmail.com", "Deangelo", "Wiegand", 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1995, 7, 19, 19, 43, 5, 605, DateTimeKind.Unspecified).AddTicks(949), new DateTime(2007, 2, 9, 23, 39, 23, 798, DateTimeKind.Unspecified).AddTicks(3267), "Libbie87@yahoo.com", "Jazlyn", "Windler", 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2004, 12, 1, 2, 59, 42, 828, DateTimeKind.Unspecified).AddTicks(338), new DateTime(2015, 3, 13, 21, 43, 8, 233, DateTimeKind.Unspecified).AddTicks(4358), "Freida.Leffler@gmail.com", "Raegan", "Altenwerth", 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2010, 6, 24, 7, 12, 25, 211, DateTimeKind.Unspecified).AddTicks(86), new DateTime(2020, 2, 12, 14, 36, 11, 546, DateTimeKind.Unspecified).AddTicks(6948), "Lavon.Rodriguez99@gmail.com", "Kiara", "Satterfield", 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2002, 6, 1, 13, 54, 6, 516, DateTimeKind.Unspecified).AddTicks(3640), new DateTime(2015, 12, 17, 3, 15, 1, 638, DateTimeKind.Unspecified).AddTicks(857), "Jacey28@gmail.com", "Logan", "Okuneva", 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1973, 12, 8, 16, 45, 1, 268, DateTimeKind.Unspecified).AddTicks(1084), new DateTime(2010, 9, 3, 20, 45, 16, 166, DateTimeKind.Unspecified).AddTicks(7148), "Hudson.Feest99@yahoo.com", "Colten", "Gutkowski", 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1999, 4, 30, 16, 0, 26, 111, DateTimeKind.Unspecified).AddTicks(2210), new DateTime(2019, 9, 30, 18, 57, 58, 612, DateTimeKind.Unspecified).AddTicks(2275), "Mckenzie_Ullrich72@gmail.com", "Thurman", "Carroll", 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1970, 8, 23, 8, 27, 22, 244, DateTimeKind.Unspecified).AddTicks(5734), new DateTime(1981, 1, 19, 5, 7, 29, 68, DateTimeKind.Unspecified).AddTicks(2106), "Felicita_Bashirian45@gmail.com", "Madonna", "Heller", 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2011, 6, 20, 15, 24, 48, 859, DateTimeKind.Unspecified).AddTicks(8318), new DateTime(2019, 6, 21, 14, 32, 14, 955, DateTimeKind.Unspecified).AddTicks(2751), "Lenny.Crooks@yahoo.com", "Adrien", "Hilpert", 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1988, 3, 19, 5, 53, 58, 63, DateTimeKind.Unspecified).AddTicks(2971), new DateTime(1996, 3, 26, 2, 48, 11, 748, DateTimeKind.Unspecified).AddTicks(3340), "Kameron_Zemlak27@hotmail.com", "Rasheed", "Barrows", 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1974, 12, 25, 10, 8, 28, 512, DateTimeKind.Unspecified).AddTicks(1796), new DateTime(2007, 4, 12, 17, 28, 58, 296, DateTimeKind.Unspecified).AddTicks(6237), "Ayla_Gerlach49@yahoo.com", "Chanelle", "Lind", 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName" },
                values: new object[] { new DateTime(1986, 2, 7, 14, 3, 33, 138, DateTimeKind.Unspecified).AddTicks(8076), new DateTime(2013, 4, 7, 14, 19, 7, 154, DateTimeKind.Unspecified).AddTicks(9751), "Roscoe77@gmail.com", "Marjolaine", "Cronin" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1976, 7, 28, 9, 19, 46, 480, DateTimeKind.Unspecified).AddTicks(8136), new DateTime(1991, 5, 26, 23, 19, 31, 454, DateTimeKind.Unspecified).AddTicks(9110), "Enoch.Kozey16@hotmail.com", "Seamus", "Spinka", 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1998, 1, 2, 20, 34, 16, 20, DateTimeKind.Unspecified).AddTicks(406), new DateTime(2011, 7, 30, 14, 1, 58, 198, DateTimeKind.Unspecified).AddTicks(1991), "Riley_Smith@hotmail.com", "Amya", "Walsh", 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1993, 4, 21, 18, 52, 34, 113, DateTimeKind.Unspecified).AddTicks(2930), new DateTime(2010, 2, 26, 17, 35, 30, 628, DateTimeKind.Unspecified).AddTicks(3624), "Ewald.Lockman@gmail.com", "Sidney", "Dicki", 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1973, 3, 5, 19, 2, 30, 856, DateTimeKind.Unspecified).AddTicks(606), new DateTime(2006, 5, 8, 13, 16, 1, 140, DateTimeKind.Unspecified).AddTicks(1582), "Elisabeth_Powlowski78@gmail.com", "Alysha", "Gutkowski", 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1987, 5, 17, 13, 32, 51, 545, DateTimeKind.Unspecified).AddTicks(6308), new DateTime(2015, 12, 31, 11, 39, 9, 785, DateTimeKind.Unspecified).AddTicks(7851), "Liza1@gmail.com", "Viola", "Kilback", 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1986, 8, 31, 11, 54, 23, 795, DateTimeKind.Unspecified).AddTicks(8630), new DateTime(2018, 10, 20, 20, 58, 9, 116, DateTimeKind.Unspecified).AddTicks(4920), "Winnifred_OHara92@gmail.com", "Moshe", "Hayes", 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName" },
                values: new object[] { new DateTime(1971, 7, 3, 6, 57, 10, 683, DateTimeKind.Unspecified).AddTicks(9001), new DateTime(2010, 4, 11, 8, 37, 30, 621, DateTimeKind.Unspecified).AddTicks(3079), "Nicklaus.Schroeder@yahoo.com", "Melvina", "Pfannerstill" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1977, 9, 23, 8, 30, 57, 951, DateTimeKind.Unspecified).AddTicks(1976), new DateTime(1990, 6, 9, 14, 25, 56, 447, DateTimeKind.Unspecified).AddTicks(8705), "Tia_Rodriguez31@gmail.com", "Keanu", "Rice", 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1972, 10, 7, 22, 51, 53, 394, DateTimeKind.Unspecified).AddTicks(7801), new DateTime(2013, 4, 13, 12, 9, 51, 662, DateTimeKind.Unspecified).AddTicks(4587), "Vida4@gmail.com", "Lynn", "Murray", 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1988, 10, 29, 19, 31, 55, 581, DateTimeKind.Unspecified).AddTicks(7561), new DateTime(2014, 9, 9, 0, 44, 4, 191, DateTimeKind.Unspecified).AddTicks(3299), "Derek_Rowe@hotmail.com", "Maverick", "Reilly", 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2010, 6, 7, 19, 25, 10, 419, DateTimeKind.Unspecified).AddTicks(1466), new DateTime(2020, 9, 18, 9, 28, 11, 768, DateTimeKind.Unspecified).AddTicks(3998), "Chandler6@hotmail.com", "Fletcher", "Bednar", 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2005, 4, 9, 1, 37, 22, 483, DateTimeKind.Unspecified).AddTicks(9234), new DateTime(2021, 6, 10, 19, 37, 17, 252, DateTimeKind.Unspecified).AddTicks(9336), "Johann24@hotmail.com", "Lewis", "Dooley", 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1999, 11, 1, 5, 48, 49, 356, DateTimeKind.Unspecified).AddTicks(8514), new DateTime(2021, 4, 14, 15, 47, 40, 696, DateTimeKind.Unspecified).AddTicks(2682), "Dewayne40@hotmail.com", "Ava", "Witting", 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1976, 3, 29, 9, 3, 54, 581, DateTimeKind.Unspecified).AddTicks(6362), new DateTime(1997, 7, 16, 6, 8, 9, 577, DateTimeKind.Unspecified).AddTicks(2328), "Derrick_Sanford@yahoo.com", "Rosalia", "Towne", 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1978, 2, 17, 12, 11, 57, 548, DateTimeKind.Unspecified).AddTicks(4878), new DateTime(1992, 12, 31, 16, 20, 38, 956, DateTimeKind.Unspecified).AddTicks(1876), "Kaitlyn_Fisher@gmail.com", "Derek", "Cole", 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1991, 9, 13, 15, 38, 9, 318, DateTimeKind.Unspecified).AddTicks(2479), new DateTime(2006, 7, 16, 7, 20, 59, 92, DateTimeKind.Unspecified).AddTicks(9819), "Jessyca.Satterfield@yahoo.com", "Elmore", "Bode", 6 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2002, 8, 3, 6, 47, 40, 680, DateTimeKind.Unspecified).AddTicks(2952), new DateTime(2015, 9, 13, 10, 59, 38, 381, DateTimeKind.Unspecified).AddTicks(1058), "Electa_Johnson51@hotmail.com", "Pat", "Hudson", 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1970, 12, 17, 14, 53, 33, 599, DateTimeKind.Unspecified).AddTicks(2008), new DateTime(1994, 6, 27, 4, 45, 53, 276, DateTimeKind.Unspecified).AddTicks(2534), "Vladimir.Windler@yahoo.com", "Barton", "Kertzmann", 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1991, 3, 28, 16, 0, 54, 191, DateTimeKind.Unspecified).AddTicks(5754), new DateTime(2007, 12, 15, 21, 4, 29, 166, DateTimeKind.Unspecified).AddTicks(5467), "Bruce_Ruecker@gmail.com", "Dandre", "Kreiger", 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1975, 5, 12, 5, 43, 1, 964, DateTimeKind.Unspecified).AddTicks(9987), new DateTime(2002, 11, 28, 8, 13, 21, 530, DateTimeKind.Unspecified).AddTicks(1636), "Robb2@hotmail.com", "Glen", "Shanahan", 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1993, 8, 12, 5, 30, 21, 2, DateTimeKind.Unspecified).AddTicks(9863), new DateTime(2017, 10, 29, 4, 21, 37, 737, DateTimeKind.Unspecified).AddTicks(7173), "Justyn.Doyle@hotmail.com", "Isaac", "Klein", 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName" },
                values: new object[] { new DateTime(1999, 2, 11, 3, 14, 2, 58, DateTimeKind.Unspecified).AddTicks(1212), new DateTime(2011, 1, 3, 4, 58, 37, 948, DateTimeKind.Unspecified).AddTicks(6482), "Kane_Maggio15@hotmail.com", "Mohamed", "Konopelski" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2003, 8, 16, 12, 5, 44, 745, DateTimeKind.Unspecified).AddTicks(7316), new DateTime(2017, 10, 31, 13, 37, 57, 836, DateTimeKind.Unspecified).AddTicks(2960), "Osborne_Gorczany10@hotmail.com", "Alexzander", "Heaney", 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1987, 8, 26, 2, 22, 12, 778, DateTimeKind.Unspecified).AddTicks(7131), new DateTime(2006, 7, 2, 6, 57, 41, 129, DateTimeKind.Unspecified).AddTicks(7558), "Omer_Rohan11@yahoo.com", "Johann", "Mraz", 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1995, 1, 1, 13, 21, 27, 529, DateTimeKind.Unspecified).AddTicks(5144), new DateTime(2019, 2, 5, 7, 0, 24, 140, DateTimeKind.Unspecified).AddTicks(7703), "Seth95@hotmail.com", "Maeve", "Rodriguez", 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1993, 5, 14, 22, 1, 53, 66, DateTimeKind.Unspecified).AddTicks(6298), new DateTime(2009, 12, 16, 6, 49, 30, 959, DateTimeKind.Unspecified).AddTicks(8334), "Bernhard_Rempel97@yahoo.com", "Deangelo", "Welch", 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1979, 8, 11, 12, 18, 38, 993, DateTimeKind.Unspecified).AddTicks(9187), new DateTime(2005, 10, 18, 19, 16, 42, 816, DateTimeKind.Unspecified).AddTicks(6111), "Ernestina.Watsica90@hotmail.com", "Bella", "Kirlin", 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1981, 11, 14, 14, 46, 49, 105, DateTimeKind.Unspecified).AddTicks(3487), new DateTime(1994, 9, 13, 14, 1, 1, 154, DateTimeKind.Unspecified).AddTicks(8589), "Dennis.Stehr@hotmail.com", "Geoffrey", "Hintz", 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 41,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2014, 4, 9, 17, 43, 53, 711, DateTimeKind.Unspecified).AddTicks(2114), new DateTime(2021, 7, 6, 22, 2, 46, 722, DateTimeKind.Unspecified).AddTicks(259), "Maria.Dietrich@gmail.com", "Nils", "Schroeder", 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 42,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1991, 4, 24, 20, 47, 33, 635, DateTimeKind.Unspecified).AddTicks(9287), new DateTime(2016, 10, 17, 7, 1, 44, 55, DateTimeKind.Unspecified).AddTicks(1793), "Kaycee_Gulgowski28@yahoo.com", "Saul", "Zemlak", 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 43,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2009, 12, 15, 7, 52, 56, 793, DateTimeKind.Unspecified).AddTicks(6440), new DateTime(2019, 11, 1, 9, 29, 3, 483, DateTimeKind.Unspecified).AddTicks(8781), "Kelvin.Batz@yahoo.com", "Maddison", "Ward", 6 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 44,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1975, 8, 12, 8, 50, 52, 277, DateTimeKind.Unspecified).AddTicks(161), new DateTime(1988, 12, 19, 18, 57, 8, 42, DateTimeKind.Unspecified).AddTicks(7549), "Ezra_Nienow73@hotmail.com", "Destin", "Goodwin", 10 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 45,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2009, 4, 22, 21, 13, 37, 528, DateTimeKind.Unspecified).AddTicks(9020), new DateTime(2020, 11, 14, 14, 59, 2, 602, DateTimeKind.Unspecified).AddTicks(460), "Gideon95@gmail.com", "Conrad", "Huel", 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 46,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName" },
                values: new object[] { new DateTime(1980, 7, 14, 0, 53, 51, 57, DateTimeKind.Unspecified).AddTicks(2613), new DateTime(2006, 7, 28, 8, 22, 23, 315, DateTimeKind.Unspecified).AddTicks(3972), "Bella.Thompson15@yahoo.com", "Parker", "Hickle" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 47,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1982, 6, 26, 12, 52, 52, 854, DateTimeKind.Unspecified).AddTicks(9456), new DateTime(2018, 4, 4, 22, 53, 34, 161, DateTimeKind.Unspecified).AddTicks(2015), "Miller65@yahoo.com", "Jaydon", "Muller", 6 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 48,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1981, 12, 24, 2, 28, 30, 906, DateTimeKind.Unspecified).AddTicks(1401), new DateTime(1999, 6, 6, 0, 29, 22, 817, DateTimeKind.Unspecified).AddTicks(6979), "Isidro.Lind@hotmail.com", "Fermin", "Oberbrunner", 6 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 49,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2003, 9, 10, 14, 21, 31, 197, DateTimeKind.Unspecified).AddTicks(8600), new DateTime(2019, 6, 14, 9, 17, 59, 175, DateTimeKind.Unspecified).AddTicks(2151), "Janice.Upton@yahoo.com", "Adrianna", "Parisian", 6 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 50,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1971, 6, 21, 12, 39, 40, 294, DateTimeKind.Unspecified).AddTicks(2601), new DateTime(2018, 2, 18, 5, 31, 5, 583, DateTimeKind.Unspecified).AddTicks(5337), "Rahsaan.Reilly66@yahoo.com", "Kory", "Towne", 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 51,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1993, 10, 1, 2, 11, 23, 330, DateTimeKind.Unspecified).AddTicks(7628), new DateTime(2020, 3, 31, 21, 40, 41, 225, DateTimeKind.Unspecified).AddTicks(7330), "Howard_Raynor34@yahoo.com", "Leatha", "Wilkinson", 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 52,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1987, 12, 13, 23, 45, 14, 463, DateTimeKind.Unspecified).AddTicks(8990), new DateTime(2020, 8, 27, 22, 15, 36, 408, DateTimeKind.Unspecified).AddTicks(1925), "Price81@gmail.com", "Trenton", "Barrows", 10 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 53,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2013, 6, 12, 7, 56, 40, 598, DateTimeKind.Unspecified).AddTicks(6124), new DateTime(2021, 6, 23, 6, 6, 57, 905, DateTimeKind.Unspecified).AddTicks(4625), "Jalon_Bechtelar77@hotmail.com", "Jenifer", "Hermann", 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 54,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1994, 10, 7, 23, 4, 33, 868, DateTimeKind.Unspecified).AddTicks(4870), new DateTime(2013, 9, 28, 4, 38, 20, 311, DateTimeKind.Unspecified).AddTicks(6703), "Moises19@yahoo.com", "Gennaro", "Fadel", 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 55,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1980, 12, 22, 22, 44, 44, 376, DateTimeKind.Unspecified).AddTicks(7417), new DateTime(2016, 11, 25, 21, 10, 52, 837, DateTimeKind.Unspecified).AddTicks(4248), "Vanessa_Mann49@hotmail.com", "Desiree", "Moen", 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 56,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2011, 5, 27, 1, 4, 11, 685, DateTimeKind.Unspecified).AddTicks(2922), new DateTime(2020, 8, 6, 23, 32, 4, 695, DateTimeKind.Unspecified).AddTicks(6671), "Layla_Nolan83@gmail.com", "Veda", "Huels", 6 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 57,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1974, 10, 10, 19, 15, 35, 427, DateTimeKind.Unspecified).AddTicks(1138), new DateTime(2006, 10, 12, 17, 36, 57, 78, DateTimeKind.Unspecified).AddTicks(1914), "Quinn52@yahoo.com", "Hillary", "O'Hara", 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 58,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1979, 10, 24, 23, 7, 24, 836, DateTimeKind.Unspecified).AddTicks(5232), new DateTime(2014, 11, 22, 23, 13, 36, 744, DateTimeKind.Unspecified).AddTicks(2643), "Neil.Gottlieb@gmail.com", "George", "Kuhlman", 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 59,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1974, 4, 14, 0, 54, 25, 896, DateTimeKind.Unspecified).AddTicks(8092), new DateTime(1984, 10, 12, 14, 18, 18, 940, DateTimeKind.Unspecified).AddTicks(5966), "Herta12@yahoo.com", "Monte", "O'Connell", 6 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 60,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2003, 11, 23, 3, 57, 58, 663, DateTimeKind.Unspecified).AddTicks(7698), new DateTime(2012, 6, 5, 16, 36, 27, 806, DateTimeKind.Unspecified).AddTicks(784), "Maximo39@hotmail.com", "Mollie", "O'Kon", 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 61,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName" },
                values: new object[] { new DateTime(1986, 4, 27, 3, 18, 43, 717, DateTimeKind.Unspecified).AddTicks(7132), new DateTime(2006, 11, 5, 7, 13, 41, 824, DateTimeKind.Unspecified).AddTicks(3936), "Otilia_Marquardt85@gmail.com", "Viva", "Conn" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 62,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2013, 6, 22, 15, 26, 58, 67, DateTimeKind.Unspecified).AddTicks(670), new DateTime(2021, 6, 26, 19, 45, 0, 517, DateTimeKind.Unspecified).AddTicks(6088), "Rhett13@gmail.com", "Leonardo", "Cassin", 10 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 63,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1995, 6, 23, 7, 43, 17, 386, DateTimeKind.Unspecified).AddTicks(8168), new DateTime(2007, 10, 10, 5, 59, 7, 396, DateTimeKind.Unspecified).AddTicks(9916), "Blaze_Reinger@gmail.com", "Hollis", "Baumbach", 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 64,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2008, 1, 21, 1, 35, 50, 702, DateTimeKind.Unspecified).AddTicks(7272), new DateTime(2021, 1, 27, 12, 0, 21, 513, DateTimeKind.Unspecified).AddTicks(7004), "Elsa_McLaughlin@hotmail.com", "Mitchell", "Cruickshank", 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 65,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2008, 10, 18, 5, 18, 29, 217, DateTimeKind.Unspecified).AddTicks(4006), new DateTime(2018, 8, 27, 6, 8, 53, 276, DateTimeKind.Unspecified).AddTicks(8827), "Everardo_Carroll@yahoo.com", "Doris", "Renner", 10 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 66,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1999, 6, 26, 6, 51, 10, 234, DateTimeKind.Unspecified).AddTicks(4430), new DateTime(2019, 5, 7, 21, 43, 56, 292, DateTimeKind.Unspecified).AddTicks(4836), "Sophie14@hotmail.com", "Dimitri", "Schaden", 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 67,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2013, 10, 13, 5, 45, 11, 803, DateTimeKind.Unspecified).AddTicks(7308), new DateTime(2021, 7, 27, 23, 14, 8, 289, DateTimeKind.Unspecified).AddTicks(3072), "Ewell.Koch@gmail.com", "Fabiola", "Goyette", 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 68,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1970, 2, 28, 9, 22, 55, 392, DateTimeKind.Unspecified).AddTicks(8883), new DateTime(1986, 9, 9, 18, 7, 10, 182, DateTimeKind.Unspecified).AddTicks(4121), "Vaughn.Padberg75@gmail.com", "Damaris", "Gorczany", 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 69,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2004, 6, 20, 1, 36, 44, 394, DateTimeKind.Unspecified).AddTicks(516), new DateTime(2016, 12, 8, 4, 54, 33, 59, DateTimeKind.Unspecified).AddTicks(3558), "Rossie.Weimann@gmail.com", "Maida", "Daugherty", 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 70,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2009, 6, 8, 16, 44, 15, 298, DateTimeKind.Unspecified).AddTicks(7646), new DateTime(2020, 3, 3, 9, 49, 12, 813, DateTimeKind.Unspecified).AddTicks(2912), "Antonina.Ziemann@yahoo.com", "Caroline", "Altenwerth", 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 71,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2007, 1, 7, 6, 46, 6, 326, DateTimeKind.Unspecified).AddTicks(4466), new DateTime(2019, 4, 8, 17, 2, 12, 596, DateTimeKind.Unspecified).AddTicks(1974), "Olga_Zboncak22@gmail.com", "Keon", "Nader", 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 72,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2012, 11, 26, 19, 18, 31, 787, DateTimeKind.Unspecified).AddTicks(9574), new DateTime(2020, 12, 20, 11, 24, 41, 283, DateTimeKind.Unspecified).AddTicks(3378), "Marlen.Brakus51@gmail.com", "Mafalda", "O'Conner", 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 73,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1978, 2, 20, 10, 22, 49, 663, DateTimeKind.Unspecified).AddTicks(5986), new DateTime(2016, 10, 15, 10, 29, 51, 771, DateTimeKind.Unspecified).AddTicks(6142), "Dion_Jacobs10@gmail.com", "Nasir", "Braun", 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 74,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2007, 5, 22, 2, 19, 12, 330, DateTimeKind.Unspecified).AddTicks(6348), new DateTime(2021, 2, 25, 20, 40, 12, 129, DateTimeKind.Unspecified).AddTicks(4188), "Carley.Kozey74@gmail.com", "Roderick", "Lebsack", 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 75,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1986, 5, 25, 14, 12, 14, 277, DateTimeKind.Unspecified).AddTicks(7723), new DateTime(2013, 8, 2, 23, 40, 39, 210, DateTimeKind.Unspecified).AddTicks(5927), "Jayde_Howell76@yahoo.com", "Selina", "Kuvalis", 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 76,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName" },
                values: new object[] { new DateTime(1993, 12, 28, 17, 19, 3, 723, DateTimeKind.Unspecified).AddTicks(331), new DateTime(2016, 3, 27, 19, 47, 38, 682, DateTimeKind.Unspecified).AddTicks(8294), "Merl85@hotmail.com", "Flossie", "Toy" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 77,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1983, 9, 4, 6, 11, 36, 256, DateTimeKind.Unspecified).AddTicks(1298), new DateTime(1998, 1, 30, 0, 17, 2, 377, DateTimeKind.Unspecified).AddTicks(3444), "Madaline.Okuneva63@hotmail.com", "Juanita", "Hansen", 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 78,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1987, 2, 9, 2, 11, 53, 950, DateTimeKind.Unspecified).AddTicks(452), new DateTime(2006, 3, 12, 2, 14, 18, 496, DateTimeKind.Unspecified).AddTicks(5186), "Keaton.Ullrich78@gmail.com", "Dixie", "Turner", 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 79,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2009, 12, 20, 11, 33, 22, 679, DateTimeKind.Unspecified).AddTicks(7422), new DateTime(2019, 1, 13, 4, 1, 16, 125, DateTimeKind.Unspecified).AddTicks(5521), "Bridie.Ferry88@gmail.com", "Linda", "McDermott", 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 80,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1975, 9, 16, 8, 34, 42, 210, DateTimeKind.Unspecified).AddTicks(7284), new DateTime(1986, 1, 7, 20, 39, 21, 197, DateTimeKind.Unspecified).AddTicks(798), "Kasandra_Shields30@gmail.com", "Frederique", "Hermann", 5 });
        }
    }
}
