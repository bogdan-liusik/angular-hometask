﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace ProjectStructure.DAL.Interfaces
{
    public interface IRepository<TEntity> where TEntity: class
    {
        List<TEntity> GetAll(Expression<Func<TEntity,bool>> filter = null);
        TEntity Get(int id);
        TEntity Create(TEntity entity);
        TEntity Update(TEntity entityToUpdate);
        void Delete(int id);
    }
}