﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ProjectStructure.Common.DTO.Team;

namespace ProjectStructure.BLL.Interfaces
{
    public interface ITeamService
    {
        Task<ICollection<TeamDTO>> GetAllTeams();
        Task<TeamDTO> GetTeamById(int projectId);
        Task<TeamDTO> CreateTeam(TeamCreateDTO project);
        Task<TeamDTO> UpdateTeam(TeamUpdateDTO project);
        Task DeleteTeam(int projectId);
    }
}