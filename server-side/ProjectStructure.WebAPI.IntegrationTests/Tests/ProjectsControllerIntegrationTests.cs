﻿using Xunit;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ProjectStructure.Common.DTO.Project;
using ProjectStructure.WebAPI.IntegrationTests.Factories;

namespace ProjectStructure.WebAPI.IntegrationTests.Tests
{
    public class ProjectsControllerIntegrationTests : IClassFixture<CustomWebApplicationFactory>
    {
        private readonly HttpClient _httpClient;
        private readonly string _endpoint;

        public ProjectsControllerIntegrationTests(CustomWebApplicationFactory factory)
        {
            _httpClient = factory.CreateClient();
            _endpoint = "api/projects";
        }

        [Fact]
        public async Task CreateProject_WhenProjectModelValid_ThenStatusCode201Created()
        {
            var project = new ProjectCreateDTO()
            {
                Name = "ProjectName",
                Description = "ProjectsDescription",
                AuthorId = 1,
                TeamId = 1,
                CreatedAt = DateTime.Now,
                Deadline = DateTime.Now.AddMonths(5)
            };
            
            var response = await _httpClient.PostAsync(
                    requestUri: _endpoint, 
                    content: new StringContent(JsonConvert.SerializeObject(project), 
                    encoding: Encoding.UTF8, 
                    mediaType: "application/json"));
            
            Assert.Equal(HttpStatusCode.Created, response.StatusCode);
        }

        [Theory]
        [InlineData("{\"id\":0,\"name\":\"ProjectName\",\"description\":\"ProjectDescription\",\"createdAt\":\"2021 - 06 - 07T16: 37:32.3101401\",\"deadline\":\"2021 - 09 - 07T16: 37:32.3101401\",\"authorId\":0,\"teamId\":1}")]
        [InlineData("{\"id\":0,\"name\":\"ProjectName\",\"description\": null,\"createdAt\":\"2021 - 06 - 07T16: 37:32.3101401\",\"deadline\":\"2021 - 09 - 07T16: 37:32.3101401\",\"authorId\":1,\"teamId\":1}")]
        [InlineData("{\"id\":0,\"name\":\"ProjectName\",\"description\":\"ProjectDescription\",\"createdAt\":\"2021 - 06 - 07T16: 37:32.3101401\",\"deadline\":\"2021 - 09 - 07T16: 37:32.3101401\",\"authorId\":1,\"teamId\":1}")]
        [InlineData("{\"id\":0,\"name\":\"ProjectName\",\"description\": \".\",\"createdAt\":\"2021 - 06 - 07T16: 37:32.3101401\",\"deadline\":\"2021 - 09 - 07T16: 37:32.3101401\"\"authorId\":1,\"teamId\":1}")]
        public async Task CreateProject_WhenModelsInvalid_ThanStatusCode400BadRequest(string content)
        {
            var response = await _httpClient.PostAsync(
                    requestUri: _endpoint, 
                    content: new StringContent(content, 
                    encoding: Encoding.UTF8, 
                    mediaType: "application/json"));
         
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }
    }
}