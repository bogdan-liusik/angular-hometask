﻿using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ProjectStructure.Common.DTO.Task;
using ProjectStructure.WebAPI.IntegrationTests.Factories;
using Xunit;

namespace ProjectStructure.WebAPI.IntegrationTests.Tests
{
    public class TasksControllerIntergrationTests : IClassFixture<CustomWebApplicationFactory>
    {
        private readonly HttpClient _httpClient;
        private readonly string _endpoint;

        public TasksControllerIntergrationTests(CustomWebApplicationFactory factory)
        {
            _httpClient = factory.CreateClient();
            _endpoint = "api/tasks";
        }
        
        [Fact]
        public async Task DeleteTask_WhenTaskExist_ThanStatusCode204NoContent()
        {
            var task = new TaskCreateDTO() 
            { 
                Name = "TaskName",
                Description = "TaskDescription",
                ProjectId = 1,
                PerformerId = 1,
                CreatedAt = DateTime.Now,
            };
            
            var response = await _httpClient.PostAsync(
                    requestUri: "api/tasks", 
                    content: new StringContent(JsonConvert.SerializeObject(task), 
                    encoding:Encoding.UTF8, 
                    mediaType: "application/json"));
            
            var createdTask = JsonConvert.DeserializeObject<TaskDTO>(await response.Content.ReadAsStringAsync());
            var deleteTaskResponse = await _httpClient.DeleteAsync($"{_endpoint}/{createdTask.Id}");
            
            Assert.Equal(HttpStatusCode.NoContent, deleteTaskResponse.StatusCode);
        }
        
        [Fact]
        public async Task DeleteTask_WhenTaskDoesNotExist_ThanStatusCode204NoContent()
        {
            var task = new TaskCreateDTO() 
            { 
                Name = "TaskName",
                Description = "TaskDescription",
                PerformerId = 1,
                CreatedAt = DateTime.Now,
            };
            
            var response = await _httpClient.PostAsync(
                    requestUri: "api/tasks", 
                    content: new StringContent(JsonConvert.SerializeObject(task), 
                    encoding:Encoding.UTF8, 
                    mediaType: "application/json"));
            
            var createdTask = JsonConvert.DeserializeObject<TaskDTO>(await response.Content.ReadAsStringAsync());
            await _httpClient.DeleteAsync($"{_endpoint}/{createdTask.Id}");
            var deleteTaskResponse = await _httpClient.DeleteAsync($"{_endpoint}/{createdTask.Id}");
            
            Assert.Equal(HttpStatusCode.NotFound, deleteTaskResponse.StatusCode);
        }
    }
}