﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ProjectStructure.Common.DTO.LinqQueryResults;
using ProjectStructure.Common.DTO.Project;
using ProjectStructure.Common.DTO.Task;
using ProjectStructure.Common.DTO.User;
using ProjectStructure.WebAPI.IntegrationTests.Factories;
using Xunit;
using Xunit.Abstractions;

namespace ProjectStructure.WebAPI.IntegrationTests.Tests
{
    public class LinqQueriesControllerIntegrationTests: IClassFixture<CustomWebApplicationFactory>
    {
        private readonly ITestOutputHelper _testOutputHelper;
        private readonly HttpClient _httpClient;
        private readonly string _endpoint;

        public LinqQueriesControllerIntegrationTests(CustomWebApplicationFactory factory, ITestOutputHelper testOutputHelper)
        {
            _testOutputHelper = testOutputHelper;
            _httpClient = factory.CreateClient();
            _endpoint = "api/LinqQueries";
        }

        [Fact]
        public async Task Query1_WhenAddTwoTasksForUser_ThenNumberOfTasksTwoAndStatusCode200Ok()
        {
            var user = new UserCreateDTO()
            {
                FirstName = "Bogdan",
                LastName = "Liusik",
                Email = "bogdanliusik@gmail.com",
                BirthDay = DateTime.Parse("31/12/2002"),
                CreatedAt = DateTime.Now,
            };
            
            var createdUserResponse = await _httpClient.PostAsync(
                    requestUri: "api/users", 
                    content: new StringContent(JsonConvert.SerializeObject(user), 
                    encoding:Encoding.UTF8, 
                    mediaType: "application/json"));
            
            var createdUser = JsonConvert.DeserializeObject<UserDTO>(await createdUserResponse.Content.ReadAsStringAsync());

            var project = new ProjectCreateDTO()
            {
                Name = "ProjectName",
                Description = "ProjectsDescription",
                AuthorId = createdUser.Id,
                TeamId = 1,
                CreatedAt = DateTime.Now,
                Deadline = DateTime.Now.AddMonths(5)
            };
            
            var createdProjectResponse = await _httpClient.PostAsync(
                    requestUri: "api/projects", 
                    content: new StringContent(JsonConvert.SerializeObject(project), 
                    encoding:Encoding.UTF8, 
                    mediaType: "application/json"));
            
            var createdProject = JsonConvert.DeserializeObject<UserDTO>(await createdProjectResponse.Content.ReadAsStringAsync());

            var task1 = new TaskCreateDTO() 
            { 
                Name = "TaskName",
                Description = "TaskDescription", 
                ProjectId = createdProject.Id,
                PerformerId = 1,
                CreatedAt = DateTime.Now,
            };
            
            var task2 = new TaskCreateDTO() 
            { 
                Name = "TaskName",
                Description = "TaskDescription", 
                ProjectId = createdProject.Id,
                PerformerId = 1,
                CreatedAt = DateTime.Now,
            };
            
            await _httpClient.PostAsync(
                    requestUri: "api/tasks", 
                    content: new StringContent(JsonConvert.SerializeObject(task1), 
                    encoding:Encoding.UTF8, 
                    mediaType: "application/json"));

            await _httpClient.PostAsync(
                    requestUri: "api/tasks", 
                    content: new StringContent(JsonConvert.SerializeObject(task2), 
                    encoding:Encoding.UTF8, 
                    mediaType: "application/json"));
            
            var queryResultResponse = await _httpClient.GetAsync($"{_endpoint}/query1/{createdUser.Id}");
            var queryResult =
                JsonConvert.DeserializeObject<ICollection<Query1StructureDTO>>(await queryResultResponse.Content
                    .ReadAsStringAsync());
            
            var result = queryResult.ToList()[0];
            Assert.Equal(HttpStatusCode.OK, queryResultResponse.StatusCode);
            Assert.Equal(createdProject.Id, result.Project.Id);
            Assert.Equal(2, result.TasksCount);
        }
        
        [Fact]
        public async Task Query1_WhenUserDoesNotHaveProjects_ThenStatusCode204NoContent()
        {
            var user = new UserCreateDTO()
            {
                FirstName = "Bogdan",
                LastName = "Liusik",
                Email = "bogdanliusik@gmail.com",
                BirthDay = DateTime.Parse("31/12/2002"),
                CreatedAt = DateTime.Now,
            };
            
            var createdUserResponse = await _httpClient.PostAsync(
                    requestUri: "api/users", 
                    content: new StringContent(JsonConvert.SerializeObject(user), 
                    encoding:Encoding.UTF8, 
                    mediaType: "application/json"));
            
            var createdUser = JsonConvert.DeserializeObject<UserDTO>(await createdUserResponse.Content.ReadAsStringAsync());
            
            var queryResultResponse = await _httpClient.GetAsync($"{_endpoint}/query1/{createdUser.Id}");
            
            Assert.Equal(HttpStatusCode.NoContent, queryResultResponse.StatusCode);
        }
    }
}