﻿using System;

namespace ProjectStructure.Common.DTO.Team
{
    public class TeamCreateDTO
    {
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; } = DateTime.Now;
    }
}