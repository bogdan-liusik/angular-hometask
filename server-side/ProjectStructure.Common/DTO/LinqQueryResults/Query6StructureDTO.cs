﻿using ProjectStructure.Common.DTO.Project;
using ProjectStructure.Common.DTO.Task;
using ProjectStructure.Common.DTO.User;

namespace ProjectStructure.Common.DTO.LinqQueryResults
{
    public class Query6StructureDTO
    {
        public UserDTO User { get; set; }
        public ProjectDTO LastProject { get; set; }
        public int NumberOfTasksInLastProject { get; set; }
        public int IncompleteOrCancelledCount { get; set; }
        public TaskDTO LongestTaskByDate { get; set; }
    }
}